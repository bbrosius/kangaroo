package com.bmb.kangaroo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.bmb.kangaroo.game.FlashcardGameActivity;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.nearby.StudyNearbyActivity;
import com.bmb.kangaroo.quiz.QuizActivity;
import com.bmb.utils.Utils;

/**
 * BaseActivity that handles the navigation drawer all activities in the app should extend from this activity. Unless they aren't going to make use of the navigation drawer.
 */
public abstract class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    NavigationView navigationDrawerList;
    private boolean useLightTheme;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Utils.setContext(this);

        SharedPreferences pref = getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);

        if( pref.getBoolean(getString(R.string.light_theme_key), false) )
        {
            getApplication().setTheme(R.style.FFMainTheme_Light);
            setTheme(R.style.FFMainTheme_Light);
            useLightTheme = true;
        }
        else
        {
            getApplication().setTheme(R.style.FFMainTheme);
            setTheme(R.style.FFMainTheme);
            useLightTheme = false;
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        navigationDrawerList = (NavigationView) findViewById(R.id.left_drawer);
        navigationDrawerList.setNavigationItemSelectedListener(this);
        ActionBar actionBar = getSupportActionBar();

        if( actionBar != null )
        {
            if(useLightTheme)
            {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_action_menu_white);
            }
            else
            {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_action_menu_dark);
            }
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getDrawerLayout().openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem)
    {
        Intent nextPageIntent = null;
        switch( menuItem.getItemId() ) {
            case R.id.subject_list:
                nextPageIntent = new Intent(BaseActivity.this, MainFlashcardSetListActivity.class);
                nextPageIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
            case R.id.settings:
                nextPageIntent = new Intent(BaseActivity.this, SubjectPreferenceActivity.class);
                if( SubjectService.getInstance().getCurrentSubjectId() != null )
                {
                    nextPageIntent.putExtra(getString(R.string.study_subject_id), SubjectService.getInstance().getCurrentSubjectId().toString());
                }
                break;
            case R.id.take_quiz:
                nextPageIntent = new Intent(BaseActivity.this, QuizActivity.class);
                if( SubjectService.getInstance().getCurrentSubjectId() != null )
                {
                    nextPageIntent.putExtra(getString(R.string.study_id), SubjectService.getInstance().getCurrentSubjectId().toString());
                }
                break;
            case R.id.study_nearby:
                nextPageIntent = new Intent(BaseActivity.this, StudyNearbyActivity.class);
                if( SubjectService.getInstance().getCurrentSubjectId() != null )
                {
                    nextPageIntent.putExtra(getString(R.string.study_id), SubjectService.getInstance().getCurrentSubjectId().toString());
                }
                break;
            case R.id.study_game:
                nextPageIntent = new Intent(BaseActivity.this, FlashcardGameActivity.class);
                break;
        }

        if( nextPageIntent != null )
        {
            startActivity(nextPageIntent);
            if( !( this instanceof MainFlashcardSetListActivity ) )
            {
                finish();
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    protected abstract DrawerLayout getDrawerLayout();
}

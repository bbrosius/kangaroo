package com.bmb.kangaroo.sql;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.bmb.kangaroo.utils.*;
import com.bmb.utils.Utils;

public class FlashcardDAO 
{
	private SQLiteDatabase flashcardDb;
	private final FlashcardOpenHelper flashcardHelper;
	
	public FlashcardDAO()
	{
		flashcardHelper = new FlashcardOpenHelper(Utils.getContext());
	}
	
	public void open() throws SQLiteException
	{
		flashcardDb = flashcardHelper.getWritableDatabase();
	}
	
	public void close()
	{
		flashcardHelper.close();
	}

	public long addFlashcard(Id subjectId, Id examId, Id flashcardSetId, Id noteId, String term, String definition, int isPicture)
	{
		ContentValues values = new ContentValues();
		values.put(FlashcardOpenHelper.COLUMN_TERM, term);
		values.put(FlashcardOpenHelper.COLUMN_DEFINITION, definition);
        values.put(FlashcardOpenHelper.COLUMN_IS_PICTURE, isPicture);
		if( examId != null )
		{
			values.put(FlashcardOpenHelper.COLUMN_EXAM_ID, examId.toString());
		}
		else
		{
			values.put(FlashcardOpenHelper.COLUMN_EXAM_ID, -1);
		}

		values.put(FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID, flashcardSetId.toString());

		if( subjectId != null )
		{
			values.put(FlashcardOpenHelper.COLUMN_SUBJECT_ID, subjectId.toString());
		}
		else
		{
			values.put(FlashcardOpenHelper.COLUMN_SUBJECT_ID, -1);
		}

        if( noteId != null )
        {
            values.put(FlashcardOpenHelper.COLUMN_NOTE_ID, noteId.toString());
        }
        else
        {
            values.put(FlashcardOpenHelper.COLUMN_NOTE_ID, -1);
        }

        values.put(FlashcardOpenHelper.COLUMN_CORRECT, -1);
		return flashcardDb.insert(FlashcardOpenHelper.FLASHCARD_TABLE_NAME, null, values);
	}

    public void updateFlashcard(Id subjectId, Id examId, Id flashcardSetId, long dbId)
    {
        ContentValues values = new ContentValues();

        if( examId != null )
        {
            values.put(FlashcardOpenHelper.COLUMN_EXAM_ID, examId.toString());
        }
        if( flashcardSetId != null )
        {
            values.put(FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID, flashcardSetId.toString());
        }

        if( subjectId != null )
        {
            values.put(FlashcardOpenHelper.COLUMN_SUBJECT_ID, subjectId.toString());
        }
        flashcardDb.update(FlashcardOpenHelper.FLASHCARD_TABLE_NAME, values, FlashcardOpenHelper.COLUMN_ID + "=" + dbId, null);
    }

    public void updateFlashcardText(String term, String definition, int isPicture, long dbId)
    {
        ContentValues values = new ContentValues();
        values.put(FlashcardOpenHelper.COLUMN_TERM, term);
        values.put(FlashcardOpenHelper.COLUMN_DEFINITION, definition);
        values.put(FlashcardOpenHelper.COLUMN_IS_PICTURE, isPicture);
        flashcardDb.update(FlashcardOpenHelper.FLASHCARD_TABLE_NAME, values, FlashcardOpenHelper.COLUMN_ID + "=" + dbId, null);
    }

    public void updateFlashcardCorrectness(int correctness, long dbId)
    {
        ContentValues values = new ContentValues();
        values.put(FlashcardOpenHelper.COLUMN_CORRECT, correctness);
        flashcardDb.update(FlashcardOpenHelper.FLASHCARD_TABLE_NAME, values, FlashcardOpenHelper.COLUMN_ID + "=" + dbId, null);
    }

	public boolean deleteFlashcard(long id)
	{
		int numRowsDeleted = flashcardDb.delete(FlashcardOpenHelper.FLASHCARD_TABLE_NAME, FlashcardOpenHelper.COLUMN_ID + " = " + id, null);
		
		boolean deleted = false;
		
		if( numRowsDeleted > 0 )
		{
			deleted = true;
		}
		
		return deleted;
	}
	
	public Cursor getFlashcard(long id)
	{
		String query = "SELECT * FROM " +  FlashcardOpenHelper.FLASHCARD_TABLE_NAME + " WHERE " + FlashcardOpenHelper.COLUMN_ID + " = " + id + ";";
		return flashcardDb.rawQuery(query, null);
	}

    /**
     * Finds all flashcards for the given id. Can be returned sequentially or in a random order.
     * @param id The id of the object to retrieve the flashcards for.
     * @param randomOrder True if the flashcards should be returned in random order, false if they should be sequential.
     * @return A cursor pointing to all the flashcards contained by the object pointed to by id, or null if none can be found.
     */
	public Cursor findFlashcards(Id id, boolean randomOrder )
	{
		String column = FlashcardOpenHelper.COLUMN_EXAM_ID;
		Cursor cursor = null;
		if( id != null )
		{
			if( id.getType() instanceof SubjectIdType )
			{
				column = FlashcardOpenHelper.COLUMN_SUBJECT_ID;
			}
			else if( id.getType() instanceof FlashcardSetIdType )
			{
				column = FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID;
			}
			else if( id.getType() instanceof ExamIdType )
			{
				column = FlashcardOpenHelper.COLUMN_EXAM_ID;
			}

            String idString = id.toString();
            String query;
            if( randomOrder )
            {
                query = "SELECT * FROM " +  FlashcardOpenHelper.FLASHCARD_TABLE_NAME + " WHERE " + column + " =  ? ORDER BY RANDOM()";
            }
            else
            {
                query = "SELECT * FROM " +  FlashcardOpenHelper.FLASHCARD_TABLE_NAME + " WHERE " + column + " =  ?";
            }


			cursor = flashcardDb.rawQuery(query, new String[] {idString});
		}
		return cursor;
	}

    public Cursor findIncorrectFlashcards(Id id, boolean randomOrder)
    {
        String column = FlashcardOpenHelper.COLUMN_EXAM_ID;
        Cursor cursor = null;
        if( id != null )
        {
            if( id.getType() instanceof SubjectIdType )
            {
                column = FlashcardOpenHelper.COLUMN_SUBJECT_ID;
            }
            else if( id.getType() instanceof FlashcardSetIdType )
            {
                column = FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID;
            }
            else if( id.getType() instanceof ExamIdType )
            {
                column = FlashcardOpenHelper.COLUMN_EXAM_ID;
            }

            String idString = id.toString();
            String query;
            if( randomOrder )
            {
                query = "SELECT * FROM " +  FlashcardOpenHelper.FLASHCARD_TABLE_NAME + " WHERE " + column + " =  ? and "
                        + FlashcardOpenHelper.COLUMN_CORRECT + " = ? ORDER BY RANDOM()";
            }
            else
            {
                query = "SELECT * FROM " +  FlashcardOpenHelper.FLASHCARD_TABLE_NAME + " WHERE " + column + " =  ? and "
                        + FlashcardOpenHelper.COLUMN_CORRECT + " = ?";
            }

            cursor = flashcardDb.rawQuery(query, new String[] {idString, String.valueOf(0)});
        }
        return cursor;
    }

    public Cursor getAllFlashcards()
    {
        String query = "SELECT * FROM " +  FlashcardOpenHelper.FLASHCARD_TABLE_NAME + " ORDER BY RANDOM()";
        return flashcardDb.rawQuery(query, null);
    }
}
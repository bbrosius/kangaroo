package com.bmb.kangaroo.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FlashcardOpenHelper extends SQLiteOpenHelper
{
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "flashcards.db";
    public static final String FLASHCARD_TABLE_NAME = "flashcards";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TERM = "term";
    public static final String COLUMN_DEFINITION = "definition";
    public static final String COLUMN_SUBJECT_ID = "subject_id";
    public static final String COLUMN_EXAM_ID = "exam_id";
    public static final String COLUMN_FLASHCARD_SET_ID = "flashcard_set_id";
    public static final String COLUMN_NOTE_ID = "note_id";
    public static final String COLUMN_CORRECT = "correct";
    public static final String COLUMN_IS_PICTURE = "is_picture";
    
    private static final String FLASHCARD_TABLE_CREATE =
                "CREATE TABLE " + FLASHCARD_TABLE_NAME + " (" 
    		+	COLUMN_ID + " INTEGER PRIMARY KEY,"
    		+	COLUMN_TERM + " TEXT,"
    		+	COLUMN_DEFINITION + " TEXT,"
    		+	COLUMN_SUBJECT_ID + " TEXT,"
    		+	COLUMN_EXAM_ID + " TEXT,"
    		+	COLUMN_FLASHCARD_SET_ID + " TEXT,"
            +   COLUMN_NOTE_ID + " TEXT,"
            +   COLUMN_CORRECT + " INTEGER,"
            +   COLUMN_IS_PICTURE + " INTEGER"
    		+	");";
	
	FlashcardOpenHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		db.execSQL(FLASHCARD_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) 
	{
		
	}
	
}
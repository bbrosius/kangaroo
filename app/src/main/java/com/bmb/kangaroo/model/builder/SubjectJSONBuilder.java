package com.bmb.kangaroo.model.builder;

import android.util.Log;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubjectJSONBuilder
{
    private final static String SUBJECT_LIST = "subjectList";
    private final static String ID = "id";
    private final static String SUBJECT_TITLE = "title";
    private final static String EXAM_IDS = "examIds";
    private final static String NOTE_IDS = "noteIds";
    private final static String FLASHCARD_SET_IDS = "flashcardSetIds";
    private final static String GRADE_GOAL = "gradeGoal";

    public static JSONObject serializeSubjectMap(Map<Id, Subject> subjectMap)
    {
        JSONObject subjectListObject = new JSONObject();
        JSONArray subjectListArray = new JSONArray();
        for(Map.Entry<Id, Subject> entry : subjectMap.entrySet())
        {
            JSONObject subjectObj = serializeSubject(entry.getValue());
            if( subjectObj != null )
            {
                subjectListArray.put(subjectObj);
            }
        }

        try
        {
            subjectListObject.put(SUBJECT_LIST, subjectListArray);
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return subjectListObject;
    }

    public static Map<Id, Subject> deserializeSubjectMap(JSONObject obj)
    {
        Map<Id, Subject> subjectMap = new HashMap<>();
        try
        {

            JSONArray subjects = obj.getJSONArray(SUBJECT_LIST);
            for( int i = 0; i < subjects.length(); i++ )
            {
                Subject subject = deserializeSubject(subjects.getJSONObject(i));
                if( subject != null )
                {
                    subjectMap.put(subject.getId(), subject);
                }
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return subjectMap;
    }

    private static JSONObject serializeSubject(Subject subject)
    {
        JSONObject subjectObj = new JSONObject();
        try
        {
            subjectObj.put(ID, subject.getId().toString());
            subjectObj.put(SUBJECT_TITLE, subject.getTitle());
            subjectObj.put(GRADE_GOAL, subject.getGradeGoal());

            JSONArray examIds = new JSONArray();
            for( Id examId : subject.getExamIds() )
            {
                examIds.put(examId.toString());
            }
            subjectObj.put(EXAM_IDS, examIds);

            JSONArray noteIds = new JSONArray();
            for( Id noteId : subject.getNoteIds() )
            {
                noteIds.put(noteId.toString());
            }
            subjectObj.put(NOTE_IDS, noteIds);

            JSONArray flashcardSetIds = new JSONArray();
            for( Id flashcardSetId : subject.getFlashcardSetIds())
            {
                flashcardSetIds.put(flashcardSetId.toString());
            }
            subjectObj.put(FLASHCARD_SET_IDS, flashcardSetIds);
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return subjectObj;
    }

    private static Subject deserializeSubject(JSONObject obj)
    {
        Subject subject = null;
        String title = null;
        Id subjectId = null;
        int gradeGoal = 0;
        ArrayList<Id> examIds = new ArrayList<>();
        ArrayList<Id> noteIds = new ArrayList<>();
        ArrayList<Id> flashcardSetIds = new ArrayList<>();

        try
        {
            if( obj.has(ID) )
            {
                subjectId = IdService.resolveId(obj.getString(ID));
            }

            if( obj.has(SUBJECT_TITLE) )
            {
                title = obj.getString(SUBJECT_TITLE);
            }

            if( obj.has(GRADE_GOAL) )
            {
                gradeGoal = obj.getInt(GRADE_GOAL);
            }

            if( obj.has(EXAM_IDS) )
            {
                JSONArray examIdArray = obj.getJSONArray(EXAM_IDS);

                for( int i = 0; i < examIdArray.length(); i++)
                {
                    Id id = IdService.resolveId(examIdArray.getString(i));
                    examIds.add(id);
                }
            }

            if( obj.has(NOTE_IDS) )
            {
                JSONArray noteIdArray = obj.getJSONArray(NOTE_IDS);

                for( int i = 0; i < noteIdArray.length(); i++ )
                {
                    Id id = IdService.resolveId(noteIdArray.getString(i));
                    noteIds.add(id);
                }
            }

            if( obj.has(FLASHCARD_SET_IDS) )
            {
                JSONArray flashcardSetArray = obj.getJSONArray(FLASHCARD_SET_IDS);

                for( int i = 0; i < flashcardSetArray.length(); i++ )
                {
                    Id id = IdService.resolveId(flashcardSetArray.getString(i));
                    flashcardSetIds.add(id);
                }
            }

        }
        catch( JSONException e)
        {
            Log.e(Utils.getContext().getString(R.string.log_tag), "Error getting subject id");
        }

        if( subjectId != null && title != null )
        {
            subject = new Subject(title);
            subject.setId(subjectId);
            subject.setGradeGoal(gradeGoal);
            subject.setFlashcardSetIds(flashcardSetIds);
            subject.setExamIds(examIds);
            subject.setNoteIds(noteIds);
        }
        return subject;
    }
}

package com.bmb.kangaroo.model;

import com.bmb.kangaroo.utils.Id;

import java.util.Calendar;

public interface SubjectChild
{
    String getTitle();

    Calendar getCreationDate();

    Id getId();
}

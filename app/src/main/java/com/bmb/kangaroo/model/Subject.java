package com.bmb.kangaroo.model;

import com.bmb.kangaroo.model.services.ExamService;
import com.bmb.kangaroo.utils.Id;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Representation of a subject.
 */
public class Subject
{
    private Id id;
    public String title;
    private ArrayList<Id> examIds;
    private ArrayList<Id> noteIds;
    private ArrayList<Id> flashcardSetIds;
    private int gradeGoal = 0;

    public Subject(String title)
    {
        this.title = title;
        examIds = new ArrayList<>();
        noteIds = new ArrayList<>();
        flashcardSetIds = new ArrayList<>();
    }

    public void setId(Id id)
    {
        this.id = id;
    }

    public Id getId()
    {
        return id;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public void setGradeGoal(int goal)
    {
        this.gradeGoal = goal;
    }

    public int getGradeGoal()
    {
        return gradeGoal;
    }

    public void setExamIds(ArrayList<Id> examIds)
    {
        this.examIds = examIds;
    }

    public void addExamId(Id id)
    {
        examIds.add(id);
    }

    public ArrayList<Id> getExamIds()
    {
        return examIds;
    }

    public Exam getNextExam()
    {
        Exam nextExam = null;
        for( Id examId : examIds )
        {
            Exam exam = ExamService.getInstance().getExam(examId);
            if( nextExam != null )
            {
                if( exam.getExamDate().getTimeInMillis() > nextExam.getExamDate().getTimeInMillis() )
                {
                    nextExam = exam;
                }
            }
            else
            {
                nextExam = exam;
            }
        }

        if( nextExam != null && nextExam.getExamDate().getTimeInMillis() < Calendar.getInstance().getTimeInMillis() )
        {
            nextExam = null;
        }
        return nextExam;
    }

    public Calendar getPreviousExamDate(Exam exam)
    {
        Calendar previousDate = null;
        Calendar examDate = exam.getExamDate();
        for( Id examId : examIds )
        {
            Exam testExam = ExamService.getInstance().getExam(examId);

            if( testExam.getExamDate().before(examDate) )
            {
                if( previousDate == null )
                {
                    previousDate = testExam.getExamDate();
                }
                else if( testExam.getExamDate().after(previousDate) )
                {
                    previousDate = testExam.getExamDate();
                }
            }
        }

        return previousDate;
    }
    public void setNoteIds(ArrayList<Id> noteIds)
    {
        this.noteIds = noteIds;
    }

    public void addNoteId(Id id, boolean addToExams)
    {
        noteIds.add(id);
    }

    public void removeNoteId(Id id)
    {
        noteIds.remove(id);
    }

    public ArrayList<Id> getNoteIds()
    {
        return noteIds;
    }

    public ArrayList<Id> getChildIds()
    {
        ArrayList<Id> children = new ArrayList<>();

        children.addAll(noteIds);
        children.addAll(flashcardSetIds);

        return children;
    }

    public ArrayList<Id> getFlashcardSetIds()
    {
        return flashcardSetIds;
    }

    public void setFlashcardSetIds(ArrayList<Id> flashcardSetIds)
    {
        this.flashcardSetIds = flashcardSetIds;
    }

    public void addFlashcardSet(Id flashcardSetId)
    {
        flashcardSetIds.add(flashcardSetId);
    }

    public void removeFlashcardSet(Id flashcardSetId)
    {
        flashcardSetIds.remove(flashcardSetId);
    }

    @Override
    public String toString()
    {
        String subject = id + "|" + title + "|" + gradeGoal;
        for( Id id : examIds )
        {
            subject += "|" + id;
        }

        for( Id id : noteIds )
        {
            subject += "|" + id;
        }

        for( Id id : flashcardSetIds )
        {
            subject += "|" + id;
        }

        return subject;
    }
}

package com.bmb.kangaroo.model.services;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.builder.FlashcardSetJSONBuilder;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.FlashcardSetIdType;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.utils.Utils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Flashcard Set Service controls access to the {@link com.bmb.kangaroo.model.FlashcardSet} objects, and saves and loads them to the persistent file.
 */
public class FlashcardSetService
{
    private final static String fileName = "flashcardsetData";

    /**
     * An map of exams, keyed by their ids.
     */
    private final Map<Id, FlashcardSet> flashcardSetMap = new HashMap<>();

    private static FlashcardSetService content;

    public static FlashcardSetService getInstance()
    {
        if( content == null )
        {
            content = new FlashcardSetService();
        }

        return content;
    }

    private FlashcardSetService()
    {
        loadFlashcardSetsFromFile();
    }

    public void closeFlashcardSets()
    {
        saveFlashcardSetsToFile();
    }

    public List<FlashcardSet> getFlashcardSets()
    {
        List<FlashcardSet> flashcardSets = new ArrayList<>();
        for (Map.Entry<Id, FlashcardSet> setEntry : flashcardSetMap.entrySet())
        {
            flashcardSets.add(setEntry.getValue());
        }
        return flashcardSets;
    }

    public FlashcardSet getFlashcardSet(Id id)
    {
        return flashcardSetMap.get(id);
    }

    private void loadFlashcardSetsFromFile()
    {
        if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            FileInputStream fis = null;

            String result = null;
            try
            {

                try
                {
                    if( Utils.getContext() != null )
                    {
                        fis = Utils.getContext().openFileInput(fileName);
                        result = IOUtils.toString(fis, "UTF-8");
                    }
                } catch( FileNotFoundException e1 )
                {
                    Log.e(Utils.getContext().getString(R.string.log_tag), "Error opening flashcard sets file: " + e1.getMessage());
                }

                // json is UTF-8 by default
                if( fis != null )
                {
                    fis.close();
                }
            } catch( Exception e )
            {
                Log.e(Utils.getContext().getString(R.string.log_tag), "Error opening flashcard sets file: " + e.getMessage());
            }

            try
            {
                if( result != null )
                {
                    JSONObject object = new JSONObject(result);
                    List<FlashcardSet> flashcardSets = FlashcardSetJSONBuilder.deserializeFlashcardSetList(object, null);
                    for( FlashcardSet set : flashcardSets )
                    {
                        flashcardSetMap.put(set.getId(), set);
                    }
                }
            } catch( JSONException e )
            {
                e.printStackTrace();
            }
        }
    }

    public void saveFlashcardSetsToFile()
    {
        if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            if( !flashcardSetMap.isEmpty() )
            {
                List<FlashcardSet> sets = new ArrayList<>();
                for( Map.Entry<Id, FlashcardSet> entry : flashcardSetMap.entrySet() )
                {
                    sets.add(entry.getValue());
                }

                JSONObject obj = FlashcardSetJSONBuilder.serializeFlashcardSetList(sets, false, null);

                try
                {
                    FileOutputStream outputStream = Utils.getContext().openFileOutput(fileName, Context.MODE_PRIVATE);
                    outputStream.write(obj.toString().getBytes());
                    outputStream.close();
                } catch( Exception e )
                {
                    Log.e(Utils.getContext().getString(R.string.log_tag), "Error saving flashcard sets file: " + e.getMessage());
                }
            }
        }
    }

    public Id addFlashcardSet(FlashcardSet flashcardSet)
    {
        Id newId = null;
        if( flashcardSet != null )
        {
            newId = IdService.mintID(new FlashcardSetIdType());
            flashcardSet.setId(newId);

            flashcardSetMap.put(newId, flashcardSet);
        }

        return newId;
    }

    public void deleteFlashcardSet(Id setId)
    {
        if( setId != null )
        {
            flashcardSetMap.remove(setId);

            FlashcardDAO dao = new FlashcardDAO();
            dao.open();

            Cursor flashcards = dao.findFlashcards(setId, false);
            if(flashcards != null && flashcards.getCount() > 0)
            {
                while( flashcards.moveToNext() )
                {
                    if( !dao.deleteFlashcard(flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID))) )
                    {
                        Log.e(Utils.getContext().getString(R.string.log_tag), Utils.getContext().getString(R.string.flashcard_delete_error));
                    }
                }

            }

            dao.close();
        }
    }
}

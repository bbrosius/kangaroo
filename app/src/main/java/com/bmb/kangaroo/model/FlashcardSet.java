package com.bmb.kangaroo.model;

import com.bmb.kangaroo.utils.Id;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class FlashcardSet implements SubjectChild
{
    private Id id;
    private String title;
    private ArrayList<Long> flashcardIds;
    private Calendar creationDate;
    private boolean isQuizletSet = false;

    public FlashcardSet(String title)
    {
        this.title = title;
        flashcardIds = new ArrayList<>();
        creationDate = new GregorianCalendar();
    }

    public void setId(Id id)
    {
        this.id = id;
    }

    @Override
    public Id getId()
    {
        return id;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    @Override
    public String getTitle()
    {
        return title;
    }

    public void setCreationDate(Calendar creationDate)
    {
        this.creationDate = creationDate;
    }

    public int getFlashcardCount()
    {
        return flashcardIds.size();
    }

    public ArrayList<Long> getFlashcardIds()
    {
        return  flashcardIds;
    }

    public void setFlashcardIds(ArrayList<Long> ids )
    {
        flashcardIds = ids;
    }

    public void addFlashcardId(Long flashcardId)
    {
        flashcardIds.add(flashcardId);
    }

    public boolean isQuizletSet()
    {
        return isQuizletSet;
    }

    public void setisQuizletSet(boolean isQuizletSet)
    {
        this.isQuizletSet = isQuizletSet;
    }

    @Override
    public Calendar getCreationDate()
    {
        return creationDate;
    }

    @Override
    public String toString()
    {

        return id + "|" + title + "|" + creationDate + "|" + isQuizletSet;
    }
}

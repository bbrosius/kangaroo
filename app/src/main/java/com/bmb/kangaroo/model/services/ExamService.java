package com.bmb.kangaroo.model.services;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.builder.ExamJSONBuilder;
import com.bmb.kangaroo.services.ExamNotificationService;
import com.bmb.kangaroo.utils.ExamIdType;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.utils.Utils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.*;

public class ExamService
{
    private final static String fileName = "examData";

    /**
     * A map of exams items, by ID.
     */
    private Map<Id, Exam> examsMap = new HashMap<>();

    private static ExamService content;

    public static ExamService getInstance()
    {
        if( content == null )
        {
            content = new ExamService();
        }

        return content;
    }

    private ExamService()
    {
        loadExamsFromFile();
    }

    public List<Exam> getExams()
    {
        ArrayList<Exam> exams = new ArrayList<>();
        for( Map.Entry<Id, Exam> examEntry : examsMap.entrySet() )
        {
            exams.add(examEntry.getValue());
        }
        return exams;
    }

    public Exam getExam(Id id)
    {
        return examsMap.get(id);
    }

    private void loadExamsFromFile()
    {
        if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            FileInputStream fis = null;

            String result = null;
            try
            {

                try
                {
                    if( Utils.getContext() != null )
                    {
                        fis = Utils.getContext().openFileInput(fileName);
                        result = IOUtils.toString(fis, "UTF-8");
                    }
                } catch( FileNotFoundException e1 )
                {
                    Log.e(Utils.getContext().getString(R.string.log_tag), "Error opening exams file: " + e1.getMessage());
                }

                // json is UTF-8 by default
                if( fis != null )
                {
                    fis.close();
                }
            } catch( Exception e )
            {
                Log.e(Utils.getContext().getString(R.string.log_tag), "Error opening exams file: " + e.getMessage());
            }

            try
            {
                if( result != null )
                {
                    JSONObject object = new JSONObject(result);
                    examsMap = ExamJSONBuilder.deserializeExamMap(object);
                }
            } catch( JSONException e )
            {
                e.printStackTrace();
            }
        }
    }

    public void saveExamsToFile()
    {
        if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            if( !examsMap.isEmpty() )
            {
                JSONObject obj = ExamJSONBuilder.serializeExamMap(examsMap);

                try
                {
                    FileOutputStream outputStream = Utils.getContext().openFileOutput(fileName, Context.MODE_PRIVATE);
                    outputStream.write(obj.toString().getBytes());
                    outputStream.close();
                } catch( Exception e )
                {
                    Log.e(Utils.getContext().getString(R.string.log_tag), "Error saving subjects file: " + e.getMessage());
                }
            }
        }
    }

    public Id addExam(Exam exam, boolean setReminder, boolean addToCalendar)
    {
        if( exam != null )
        {
            exam.setId(IdService.mintID(new ExamIdType()));

            //TODO: Set this up to be called only when using android version 4.0 so we can back port the app
            if( addToCalendar && exam.getExamDate() != null )
            {
                Intent addEventIntent = new Intent(Intent.ACTION_INSERT);
                addEventIntent.setData(CalendarContract.Events.CONTENT_URI);
                if( exam.getExamDate() != null )
                {
                    addEventIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, exam.getExamDate().getTimeInMillis());
                    Calendar endDate = exam.getExamDate();
                    endDate.add(Calendar.HOUR_OF_DAY, exam.getExamDate().get(Calendar.HOUR_OF_DAY) + 1);
                    addEventIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endDate);
                    addEventIntent.putExtra(CalendarContract.Events.TITLE, SubjectService.getInstance().getSubject(exam.getSubjectId()).title);
                    addEventIntent.putExtra(CalendarContract.Events.DESCRIPTION, exam.title);
                    Utils.getContext().startActivity(addEventIntent);
                }
            }

            if( setReminder && exam.getReminderDate() != null )
            {
                setNotificationAlarm(exam.getReminderDate(), exam.getId());
            }

            examsMap.put(exam.getId(), exam);

            return exam.getId();
        }

        return null;
    }

    private void setNotificationAlarm(Calendar reminderDate, Id examId)
    {
        AlarmManager alarmManager = (AlarmManager) Utils.getContext().getSystemService(Context.ALARM_SERVICE);
        long when = reminderDate.getTimeInMillis();
        Intent intent = new Intent(Utils.getContext(), ExamNotificationService.class);
        intent.putExtra(ExamNotificationService.EXAM_ID_EXTRA, examId.toString());
        PendingIntent pendingIntent = PendingIntent.getService(Utils.getContext(), 0, intent, 0);
        alarmManager.set(AlarmManager.RTC, when, pendingIntent);
    }
}

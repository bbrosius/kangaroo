package com.bmb.kangaroo.model.builder;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.FlashcardSetIdType;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class FlashcardSetJSONBuilder
{
    private final static String FLASHCARD_SET_LIST = "flashcardSets";
    private final static String FLASHCARD_SET_ID = "id";
    private final static String FLASHCARD_SET_TITLE = "title";
    private final static String FLASHCARD_SET_CREATION_DATE = "creationDate";
    private final static String FLASHCARD_SET_IS_QUIZLET = "isQuizlet";
    private final static String FLASHCARD_IDS = "flashcardIds";
    private final static String FLASHCARDS = "flashcards";
    private final static String FLASHCARD_ID = "flashcard_id";
    private final static String FLASHCARD_TERM = "term";
    private final static String FLASHCARD_DEFINITION = "definition";
    private final static String FLASHCARD_SUBJECT_ID = "subjectId";
    private final static String FLASHCARD_NOTE_ID = "noteId";
    private final static String FLASHCARD_EXAM_ID = "examId";
    private final static String FLASHCARD_IS_PICTURE = "isPictureFlashcard";

    public static JSONObject serializeFlashcardSetList(List<FlashcardSet> flashcardSetList, boolean includeFlashcards, Activity activity)
    {
        JSONObject flashcardSetListObject = new JSONObject();
        JSONArray flashcardSetArray = new JSONArray();
        for(FlashcardSet set : flashcardSetList)
        {
            JSONObject setObj = serializeFlashcardSet(set, includeFlashcards, activity);
            if( setObj != null )
            {
                flashcardSetArray.put(setObj);
            }
        }

        try
        {
            flashcardSetListObject.put(FLASHCARD_SET_LIST, flashcardSetArray);
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return flashcardSetListObject;
    }

    public static List<FlashcardSet> deserializeFlashcardSetList(JSONObject obj, Activity activity)
    {
        List<FlashcardSet> flashcardSetList = new ArrayList<>();
        try
        {

            JSONArray flashcardsSets = obj.getJSONArray(FLASHCARD_SET_LIST);
            for( int i = 0; i < flashcardsSets.length(); i++ )
            {
                FlashcardSet flashcardSet = deserializeFlashcardSet(flashcardsSets.getJSONObject(i), activity);
                if( flashcardSet != null )
                {
                    flashcardSetList.add(flashcardSet);
                }
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return flashcardSetList;
    }

    public static JSONObject serializeFlashcardSet(FlashcardSet flashcardSet, boolean includeFlashcards, Activity activity)
    {
        JSONObject flashcardSetObj = new JSONObject();
        try
        {
            flashcardSetObj.put(FLASHCARD_SET_ID, flashcardSet.getId().toString());
            flashcardSetObj.put(FLASHCARD_SET_TITLE, flashcardSet.getTitle());
            flashcardSetObj.put(FLASHCARD_SET_IS_QUIZLET, flashcardSet.isQuizletSet());
            DateFormat dateFormat = new SimpleDateFormat(Utils.getContext().getString(R.string.date_format), Locale.getDefault());

            flashcardSetObj.put(FLASHCARD_SET_CREATION_DATE, dateFormat.format(flashcardSet.getCreationDate().getTime()));

            JSONArray flashcardIds = new JSONArray();
            for( long flashcardId : flashcardSet.getFlashcardIds() )
            {
                flashcardIds.put(flashcardId);
            }
            flashcardSetObj.put(FLASHCARD_IDS, flashcardIds);

            if( includeFlashcards )
            {
                FlashcardDAO flashcardDAO = new FlashcardDAO();
                flashcardDAO.open();

                JSONArray flashcards = new JSONArray();
                Cursor flashcardCursor = flashcardDAO.findFlashcards(flashcardSet.getId(), false);
                while( flashcardCursor.moveToNext() )
                {
                    JSONObject flashcardObject = new JSONObject();
                    flashcardObject.put(FLASHCARD_DEFINITION, flashcardCursor.getString(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION)));
                    flashcardObject.put(FLASHCARD_ID, flashcardCursor.getLong(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_ID)));
                    int flashcardIsPicture = flashcardCursor.getInt(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_IS_PICTURE));
                    flashcardObject.put(FLASHCARD_IS_PICTURE, flashcardIsPicture);
                    String flashcardTerm = flashcardCursor.getString(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                    if( flashcardIsPicture == 1 && activity != null )
                    {
                        try
                        {
                            Bitmap image = GraphicUtils.loadBitmap(Uri.parse(flashcardTerm), 250, 250, activity);
                            String imageString = GraphicUtils.getStringFromBitmap(image);
                            flashcardObject.put(FLASHCARD_TERM, imageString);
                        }
                        catch( FileNotFoundException e )
                        {
                            Log.e(activity.getString(R.string.log_tag), "Error reading bitmap to save to flashcard file.");
                        }
                    }
                    else
                    {
                        flashcardObject.put(FLASHCARD_TERM, flashcardTerm);
                    }
                    String subjectId = flashcardCursor.getString(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_SUBJECT_ID));
                    if( subjectId != null && !subjectId.isEmpty() )
                    {
                        flashcardObject.put(FLASHCARD_SUBJECT_ID, subjectId);
                    }

                    String examId = flashcardCursor.getString(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_EXAM_ID));
                    if( examId != null && !examId.isEmpty() )
                    {
                        flashcardObject.put(FLASHCARD_EXAM_ID, examId);
                    }

                    String noteId = flashcardCursor.getString(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_NOTE_ID));
                    if( noteId != null && !noteId.isEmpty() )
                    {
                        flashcardObject.put(FLASHCARD_NOTE_ID, noteId);
                    }

                    flashcards.put(flashcardObject);
                }

                flashcardSetObj.put(FLASHCARDS, flashcards);
                flashcardDAO.close();
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return flashcardSetObj;
    }

    public static FlashcardSet deserializeFlashcardSet(JSONObject obj, Activity activity)
    {
        FlashcardSet flashcardSet;
        String title = null;
        Id setId = null;
        Calendar creationDate = null;
        ArrayList<Long> flashcardIds = null;
        boolean isQuizletSet = false;
        try
        {
            if( obj.has(FLASHCARD_SET_ID) )
            {
                setId = IdService.resolveId( obj.getString(FLASHCARD_SET_ID));
            }

            if( obj.has(FLASHCARD_SET_TITLE) )
            {
                title = obj.getString(FLASHCARD_SET_TITLE);
            }

            if( obj.has(FLASHCARD_SET_CREATION_DATE) )
            {
                DateFormat dateFormat = new SimpleDateFormat(Utils.getContext().getString(R.string.date_format), Locale.getDefault());
                String dateString = obj.getString(FLASHCARD_SET_CREATION_DATE);
                creationDate = new GregorianCalendar();
                creationDate.setTime(dateFormat.parse(dateString));
            }

            if( obj.has(FLASHCARD_SET_IS_QUIZLET) )
            {
                isQuizletSet = obj.getBoolean(FLASHCARD_SET_IS_QUIZLET);
            }

            if( obj.has(FLASHCARD_IDS) )
            {
                flashcardIds = new ArrayList<>();
                JSONArray jsonFlashcardIds = obj.getJSONArray(FLASHCARD_IDS);
                for( int i = 0; i < jsonFlashcardIds.length(); i++ )
                {
                    flashcardIds.add(jsonFlashcardIds.getLong(i));
                }
            }
        }
        catch( JSONException e)
        {
            Log.e(Utils.getContext().getString(R.string.log_tag), "Error getting flashcard set");
            return null;
        }
        catch( ParseException e )
        {
            Log.e(Utils.getContext().getString(R.string.log_tag), "Error getting flashcard set creation date");
        }

        flashcardSet = new FlashcardSet(title);
        if( setId == null )
        {
            setId = IdService.mintID(new FlashcardSetIdType());
        }

        flashcardSet.setId(setId);
        flashcardSet.setisQuizletSet(isQuizletSet);

        if( creationDate == null )
        {
            creationDate = new GregorianCalendar();
        }
        flashcardSet.setCreationDate(creationDate);

        if( flashcardIds != null )
        {
            flashcardSet.setFlashcardIds(flashcardIds);
        }
        if( setId != null && obj.has(FLASHCARDS))
        {
            try
            {
                JSONArray flashcardsArray = obj.getJSONArray(FLASHCARDS);
                FlashcardDAO flashcardDAO = new FlashcardDAO();
                flashcardDAO.open();
                flashcardSet.getFlashcardIds().clear();

                for( int i = 0; i < flashcardsArray.length(); i++)
                {
                    //Do each flashcard in a separate try catch so we can continue even if one flashcard can't be deserialized.
                    try
                    {
                        JSONObject flashcardObj = flashcardsArray.getJSONObject(i);
                        String term = flashcardObj.getString(FLASHCARD_TERM);
                        String definition = flashcardObj.getString(FLASHCARD_DEFINITION);
                        int isPicture = flashcardObj.getInt(FLASHCARD_IS_PICTURE);

                        if( isPicture == 1 && activity != null )
                        {
                            Bitmap flashcardImage = GraphicUtils.getBitmapFromString(term);
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
                            String imageFileName = "FLASHCARD_" + timeStamp;
                            File storageDir = Environment.getExternalStoragePublicDirectory(
                                    Environment.DIRECTORY_PICTURES);
                            try {
                                File image = File.createTempFile(
                                        imageFileName,  /* prefix */
                                        ".jpg",         /* suffix */
                                        storageDir      /* directory */
                                );

                                FileOutputStream fOut = new FileOutputStream(image);

                                flashcardImage.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                                fOut.flush();
                                fOut.close();

                                Uri imageUri = Uri.fromFile(image);
                                term = imageUri.toString();

                                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                mediaScanIntent.setData(imageUri);
                                activity.sendBroadcast(mediaScanIntent);
                            }
                            catch( IOException e )
                            {
                                Log.e(activity.getString(R.string.log_tag), "Error reading in bitmap from flashcard file");
                            }
                        }

                        Id subjectId = null;
                        Id examId = null;
                        Id noteId = null;

                        if( flashcardObj.has(FLASHCARD_SUBJECT_ID) )
                        {
                            subjectId = IdService.resolveId(flashcardObj.getString(FLASHCARD_SUBJECT_ID));
                        }

                        if( flashcardObj.has(FLASHCARD_EXAM_ID) )
                        {
                            examId = IdService.resolveId(flashcardObj.getString(FLASHCARD_EXAM_ID));
                        }

                        if( flashcardObj.has(FLASHCARD_NOTE_ID) )
                        {
                            noteId = IdService.resolveId(flashcardObj.getString(FLASHCARD_NOTE_ID));
                        }

                        if( term != null && definition != null )
                        {
                            long dbId = flashcardDAO.addFlashcard(subjectId, examId, setId, noteId, term, definition, isPicture);
                            flashcardSet.addFlashcardId(dbId);
                        }
                    }
                    catch(JSONException e)
                    {
                        Log.e(Utils.getContext().getString(R.string.log_tag), "Error deserializing flashcard");
                    }
                }
                flashcardDAO.close();
            }
            catch( JSONException e )
            {
                Log.d(Utils.getContext().getString(R.string.log_tag), "Unable to get flashcards.");
            }
        }
        return flashcardSet;
    }
}

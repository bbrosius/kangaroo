package com.bmb.kangaroo.model.services;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.builder.SubjectJSONBuilder;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.kangaroo.utils.SubjectIdType;
import com.bmb.utils.Utils;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.*;

public class SubjectService
{
    private final static String fileName = "subjectData";

    /**
     * A map of subject items, by ID.
     */
    private Map<Id, Subject> subjectsMap = new HashMap<>();

    private Id currentSubject;

    private static SubjectService content;

    public static SubjectService getInstance()
    {
        if( content == null )
        {
            content = new SubjectService();
        }

        return content;
    }

    private SubjectService()
    {
        loadSubjectsFromFile();
    }

    public void setCurrentSubject(Id currentSubject)
    {
        this.currentSubject = currentSubject;
    }

    public Id getCurrentSubjectId()
    {
        return currentSubject;
    }

    public Subject getCurrentSubject()
    {
        return subjectsMap.get(getCurrentSubjectId());
    }

    private void loadSubjectsFromFile()
    {
        if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            FileInputStream fis = null;
            String result = null;
            try
            {

                try
                {
                    if( Utils.getContext() != null )
                    {
                        fis = Utils.getContext().openFileInput(fileName);
                        result = IOUtils.toString(fis, "UTF-8");
                    }
                    else
                    {
                        Log.e("BMB", "Context is null");
                    }
                } catch( FileNotFoundException e1 )
                {
                    Log.e(Utils.getContext().getString(R.string.log_tag), "Error opening subjects file: " + e1.getMessage());
                }

                if( fis != null )
                {
                    fis.close();
                }
            } catch( Exception e )
            {
                Log.e(Utils.getContext().getString(R.string.log_tag), "Error opening subjects file: " + e.getMessage());
            }

            try
            {
                if( result != null )
                {
                    JSONObject object = new JSONObject(result);
                    subjectsMap = SubjectJSONBuilder.deserializeSubjectMap(object);
                }
            } catch( JSONException e )
            {
                e.printStackTrace();
            }
        }
    }

    public void saveSubjectsToFile()
    {
        if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            if( !subjectsMap.isEmpty() )
            {
                JSONObject obj = SubjectJSONBuilder.serializeSubjectMap(subjectsMap);

                try
                {
                    FileOutputStream outputStream = Utils.getContext().openFileOutput(fileName, Context.MODE_PRIVATE);
                    outputStream.write(obj.toString().getBytes());
                    outputStream.close();
                } catch( Exception e )
                {
                    Log.e(Utils.getContext().getString(R.string.log_tag), "Error saving subjects file: " + e.getMessage());
                }
            }
        }
    }

    public List<Id> getFlashcardSetsForExam(Exam exam)
    {
        List<Id> setIds = new ArrayList<>();

        Subject examSubject = subjectsMap.get(exam.getSubjectId());
        if( exam.isCumulative() )
        {
            setIds.addAll(examSubject.getFlashcardSetIds());
        }
        else
        {
            Calendar previousExamDate = examSubject.getPreviousExamDate(exam);
            Calendar examDate = exam.getExamDate();
            for( Id setId : examSubject.getFlashcardSetIds() )
            {
                FlashcardSet set = FlashcardSetService.getInstance().getFlashcardSet(setId);
                if( set.getCreationDate().before(previousExamDate) && set.getCreationDate().after(examDate) )
                {
                    setIds.add(setId);
                }
            }
        }

        return setIds;
    }

    public Id addSubject(Subject subject)
    {
        subject.setId(IdService.mintID(new SubjectIdType()));
        subjectsMap.put(subject.getId(), subject);

        return subject.getId();
    }


    public Subject getSubject(Id id)
    {
        return subjectsMap.get(id);
    }

    public List<Subject> getSubjects()
    {
        ArrayList<Subject> subjects = new ArrayList<>();

        for( Map.Entry<Id, Subject> subjectEntry : subjectsMap.entrySet() )
        {
            subjects.add(subjectEntry.getValue());
        }

        return subjects;
    }

}

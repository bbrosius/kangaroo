package com.bmb.kangaroo.model.builder;

import android.util.Log;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExamJSONBuilder
{
    private final static String EXAM_LIST = "examList";
    private final static String ID = "id";
    private final static String EXAM_TITLE = "title";
    private final static String SUBJECT_ID = "subjectId";
    private final static String EXAM_DATE = "examDate";
    private final static String REMINDER_DATE = "reminderDate";
    private final static String IS_CUMULATIVE = "isCumulative";
    private final static String GRADE = "grade";
    private final static String GRADE_GOAL = "gradeGoal";

    public static JSONObject serializeExamMap(Map<Id, Exam> examMap)
    {
        JSONObject examListObject = new JSONObject();
        JSONArray examListArray = new JSONArray();
        for(Map.Entry<Id, Exam> entry : examMap.entrySet())
        {
            JSONObject examObj = serializeExam(entry.getValue());
            if( examObj != null )
            {
                examListArray.put(examObj);
            }
        }

        try
        {
            examListObject.put(EXAM_LIST, examListArray);
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return examListObject;
    }

    public static Map<Id, Exam> deserializeExamMap(JSONObject obj)
    {
        Map<Id, Exam> examMap = new HashMap<>();
        try
        {
            JSONArray exams = obj.getJSONArray(EXAM_LIST);
            for( int i = 0; i < exams.length(); i++ )
            {
                Exam exam = deserializeExam(exams.getJSONObject(i));
                if( exam != null )
                {
                    examMap.put(exam.getId(), exam);
                }
            }
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return examMap;
    }

    private static JSONObject serializeExam(Exam exam)
    {
        JSONObject examObj = new JSONObject();
        try
        {
            DateFormat dateFormat = new SimpleDateFormat(Utils.getContext().getString(R.string.date_format), Locale.getDefault());

            examObj.put(ID, exam.getId().toString());
            examObj.put(EXAM_TITLE, exam.getTitle());
            if( exam.getExamDate() != null )
            {
                examObj.put(EXAM_DATE, dateFormat.format(exam.getExamDate().getTime()));
            }
            if( exam.getReminderDate() != null )
            {
                examObj.put(REMINDER_DATE, dateFormat.format(exam.getReminderDate().getTime()));
            }
            examObj.put(GRADE_GOAL, exam.getGradeGoal());
            examObj.put(SUBJECT_ID, exam.getSubjectId());
            examObj.put(GRADE, exam.getGradeGoal());
            examObj.put(IS_CUMULATIVE, exam.isCumulative());
        }
        catch( JSONException e )
        {
            e.printStackTrace();
        }

        return examObj;
    }

    private static Exam deserializeExam(JSONObject obj)
    {
        Exam exam = null;
        String title = null;
        Id examId = null;
        int gradeGoal = -1;
        int grade = -1;
        Calendar examDate = null;
        Calendar reminderDate = null;
        Id subjectId = null;
        boolean isCumulative = false;

        try
        {
            if( obj.has(ID) )
            {
                examId = IdService.resolveId(obj.getString(ID));
            }

            if( obj.has(EXAM_TITLE) )
            {
                title = obj.getString(EXAM_TITLE);
            }

            if( obj.has(GRADE_GOAL) )
            {
                gradeGoal = obj.getInt(GRADE_GOAL);
            }

            if( obj.has(GRADE) )
            {
                grade = obj.getInt(GRADE);
            }

            if( obj.has(IS_CUMULATIVE) )
            {
                isCumulative = obj.getBoolean(IS_CUMULATIVE);
            }

            if( obj.has(EXAM_DATE) )
            {
                DateFormat dateFormat = new SimpleDateFormat(Utils.getContext().getString(R.string.date_format), Locale.getDefault());
                String dateString = obj.getString(EXAM_DATE);
                examDate = new GregorianCalendar();
                if( !dateString.isEmpty() )
                {
                    examDate.setTime(dateFormat.parse(dateString));
                }
            }

            if( obj.has(REMINDER_DATE) )
            {
                DateFormat dateFormat = new SimpleDateFormat(Utils.getContext().getString(R.string.date_format), Locale.getDefault());
                String dateString = obj.getString(REMINDER_DATE);
                reminderDate = new GregorianCalendar();
                if( !dateString.isEmpty() )
                {
                    reminderDate.setTime(dateFormat.parse(dateString));
                }
            }

            if( obj.has(SUBJECT_ID) )
            {
                subjectId = IdService.resolveId(obj.getString(SUBJECT_ID));
            }


        }
        catch( JSONException e)
        {
            Log.e(Utils.getContext().getString(R.string.log_tag), "Error getting subject id");
        }
        catch( ParseException e )
        {
            e.printStackTrace();
        }

        if( examId != null && title != null && examDate != null )
        {
            exam = new Exam(title);
            exam.setId(examId);
            exam.setGradeGoal(gradeGoal);
            exam.setGrade(grade);
            exam.setExamDate(examDate);
            exam.setReminderDate(reminderDate);
            exam.setIsCumulative(isCumulative);
            exam.setSubjectId(subjectId);
        }
        return exam;
    }
}

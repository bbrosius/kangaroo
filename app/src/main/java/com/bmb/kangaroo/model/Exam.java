package com.bmb.kangaroo.model;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.utils.Id;
import com.bmb.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Representation of an Exam.
 */
public class Exam
{
    private Id id;
    public String title;
    private Id subjectId;
    private Calendar examDate;
    private Calendar reminderDate;
    private boolean isCumulative;
    private int grade = -1;
    private int gradeGoal = -1;

    public Exam(String title)
    {
        this.title = title;
    }

    public Exam(String title, Calendar examDate, Calendar reminderDate, Id subjectId, int grade, int gradeGoal, boolean isCumulative)
    {
        this.title = title;
        this.examDate = examDate;
        this.reminderDate = reminderDate;
        this.subjectId = subjectId;
        this.isCumulative = isCumulative;
        this.grade = grade;
        this.gradeGoal = gradeGoal;
    }

    public void setId(Id id)
    {
        this.id = id;
    }

    public Id getId()
    {
        return id;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    public int getGrade()
    {
        return grade;
    }

    public void setGrade(int grade)
    {
        this.grade = grade;
    }

    public int getGradeGoal()
    {
        return gradeGoal;
    }

    public void setGradeGoal(int gradeGoal)
    {
        this.gradeGoal = gradeGoal;
    }

    public void setSubjectId(Id id)
    {
        this.subjectId = id;
    }

    public Id getSubjectId()
    {
        return subjectId;
    }

    public void setExamDate(Calendar date)
    {
        examDate = date;
    }

    public Calendar getExamDate()
    {
        return examDate;
    }

    public void setReminderDate(Calendar date)
    {
        reminderDate = date;
    }

    public Calendar getReminderDate()
    {
        return reminderDate;
    }

    public void setIsCumulative(boolean isCumulative)
    {
        this.isCumulative = isCumulative;
    }

    public boolean isCumulative()
    {
        return isCumulative;
    }

    @Override
    public String toString()
    {
        DateFormat dateFormat = new SimpleDateFormat(Utils.getContext().getString(R.string.date_format), Locale.getDefault());

        String exam = id + "|" + title + "|" + subjectId + "|" + dateFormat.format(examDate.getTime()) + "|";
        if( reminderDate != null )
        {
            exam += dateFormat.format(reminderDate.getTime());
        }

        exam += "|" + isCumulative + "|" + gradeGoal + "|" + grade;

        return exam;
    }
}

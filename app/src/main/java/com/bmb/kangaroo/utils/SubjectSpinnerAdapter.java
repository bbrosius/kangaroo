package com.bmb.kangaroo.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Subject;

import java.util.List;

/**
 * Adapter that controls the subject spinner that appears in the actionbar of the SubjectListActivity and ImportFlashcardActivity.
 */
public class SubjectSpinnerAdapter extends ArrayAdapter<Subject> implements SpinnerAdapter
{
    final Context context;
    private List<Subject> subjects;


    public SubjectSpinnerAdapter(Context context, List<Subject> subjects)
    {
        super(context, R.layout.title_spinner_layout, subjects);
        this.subjects = subjects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        SubjectHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.title_spinner_layout, parent, false);

            holder = new SubjectHolder();
            holder.subjectTitle = (TextView)row.findViewById(R.id.subject_title);

            row.setTag(holder);
        }
        else
        {
            holder = (SubjectHolder)row.getTag();
        }

        Subject subject = null;
        if( position < subjects.size() )
        {
            subject = subjects.get(position);
        }
        if( subject != null && subject.getTitle() != null && !subject.getTitle().isEmpty() )
        {
            holder.subjectTitle.setText(subject.getTitle());
        }

        return row;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        SubjectHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(R.layout.title_spinner_layout, parent, false);

            holder = new SubjectHolder();
            holder.subjectTitle = (TextView)row.findViewById(R.id.subject_title);

            row.setTag(holder);
        }
        else
        {
            holder = (SubjectHolder)row.getTag();
        }

        Subject subject = null;
        if( position < subjects.size() )
        {
            subject = subjects.get(position);
        }
        if( subject != null && subject.getTitle() != null && !subject.getTitle().isEmpty() )
        {
            holder.subjectTitle.setText(subject.getTitle());
        }

        return row;
    }

    private class SubjectHolder
    {
        TextView subjectTitle;
    }

    public void setSubjects(List<Subject> subjects)
    {
        this.subjects = subjects;
    }

    public List<Subject> getSubjects()
    {
        return subjects;
    }
}

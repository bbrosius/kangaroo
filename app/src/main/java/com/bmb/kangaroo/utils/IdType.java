package com.bmb.kangaroo.utils;

public interface IdType
{
	String getIdPrefix();
}
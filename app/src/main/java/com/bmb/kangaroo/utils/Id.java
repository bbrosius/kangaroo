package com.bmb.kangaroo.utils;

public class Id
{
	private final IdType type;
	private int uid;
	
	public Id(IdType type)
	{
		this.type = type;
	}
	
	public Id(IdType type, int uid)
	{
		this.type = type;
		this.uid = uid;
	}
	
	public void setUid(int uid)
	{
		this.uid = uid;
	}
	
	public int getUid()
	{
		return uid;
	}
	
	public IdType getType()
	{
		return type;
	}
	
	@Override
	public String toString()
	{
		return type.getIdPrefix() + ":" + uid;
	} 
	
	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null)
		{
			return false;
		}

		if( o instanceof Id )
		{
			Id other = (Id) o;
			if( type == null )
			{
				if( other.type != null )
				{
					return false;
				}
			}
			else if( !type.equals(other.type) )
			{
				return false;
			}
			return uid == other.uid;
		}

		return false;

	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + uid;

		return result;
	}
}
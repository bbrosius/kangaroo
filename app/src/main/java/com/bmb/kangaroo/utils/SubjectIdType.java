package com.bmb.kangaroo.utils;

public class SubjectIdType implements IdType
{
	private final String PREFIX = "subject";
	
	@Override
	public String getIdPrefix() {
		return PREFIX;
	}
	
	@Override
	public boolean equals(Object o)
	{
        if (o == null)
        {
            return false;
        }

		if (this == o)
		{
			return true;
		}

		if( o instanceof IdType )
		{
			IdType other = (IdType) o;

			return PREFIX.equalsIgnoreCase(other.getIdPrefix());
		}

		return false;

	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + PREFIX.hashCode();

		return result;
	}
	
}
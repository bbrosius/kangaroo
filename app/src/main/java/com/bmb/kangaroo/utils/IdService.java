package com.bmb.kangaroo.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.bmb.kangaroo.R;
import com.bmb.utils.Utils;

import java.io.*;
import java.util.ArrayList;

public class IdService
{
	private final static String idFileName = "generatedId";
	private final static ArrayList<IdType> idTypes = new ArrayList<>();
	
	//TODO: Fix this so these aren't hard coded into the service. 
	static
	{
		idTypes.add(new SubjectIdType());
		idTypes.add(new ExamIdType());
        idTypes.add(new FlashcardSetIdType());
	}
	
	public static Id mintID(IdType type)
	{
		int uid = getNextUid();
		return new Id(type, uid);
	}
	
	private static int getNextUid()
	{
		int newUid = 0;
		if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
				== PackageManager.PERMISSION_GRANTED)
		{
			FileInputStream fis = null;
			try
			{
				fis = Utils.getContext().openFileInput(idFileName);
			} catch( FileNotFoundException e1 )
			{
				Log.e(Utils.getContext().getString(R.string.log_tag), "Error opening id file: " + e1.getMessage());
			}

			BufferedReader br;
			if( fis != null )
			{
				try
				{
					br = new BufferedReader(new InputStreamReader(fis));

					int lastUid;

					String line;
					line = br.readLine();
					br.close();
					lastUid = Integer.parseInt(line);
					newUid = lastUid + 1;

				} catch( Exception e )
				{
					Log.d(Utils.getContext().getString(R.string.log_tag), "Id file couldn't be read starting id count at 0" + e.getMessage());
					newUid = 0;
				}
			}
			setLastUid(newUid);
		}
		return newUid;
	}
	
	private static void setLastUid(int uid)
	{
		if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
				== PackageManager.PERMISSION_GRANTED)
		{
			try
			{
				FileOutputStream outputStream = Utils.getContext().openFileOutput(idFileName, Context.MODE_PRIVATE);
				outputStream.write(String.valueOf(uid).getBytes());
				outputStream.close();
			} catch( Exception e )
			{
				Log.e(Utils.getContext().getString(R.string.log_tag), "Error saving id file: " + e.getMessage());
			}
		}
	}
	
	public static Id resolveId(String idString)
	{
		Id id = null;
		if(idString != null && !idString.isEmpty())
		{
			String[] idParts = idString.split(":");
			
			for(IdType type : idTypes)
			{
				if(idParts[0].equalsIgnoreCase(type.getIdPrefix()))
				{
					id = new Id( type, Integer.valueOf(idParts[1]) );
				}
			}
		}
		return id;
	}
}
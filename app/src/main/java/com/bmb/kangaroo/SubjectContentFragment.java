package com.bmb.kangaroo;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bmb.kangaroo.dialogs.AddFlashcardSetDialog;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.SubjectChild;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.quizlet.QuizletActivity;
import com.bmb.kangaroo.utils.FlashcardSetIdType;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.kangaroo.utils.SubjectIdType;
import com.bmb.kangaroo.views.NotePaperLayout;
import com.bmb.kangaroo.views.NotePaperTextView;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class SubjectContentFragment extends ListFragment
{
    /**
     * The subject this fragment is presenting notes for.
     */
    private Subject subject;

    /**
     * The next exam for displaying the countdown clock if it's enabled.
     */
    private Exam nextExam;

    protected final static int NO_SUBJECTS_ID = -888;
    protected final static int NO_FLASHCARD_SETS_ID = -999;

    private TextView examDate;
    private LinearLayout examHeaderLayout;
    private CountDownTimer nextExamCountDownTimer;

    @Override
    public void onListItemClick(ListView l, View setCard, int position, long id)
    {
        super.onListItemClick(l, setCard, position, id);
        Id childId = subjectChildIds.get(position);
        if( childId.getType() instanceof FlashcardSetIdType)
        {
            NotePaperTextView setTitle = (NotePaperTextView) setCard.findViewById(R.id.note_list_title);
            ImageView quizletImage = (ImageView) setCard.findViewById(R.id.set_from_quizlet);
            NotePaperTextView cardcount = (NotePaperTextView) setCard.findViewById(R.id.note_list_text);
            callBacks.onFlashcardSetSelected(childId, (NotePaperLayout) setCard, setTitle, quizletImage, cardcount);
        }
    }


    private ArrayList<Id> subjectChildIds;

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private SubjectChildSelectedCallback callBacks = childCallback;

    public SubjectContentFragment()
    {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RelativeLayout noteListLayout = (RelativeLayout) inflater.inflate(R.layout.subject_content_layout, null);

        boolean showCountdown = false;
        if( subject != null )
        {
            SharedPreferences pref = getActivity().getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
            showCountdown = pref.getBoolean(getString(R.string.show_next_exam_countdown_key), false);
        }

        nextExam = null;

        if(subject != null && subject.getNextExam() != null)
        {
            nextExam = subject.getNextExam();
        }

        if( nextExam != null && showCountdown)
        {
            examHeaderLayout = (LinearLayout) noteListLayout.findViewById(R.id.exam_header);
            examHeaderLayout.setVisibility(View.VISIBLE);
            TextView title = (TextView) examHeaderLayout.findViewById(R.id.exam_header_title);
            title.setText(getString(R.string.next_exam) + " " + nextExam.getTitle());

            Calendar nextExamDate = nextExam.getExamDate();
            long millisInTheFuture = nextExamDate.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
            examDate = (TextView) examHeaderLayout.findViewById(R.id.exam_header_date);
            parseTimeLeft(millisInTheFuture);

            examHeaderLayout.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent studyIntent = new Intent(getActivity(), FlashcardActivity.class);
                    studyIntent.putExtra(getString(R.string.study_id), nextExam.getId().toString());
                    studyIntent.putExtra(getString(R.string.flashcard_calling_activity), FlashcardActivity.SUBJECT_LIST);
                    startActivity(studyIntent);
                }
            });

            nextExamCountDownTimer = new CountDownTimer(millisInTheFuture, 1000)
            {
                @Override
                public void onFinish() {
                    examDate.setText(DateUtils.formatElapsedTime(0));
                }

                @Override
                public void onTick(long millisUntilFinished)
                {
                    parseTimeLeft(millisUntilFinished);
                }
            };

            nextExamCountDownTimer.start();
        }

        final FloatingActionsMenu subjectContentFloatingMenu = (FloatingActionsMenu) noteListLayout.findViewById(R.id.subject_content_add_menu);

        if( SubjectService.getInstance().getSubjects().isEmpty() )
        {
            subjectContentFloatingMenu.setVisibility(View.GONE);
        }

        FloatingActionButton addNewFlashcardSetButton = (FloatingActionButton) noteListLayout.findViewById(R.id.add_new_flashcard_set_button);
        addNewFlashcardSetButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogFragment flashcardSetDialog = new AddFlashcardSetDialog();
                flashcardSetDialog.show( getFragmentManager(), getString(R.string.add_flashcard_set_dialog_tag) );
                subjectContentFloatingMenu.collapse();
            }
        });

        FloatingActionButton importFromQuizletSetButton = (FloatingActionButton) noteListLayout.findViewById(R.id.import_from_quizlet_button);
        importFromQuizletSetButton.setOnClickListener( new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent quizletIntent = new Intent(getActivity(), QuizletActivity.class);
                quizletIntent.putExtra(getString(R.string.study_subject_id), SubjectService.getInstance().getCurrentSubjectId().toString());
                Exam nextExam = SubjectService.getInstance().getCurrentSubject().getNextExam();
                if( nextExam != null )
                {
                    quizletIntent.putExtra(getString(R.string.study_exam_id), nextExam.getId().toString());
                }
                startActivity(quizletIntent);
                subjectContentFloatingMenu.collapse();
            }
        });

        return noteListLayout;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        if( nextExamCountDownTimer != null )
        {
            nextExamCountDownTimer.cancel();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setPadding(16, 16, 16, 16);
        getListView().setDivider(getResources().getDrawable(R.color.transparent_color));
        getListView().setDividerHeight(16);
    }

    public void setCountdownClockVisibility(boolean visible)
    {
        if( !visible )
        {
            if( examHeaderLayout != null )
            {
                examHeaderLayout.setVisibility(View.GONE);
                examHeaderLayout.postInvalidate();
            }
        }
        else
        {
            if( examHeaderLayout != null )
            {
                examHeaderLayout.setVisibility(View.VISIBLE);
                examHeaderLayout.postInvalidate();
            }
            else
            {
                if(subject != null && subject.getNextExam() != null)
                {
                    nextExam = subject.getNextExam();
                }

                if( nextExam != null && getView() != null )
                {
                    examHeaderLayout = (LinearLayout) getView().findViewById(R.id.exam_header);
                    TextView title = (TextView) examHeaderLayout.findViewById(R.id.exam_header_title);
                    title.setText(getString(R.string.next_exam) + " " + nextExam.getTitle());

                    Calendar nextExamDate = nextExam.getExamDate();
                    long millisInTheFuture = nextExamDate.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
                    examDate = (TextView) examHeaderLayout.findViewById(R.id.exam_header_date);
                    parseTimeLeft(millisInTheFuture);

                    examHeaderLayout.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            Intent studyIntent = new Intent(getActivity(), FlashcardActivity.class);
                            studyIntent.putExtra(getString(R.string.study_id), nextExam.getId().toString());
                            studyIntent.putExtra(getString(R.string.flashcard_calling_activity), FlashcardActivity.SUBJECT_LIST);
                            startActivity(studyIntent);
                        }
                    });

                    nextExamCountDownTimer = new CountDownTimer(millisInTheFuture, 1000)
                    {
                        @Override
                        public void onFinish() {
                            examDate.setText(DateUtils.formatElapsedTime(0));
                        }

                        @Override
                        public void onTick(long millisUntilFinished) {
                            parseTimeLeft(millisUntilFinished);
                        }
                    };

                    nextExamCountDownTimer.start();

                    examHeaderLayout.postInvalidate();
                }
            }
        }
    }

    private void parseTimeLeft(long millisUntilFinished)
    {
        StringBuilder time = new StringBuilder();
        time.setLength(0);
        // Use days if appropriate
        if(millisUntilFinished > DateUtils.DAY_IN_MILLIS) {
            long count = millisUntilFinished / DateUtils.DAY_IN_MILLIS;
            if(count > 1)
                time.append(count).append(" ").append(getString(R.string.days)).append(" ");
            else
                time.append(count).append(" ").append(getString(R.string.day)).append(" ");

            millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
        }

        time.append(DateUtils.formatElapsedTime(Math.round(millisUntilFinished / 1000d)));
        examDate.setText(time.toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if( getArguments() != null && getArguments().containsKey(getString(R.string.study_subject_id)) )
        {
            Id id = IdService.resolveId(getArguments().getString(getString(R.string.study_subject_id)));
            if( id.getType() instanceof SubjectIdType)
            {
                subject = SubjectService.getInstance().getSubject(id);
            }
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        subjectChildIds = new ArrayList<>();

        if( subject != null )
        {
            if( subject.getChildIds().size() > 0 )
            {
                subjectChildIds = subject.getChildIds();
            }
            else
            {
                subjectChildIds.add(new Id(new FlashcardSetIdType(), NO_FLASHCARD_SETS_ID));
            }
        }
        else
        {
            subjectChildIds.add(new Id(new FlashcardSetIdType(), NO_SUBJECTS_ID));
        }

        setListAdapter( new SubjectChildAdapter(getActivity(), subjectChildIds) );
    }

    public void newChildAdded(SubjectChild child)
    {
        if(subjectChildIds.size() == 1)
        {
            if(subjectChildIds.get(0).getUid() == NO_FLASHCARD_SETS_ID)
            {
                subjectChildIds.remove(0);
            }
        }
        subjectChildIds.add(child.getId());
        ListAdapter listAdapter = getListAdapter();
        ((BaseAdapter) listAdapter).notifyDataSetChanged();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //when resuming check to see if sets were added from Quizlet
        if( subject != null && !subject.getChildIds().isEmpty() )
        {
            subject = SubjectService.getInstance().getSubject(subject.getId());
            if( subject.getChildIds().size() != subjectChildIds.size() || (subjectChildIds.size() == 1 && subjectChildIds.get(0).getUid() == NO_FLASHCARD_SETS_ID))
            {
                subjectChildIds = subject.getChildIds();
                setListAdapter(new SubjectChildAdapter(getActivity(), subjectChildIds));
            }

            ((ArrayAdapter)getListAdapter()).notifyDataSetChanged();
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof SubjectChildSelectedCallback))
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callBacks = (SubjectChildSelectedCallback) activity;
    }

    /**
     * Callback that notifies callers when a subject child object has been selected.
     */
    public interface SubjectChildSelectedCallback
    {
        /**
         * Callback for when an item has been selected.
         */
        void onFlashcardSetSelected(Id flashcardSetId, NotePaperLayout setCard, NotePaperTextView setTitle, ImageView quizletImage, NotePaperTextView cardCount);
    }

    /**
     * A dummy implementation of the {@code SubjectChildSelectedCallback} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private final static SubjectChildSelectedCallback childCallback = new SubjectChildSelectedCallback()
    {
        @Override
        public void onFlashcardSetSelected(Id flashcardSetId, NotePaperLayout setCard, NotePaperTextView setTitle, ImageView quizletImage, NotePaperTextView cardCount)
        {
        }
    };

    private class SubjectChildAdapter extends ArrayAdapter<Id>
    {
        final Context context;
        List<Id> childIds = null;

        public SubjectChildAdapter(Context context, List<Id> ids)
        {
            super(context, R.layout.note_list_item, ids);
            this.context = context;
            this.childIds = ids;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            FlashcardSetHolder holder;

            if(row == null)
            {
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(R.layout.note_list_item, parent, false);

                holder = new FlashcardSetHolder();
                holder.childTitle = (TextView)row.findViewById(R.id.note_list_title);
                holder.childSummary = (TextView)row.findViewById(R.id.note_list_text);
                holder.isQuizletSet = (ImageView)row.findViewById(R.id.set_from_quizlet);
                holder.deleteItem = (ImageButton)row.findViewById(R.id.delete_item);

                row.setTag(holder);
            }
            else
            {
                holder = (FlashcardSetHolder)row.getTag();
            }

            final Id childId = childIds.get(position);

            if( childId.getUid() == NO_FLASHCARD_SETS_ID )
            {
                holder.childTitle.setText(R.string.first_note_title);
                holder.childSummary.setText(R.string.first_note_text);
                holder.isQuizletSet.setVisibility(View.GONE);
                holder.deleteItem.setVisibility(View.GONE);
            }
            else if( childId.getUid() == NO_SUBJECTS_ID )
            {
                holder.childTitle.setText(R.string.welcome);
                holder.childSummary.setText(R.string.welcome_text);
                holder.isQuizletSet.setVisibility(View.GONE);
                holder.deleteItem.setVisibility(View.GONE);
            }
            else
            {
                FlashcardSet set = FlashcardSetService.getInstance().getFlashcardSet(childId);
                if( set != null )
                {
                    holder.childTitle.setText(set.getTitle());
                    holder.childSummary.setText(getString(R.string.flashcards) + " " + set.getFlashcardCount());
                    if( set.isQuizletSet() )
                    {
                        holder.isQuizletSet.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        holder.isQuizletSet.setVisibility(View.GONE);
                    }

                    holder.deleteItem.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                    context);

                            // set title
                            alertDialogBuilder.setTitle(getString(R.string.delete_flashcard_set));

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage(getString(R.string.confirm_delete))
                                    .setCancelable(false)
                                    .setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            FlashcardSetService.getInstance().deleteFlashcardSet(childId);
                                            subject.removeFlashcardSet(childId);
                                            childIds.remove(position);
                                            notifyDataSetChanged();
                                            dialog.dismiss();
                                        }
                                    })
                                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        }
                    });
                }
            }

            return row;
        }

        private class FlashcardSetHolder
        {
            TextView childTitle;
            TextView childSummary;
            ImageView isQuizletSet;
            ImageButton deleteItem;
        }
    }

}
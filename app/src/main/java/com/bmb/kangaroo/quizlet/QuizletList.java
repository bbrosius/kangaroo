package com.bmb.kangaroo.quizlet;

import com.google.api.client.util.Key;

import java.util.List;

/**
 * Class for handling quizlet list response
 */
public class QuizletList
{
    @Key("total_results")
    public int totalSets;

    @Key("total_pages")
    public int totalPages;

    @Key("page")
    public int page;

    @Key("image_set_count")
    public int imageSetCount;

    @Key("sets")
    public List<QuizletFlashcardSet> flashcardSets;
}

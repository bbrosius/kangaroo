package com.bmb.kangaroo.quizlet;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.SubjectPreferenceActivity;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.oauth2.OauthLoginFragment;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson.JacksonFactory;

import java.util.ArrayList;

/**
 * Activity for importing and exporting from quizlet.
 */
public class QuizletActivity extends AppCompatActivity implements QuizletFragment.QuizletQueryResult, OauthLoginFragment.OauthLoginFinished, QuizletListFragment.QuizletSetSelectedCallback, QuizletExportFragment.QuizletExportResult, SharedPreferences.OnSharedPreferenceChangeListener
{
    private static boolean showingSetList = false;
    private ArrayList<Integer> selectedSets;
    private final static String BASE_SET_CALL = "https://api.quizlet.com/2.0/sets/";
    private String accessToken;
    private Id examId = null;
    private Id subjectId = null;
    private Id noteId = null;
    private String exportSetId = null;
    private boolean exportMode = false;

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private HttpRequestFactory requestFactory;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        SharedPreferences pref = getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
        pref.registerOnSharedPreferenceChangeListener(this);
        requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
            public void initialize(HttpRequest request) {
                request.setParser(new JsonObjectParser(JSON_FACTORY));
            }
        });

        if( pref.getBoolean(getString(R.string.light_theme_key), false) )
        {
            getApplication().setTheme(R.style.FFMainTheme_Light);
            setTheme(R.style.FFMainTheme_Light);
        }
        else
        {
            getApplication().setTheme(R.style.FFMainTheme);
            setTheme(R.style.FFMainTheme);
        }
        super.onCreate(savedInstanceState);

        if( getIntent().hasExtra(getString(R.string.study_note_id)) )
        {
            noteId = IdService.resolveId(getIntent().getStringExtra(getString(R.string.study_note_id)));
        }

        if( getIntent().hasExtra(getString(R.string.study_subject_id)) )
        {
            subjectId = IdService.resolveId(getIntent().getStringExtra(getString(R.string.study_subject_id)));
        }

        if( getIntent().hasExtra(getString(R.string.study_exam_id)) )
        {
            examId = IdService.resolveId(getIntent().getStringExtra(getString(R.string.study_exam_id)));
        }

        if( getIntent().hasExtra(getString(R.string.quizlet_export_arg)) )
        {
            exportMode = true;
            exportSetId = getIntent().getStringExtra(getString(R.string.quizlet_export_arg));
        }

        setContentView(R.layout.quizlet_main_layout);

        Toolbar toolBar = (Toolbar) findViewById(R.id.quizlet_toolbar);
        setSupportActionBar(toolBar);

        accessToken = pref.getString(getString(R.string.quizlet_token_key), "");

        selectedSets = new ArrayList<>();

        if( exportMode )
        {
            FragmentManager fragmentManager = getSupportFragmentManager();
            QuizletExportFragment exportFragment = (QuizletExportFragment) fragmentManager.findFragmentByTag(getString(R.string.quizlet_export_tag));
            if( exportFragment == null )
            {
                exportFragment = new QuizletExportFragment();
                Bundle arguments = new Bundle();
                arguments.putString(getString(R.string.quizlet_export_arg), exportSetId);
                exportFragment.setArguments(arguments);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.quizlet_content, exportFragment, getString(R.string.quizlet_export_tag))
                        .addToBackStack("export")
                        .commit();
            }
        }
        else if( !showingSetList )
        {
            QuizletFragment mainScreen = new QuizletFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.quizlet_content, mainScreen)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.quizlet_import_menu, menu);

        MenuItem importSets = menu.findItem(R.id.import_quizlet_set);
        if( importSets != null )
        {
            importSets.setVisible(showingSetList);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch( item.getItemId() )
        {
            case R.id.import_quizlet_set:
                new ImportQuizletTask().execute("");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void queryResult(QuizletMode mode, QuizletList result, String callUrl)
    {
        QuizletListFragment resultList = (QuizletListFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.quizlet_list_tag));
        if( resultList == null )
        {
            resultList = new QuizletListFragment();
            resultList.setQuizletResultList(result);

            Bundle arguments = new Bundle();
            arguments.putString(QuizletListFragment.QUIZLET_LIST_MODE, mode.getMode());
            arguments.putString(QuizletListFragment.QUIZLET_CALL_URL, callUrl);
            resultList.setArguments(arguments);
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.quizlet_content, resultList, getString(R.string.quizlet_list_tag))
                    .addToBackStack("results")
                    .commit();

            showingSetList = true;
        }
        invalidateOptionsMenu();
    }

    @Override
    public void loginRequest()
    {
        OauthLoginFragment loginFragment = new OauthLoginFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.quizlet_content, loginFragment)
                .commit();
    }

    @Override
    public void exportComplete()
    {
        this.finish();
    }

    @Override
    public void oauthFinished(boolean loginSuccessful)
    {
        if( exportMode )
        {
            QuizletExportFragment exportFragment = new QuizletExportFragment();
            Bundle arguments = new Bundle();
            arguments.putString(getString(R.string.quizlet_export_arg), exportSetId);
            exportFragment.setArguments(arguments);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.quizlet_content, exportFragment)
                    .addToBackStack("content")
                    .commit();
        }
        else
        {
            QuizletFragment mainScreen = new QuizletFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.quizlet_content, mainScreen)
                    .addToBackStack("content")
                    .commit();
        }
    }

    @Override
    public void setSelected(int selectedSet)
    {
        if( !selectedSets.contains(selectedSet) )
        {
            selectedSets.add(selectedSet);
        }
    }

    @Override
    public void deselectSet(int deselectedSet)
    {
        selectedSets.remove((Integer) deselectedSet);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if( key.equalsIgnoreCase(getString(R.string.quizlet_token_key)) )
        {
            accessToken = sharedPreferences.getString(getString(R.string.quizlet_token_key), "");
        }
    }

    private class ImportQuizletTask extends AsyncTask<String, String, Integer>
    {
        private final ProgressDialog importSetsDialog;
        private int addedSets = 0;
        private String errorMessage = null;

        public ImportQuizletTask()
        {
            importSetsDialog = new ProgressDialog(QuizletActivity.this);
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            importSetsDialog.setTitle(getString(R.string.importing_quizlet_sets));
            importSetsDialog.setIndeterminate(false);
            importSetsDialog.setMax(selectedSets.size());
            importSetsDialog.show();
        }

        @Override
        protected void onProgressUpdate(String... importedSets)
        {
            super.onProgressUpdate(importedSets);
            importSetsDialog.setProgress(addedSets);
            importSetsDialog.setMessage(getString(R.string.importing) + " " + importedSets[0]);
        }

        @Override
        protected Integer doInBackground(String... setUrls)
        {
            try
            {
                Subject currentSubject = SubjectService.getInstance().getSubject(subjectId);
                for( int setId : selectedSets )
                {
                    String setUrl = buildSetUrl(setId);
                    GenericUrl url = new GenericUrl(setUrl);
                    HttpRequest setRequest = requestFactory.buildGetRequest(url);

                    HttpResponse response = setRequest.execute();
                    if( response.getStatusCode() != 200 )
                    {
                        errorMessage = "Error importing quizlet sets: " + response.getStatusMessage();
                    }
                    else
                    {
                        QuizletFlashcardSet quizletSet = response.parseAs(QuizletFlashcardSet.class);
                        FlashcardSet importedSet = quizletSet.asFlashcardSet(subjectId, noteId, examId);
                        importedSet.setisQuizletSet(true);
                        publishProgress(importedSet.getTitle());
                        currentSubject.addFlashcardSet(importedSet.getId());
                        addedSets++;
                    }
                }
            }
            catch( Exception e )
            {
                Log.e(getString(R.string.log_tag), "Error reading quizlet json response " + e.getMessage());
                errorMessage = "Error reading quizlet json response " + e.getMessage();
            }

            return addedSets;
        }

        @Override
        protected void onPostExecute(Integer setsAdded)
        {
            super.onPostExecute(setsAdded);
            if( importSetsDialog.isShowing() )
            {
                importSetsDialog.dismiss();
            }

            showingSetList = false;
            if( errorMessage != null && !errorMessage.isEmpty() )
            {
                Toast.makeText(QuizletActivity.this, errorMessage, Toast.LENGTH_LONG).show();
            }
            QuizletActivity.this.finish();
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if(showingSetList)
        {
            showingSetList = false;
        }
    }

    private String buildSetUrl(int setId) {
        StringBuilder sb = new StringBuilder();
        sb.append( BASE_SET_CALL );

        sb.append(setId);

        sb.append("?access_token=");
        if( accessToken != null && !accessToken.isEmpty() )
        {
            sb.append(accessToken);
        }

        return sb.toString();
    }
}
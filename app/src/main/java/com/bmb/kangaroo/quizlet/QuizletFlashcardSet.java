package com.bmb.kangaroo.quizlet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.utils.Id;
import com.google.api.client.util.Key;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class QuizletFlashcardSet
{
    @Key("title")
    public String setTitle;

    @Key("term_count")
    public int termCount;

    @Key("id")
    public int setId;

    @Key("terms")
    public List<QuizletFlashcard> flashcards;

    public static class QuizletFlashcard
    {
        @Key("term")
        public String term;

        @Key("definition")
        public String definition;

        @Key("image")
        public QuizletImage image;
    }

    public static class QuizletImage
    {
        @Key("url")
        public String imageUrl;
    }

    public FlashcardSet asFlashcardSet(Id subjectId, Id noteId, Id examId)
    {
        FlashcardSet flashcardSet = null;

        if( setTitle != null && !setTitle.isEmpty() )
        {
            flashcardSet = new FlashcardSet(setTitle);
            FlashcardSetService.getInstance().addFlashcardSet(flashcardSet);

            if( flashcards != null && flashcards.size() > 0 )
            {
                FlashcardDAO flashcardDao = new FlashcardDAO();
                flashcardDao.open();
                for( int i = 0; i < flashcards.size(); i++ )
                {
                    QuizletFlashcard flashcard = flashcards.get(i);
                    if( flashcard != null )
                    {
                        String term = "";
                        String definition = "";

                        int image = 0;
                        if( flashcard.image != null && flashcard.image.imageUrl != null && !flashcard.image.imageUrl.isEmpty()
                                && !flashcard.image.imageUrl.equalsIgnoreCase("null"))
                        {
                            //Get image from quizlet
                            FileOutputStream out = null;

                            try {
                                InputStream in = new java.net.URL(flashcard.image.imageUrl).openStream();
                                Bitmap cardImage = BitmapFactory.decodeStream(in);

                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
                                String imageFileName = "FLASHCARD_" + timeStamp;
                                File storageDir = Environment.getExternalStoragePublicDirectory(
                                        Environment.DIRECTORY_PICTURES);
                                File imageFile = File.createTempFile(
                                        imageFileName,  /* prefix */
                                        ".jpg",         /* suffix */
                                        storageDir      /* directory */
                                );

                                //Get the file URI
                                term = Uri.fromFile(imageFile).toString();

                                out = new FileOutputStream(imageFile);
                                cardImage.compress(Bitmap.CompressFormat.PNG, 100, out);

                                image = 1;
                            } catch (Exception e) {
                                Log.e("BMB", e.getMessage());
                                e.printStackTrace();
                            } finally {
                                try
                                {
                                    if( out != null )
                                    {
                                        out.close();
                                    }
                                }
                                catch( IOException e )
                                {
                                    e.printStackTrace();
                                }
                            }
                        }

                        if( image == 0 && flashcard.term != null && !flashcard.term.isEmpty() )
                        {
                            term = flashcard.term;
                        }

                        if( flashcard.definition != null && !flashcard.definition.isEmpty() )
                        {
                            definition = flashcard.definition;
                        }
                        else if( image == 1 && flashcard.term != null && !flashcard.term.isEmpty() )
                        {
                            definition = flashcard.term;
                        }

                        if( !term.isEmpty() && !definition.isEmpty() )
                        {
                            long flashcardId = flashcardDao.addFlashcard(subjectId, examId, flashcardSet.getId(), null, term, definition, image);
                            flashcardSet.addFlashcardId(flashcardId);
                        }
                    }
                }
                flashcardDao.close();
            }
        }
        return flashcardSet;
    }
}

package com.bmb.kangaroo.quizlet;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bmb.kangaroo.R;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson.JacksonFactory;

import java.util.ArrayList;
import java.util.List;

public class QuizletListFragment extends ListFragment implements AbsListView.OnScrollListener
{
    public final static String QUIZLET_LIST_MODE = "quizlet_list_mode";
    public final static String QUIZLET_CALL_URL = "quizlet_call_url";

    //Parameters for handling search results
    private int totalPages = 0;
    private int page = 0;

    private String callUrl;

    private BaseAdapter adapter;
    private ArrayList<QuizletFlashcardSet> sets;

    private QuizletSetSelectedCallback callback = setCallback;

    private boolean fetchingNextPage = false;

    private QuizletList resultList;

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private HttpRequestFactory requestFactory;

    public QuizletListFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        sets = new ArrayList<>();

        if( getArguments().containsKey(QUIZLET_CALL_URL) )
        {
            callUrl = getArguments().getString(QUIZLET_CALL_URL);
        }

        requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
            public void initialize(HttpRequest request) {
                request.setParser(new JsonObjectParser(JSON_FACTORY));
            }
        });

        sets.addAll(resultList.flashcardSets);
        adapter =  new QuizletSetAdapter(getActivity(), sets);
        setListAdapter(adapter);
        setRetainInstance(true);
    }

    public void setQuizletResultList(QuizletList list)
    {
        this.resultList = list;
        totalPages = resultList.totalPages;
        page = resultList.page;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        getListView().setOnScrollListener(this);

    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (!(activity instanceof QuizletSetSelectedCallback))
        {
            throw new IllegalStateException(
                    "Activity must implement quizlet fragment callbacks.");
        }

        callback = (QuizletSetSelectedCallback) activity;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState)
    {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {
        boolean loadMore = firstVisibleItem + visibleItemCount >= (totalItemCount - 5);

        if( loadMore && page != totalPages && !fetchingNextPage )
        {
            fetchingNextPage = true;
            new QuizletNextPageTask().execute();
        }
    }

    private class QuizletNextPageTask extends AsyncTask<String, Void, QuizletList>
    {
        private String errorMessage = null;

        @Override
        protected QuizletList doInBackground(String... params)
        {
            QuizletList result = null;
            try
            {
                int newPage = page + 1;
                callUrl = callUrl.replace("page=" + page, "page=" + newPage);

                GenericUrl url = new GenericUrl(callUrl);
                HttpRequest setRequest = requestFactory.buildGetRequest(url);

                HttpResponse response = setRequest.execute();


                if( response.getStatusCode() >= 400 )
                {
                    Log.e(getString(R.string.log_tag), "Error querying quizlet " + response.getStatusMessage());
                    errorMessage = "Error reading quizlet json response " + response.getStatusCode() + ": " + response.getStatusMessage();
                }
                else
                {
                    result = response.parseAs(QuizletList.class);
                }
            }
            catch( Exception e )
            {
                Log.e(getString(R.string.log_tag), "Error reading quizlet json response " + e.getMessage());
                errorMessage = "Error reading quizlet json response " + e.getMessage();
            }
            return result;
        }

        @Override
        protected void onPostExecute(QuizletList result)
        {
            super.onPostExecute(result);
            if( errorMessage != null && !errorMessage.isEmpty() )
            {
                Toast.makeText(getActivity(), "Error getting search results " + errorMessage, Toast.LENGTH_LONG).show();
            }
            else
            {
                sets.addAll(result.flashcardSets);
                fetchingNextPage = false;
                adapter.notifyDataSetChanged();
            }
        }
    }

    private class QuizletSetAdapter extends ArrayAdapter<QuizletFlashcardSet> implements CompoundButton.OnCheckedChangeListener
    {
        private final Context context;
        private final List<QuizletFlashcardSet> flashcardSets;
        private final ArrayList<Integer> itemChecked = new ArrayList<>();


        public QuizletSetAdapter(Context context, List<QuizletFlashcardSet> sets)
        {
            super(context, R.layout.quizlet_set_layout, sets);
            this.context = context;
            this.flashcardSets = sets;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            SetHolder holder;

            if(row == null)
            {
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(R.layout.quizlet_set_layout, parent, false);

                holder = new SetHolder();
                holder.setTitle = (TextView)row.findViewById(R.id.quizlet_flashcard_set_title);
                holder.setDescription = (TextView)row.findViewById(R.id.quizlet_flashcard_set_description);
                holder.flashcardCount = (TextView)row.findViewById(R.id.quizlet_flashcard_count);
                holder.loadingSpinner = (ProgressBar)row.findViewById(R.id.quizlet_loading);
                holder.quizletSetSelect = (CheckBox)row.findViewById(R.id.quizlet_set_select);
                holder.quizletSetSelect.setTag(position);
                holder.quizletSetSelect.setOnCheckedChangeListener(this);

                row.setTag(holder);
            }
            else
            {
                holder = (SetHolder)row.getTag();
                ((SetHolder) row.getTag()).quizletSetSelect.setTag(position);

            }

            QuizletFlashcardSet set = flashcardSets.get(position);

            if( set != null )
            {
                if( position == flashcardSets.size() - 1 && page != totalPages )
                {
                    holder.setTitle.setVisibility(View.GONE);
                    holder.setDescription.setVisibility(View.GONE);
                    holder.flashcardCount.setVisibility(View.GONE);
                    holder.quizletSetSelect.setVisibility(View.GONE);
                    holder.loadingSpinner.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder.setTitle.setText(set.setTitle);
                    holder.setTitle.setVisibility(View.VISIBLE);

                    holder.flashcardCount.setText( set.termCount + " " + getString(R.string.flashcards));
                    holder.flashcardCount.setVisibility(View.VISIBLE);

                    holder.quizletSetSelect.setVisibility(View.VISIBLE);
                    holder.quizletSetSelect.setChecked(itemChecked.contains(position));

                    holder.loadingSpinner.setVisibility(View.GONE);
                }

            }
            return row;
        }

        @Override
        public void onCheckedChanged(CompoundButton view, boolean isChecked)
        {
            if(isChecked)
            {
                itemChecked.add((Integer)view.getTag());
                callback.setSelected(flashcardSets.get((Integer)view.getTag()).setId);
            }
            else
            {
                itemChecked.remove((Integer)view.getTag());
                callback.deselectSet(flashcardSets.get((Integer) view.getTag()).setId);
            }
        }

        private class SetHolder
        {
            TextView setTitle;
            TextView setDescription;
            TextView flashcardCount;
            ProgressBar loadingSpinner;
            CheckBox quizletSetSelect;
        }
    }

    public interface QuizletSetSelectedCallback {
        void setSelected(int selectedSet);

        void deselectSet(int deselectedSet);
    }

    private final static QuizletSetSelectedCallback setCallback = new QuizletSetSelectedCallback()
    {
        @Override
        public void setSelected(int selectedSet)
        {

        }

        @Override
        public void deselectSet(int deselectedSet)
        {

        }
    };
}
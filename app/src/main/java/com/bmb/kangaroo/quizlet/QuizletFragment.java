package com.bmb.kangaroo.quizlet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bmb.kangaroo.R;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson.JacksonFactory;


public class QuizletFragment extends Fragment
{

    private EditText searchQuery;

    private String accessToken;

    private String quizletUserId;
    private QuizletMode mode;

    private TextView messageView;

    private final static String QUIZLET_CLIENT_ID = "CA8RSrKrt8";

    private final static String SEARCH_CALL = "https://api.quizlet.com/2.0/search/sets?client_id=" + QUIZLET_CLIENT_ID + "&time_format=fuzzy_date";
    private final static String BASE_SET_CALL = "https://api.quizlet.com/2.0/users/";
    private final static String FAVORITES = "/favorites?access_token=";
    private final static String MY_SETS = "/sets?access_token=";

    private QuizletQueryResult queryResultCallback = dummyResultHandler;

    private boolean accessGranted;

    private String callUrl = "";

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private HttpRequestFactory requestFactory;

    public QuizletFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
            public void initialize(HttpRequest request) {
                request.setParser(new JsonObjectParser(JSON_FACTORY));
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final LinearLayout quizletLayout = (LinearLayout) inflater.inflate(R.layout.quizlet_main_screen, null);

        messageView = (TextView) quizletLayout.findViewById(R.id.quizlet_error_message);

        final SharedPreferences pref = getActivity().getSharedPreferences(getString(R.string.preferences_key), Context.MODE_PRIVATE);
        accessToken = pref.getString(getString(R.string.quizlet_token_key), "");

        //Get the expiration time or set the default value to 1000 ms before current time
        long tokenExpirationTime = pref.getLong(getString(R.string.quizlet_token_expiration_key), System.currentTimeMillis() - 1000l);
        if( tokenExpirationTime < System.currentTimeMillis() )
        {
            accessToken = "";
        }

        quizletUserId = pref.getString(getString(R.string.quizlet_user_id_key), "");
        accessGranted = false;
        Button grantAccessButton = (Button) quizletLayout.findViewById(R.id.quizlet_login_button);

        if( (accessToken == null || accessToken.isEmpty())|| quizletUserId == null || quizletUserId.isEmpty() )
        {
            accessGranted = false;
            grantAccessButton.setText(R.string.grant_access);
        }
        else
        {
            accessGranted = true;
            grantAccessButton.setText(R.string.change_user);
            messageView.setVisibility(View.VISIBLE);
            messageView.setText(getString(R.string.logged_in_as) + " " + quizletUserId);
        }

        grantAccessButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( accessGranted )
                {
                    SharedPreferences.Editor e = pref.edit();
                    e.putString(getString(R.string.quizlet_token_key), "");
                    accessToken = "";

                    e.putString(getString(R.string.quizlet_user_id_key), "");
                    quizletUserId = "";

                    e.apply();
                    queryResultCallback.loginRequest();
                }
                else
                {
                    queryResultCallback.loginRequest();
                }
            }
        });

        Button mySetsButton = (Button) quizletLayout.findViewById(R.id.quizlet_my_sets_button);

        mySetsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mode = QuizletMode.USER_LIST;
                new QuizletQueryTask().execute(buildSetUrl(MY_SETS));
            }
        });

        Button favoriteSetsButton = (Button) quizletLayout.findViewById(R.id.quizlet_favorite_sets_button);
        favoriteSetsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mode = QuizletMode.USER_LIST;
                new QuizletQueryTask().execute(buildSetUrl(FAVORITES));
            }
        });

        searchQuery = (EditText) quizletLayout.findViewById(R.id.quizlet_search_box);
        Button searchButton = (Button) quizletLayout.findViewById(R.id.quizlet_search_button);
        searchButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mode = QuizletMode.SEARCH;
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(quizletLayout.getWindowToken(), 0);
                new QuizletQueryTask().execute(getSearchUrl());
            }
        });

        mySetsButton.setEnabled(accessGranted);
        favoriteSetsButton.setEnabled(accessGranted);
        searchQuery.setEnabled(accessGranted);
        searchButton.setEnabled(accessGranted);

        return quizletLayout;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (!(activity instanceof QuizletQueryResult))
        {
            throw new IllegalStateException(
                    "Activity must implement quizlet fragment callbacks.");
        }

        queryResultCallback = (QuizletQueryResult) activity;
    }

    private String getSearchUrl() {
        StringBuilder sb = new StringBuilder();
        sb.append( SEARCH_CALL );
        sb.append( "&q=" );
        sb.append( searchQuery.getText().toString() );
        sb.append( "&page=" );
        sb.append( 1 );
        Log.d(getString(R.string.log_tag), sb.toString());
        return sb.toString() ;
    }

    private String buildSetUrl(String context) {
        StringBuilder sb = new StringBuilder();
        sb.append( BASE_SET_CALL );

        if( quizletUserId != null && !quizletUserId.isEmpty() )
        {
            sb.append(quizletUserId);
        }

        sb.append(context);

        if( accessToken != null && !accessToken.isEmpty() )
        {
            sb.append(accessToken);
        }

        return sb.toString();

    }

    private class QuizletQueryTask extends AsyncTask<String, Void, QuizletList>
    {
        private final ProgressDialog queryProgressDialog;
        private String errorMessage = null;
        public QuizletQueryTask()
        {
            queryProgressDialog  = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            queryProgressDialog.setMessage(getString(R.string.executing_quizlet_request));
            queryProgressDialog.show();
        }

        @Override
        protected QuizletList doInBackground(String... params)
        {
            QuizletList list = null;
            if( params.length > 0 )
            {
                try
                {
                    callUrl = params[0];
                    GenericUrl url = new GenericUrl(callUrl);
                    HttpRequest setRequest = requestFactory.buildGetRequest(url);

                    HttpResponse response = setRequest.execute();


                    if( response.getStatusCode() >= 400 )
                    {
                        Log.e(getString(R.string.log_tag), "Error querying quizlet " + response.getStatusMessage());
                        errorMessage = "Error reading quizlet json response " + response.getStatusCode() + ": " + response.getStatusMessage();
                    }
                    else
                    {
                        list = response.parseAs(QuizletList.class);
                    }

                }
                catch( Exception e )
                {
                    Log.e(getString(R.string.log_tag), "Error reading quizlet json response " + e.getMessage());
                    errorMessage = "Error reading quizlet json response " + e.getMessage();
                }
            }
            return list;
        }

        @Override
        protected void onPostExecute(QuizletList quizletList)
        {
            super.onPostExecute(quizletList);
            if( queryProgressDialog.isShowing() )
            {
                queryProgressDialog.dismiss();
            }

            if( errorMessage != null && !errorMessage.isEmpty() )
            {
                messageView.setVisibility(View.VISIBLE);
                messageView.setText(errorMessage);
                messageView.setTextColor(Color.RED);
            }
            else
            {
                queryResultCallback.queryResult(mode, quizletList, callUrl);
            }
        }
    }

    private final static QuizletQueryResult dummyResultHandler = new QuizletQueryResult()
    {
        @Override
        public void queryResult(QuizletMode mode, QuizletList result, String callUrl)
        {

        }

        @Override
        public void loginRequest()
        {

        }
    };

    public interface QuizletQueryResult {
        void queryResult(QuizletMode mode, QuizletList result, String callUrl);

        void loginRequest();
    }
}
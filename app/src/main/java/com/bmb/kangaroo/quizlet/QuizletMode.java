package com.bmb.kangaroo.quizlet;

public enum QuizletMode
{
    SEARCH ("search"),
    USER_LIST ("user_list");

    private final String mode;

    QuizletMode(String value)
    {
        this.mode = value;
    }

    public String getMode()
    {
        return mode;
    }

    public static QuizletMode fromString(String mode)
    {
        if( mode != null )
        {
            for( QuizletMode b : QuizletMode.values() )
            {
                if( mode.equalsIgnoreCase(b.mode) )
                {
                    return b;
                }
            }
        }
        return null;
    }
}

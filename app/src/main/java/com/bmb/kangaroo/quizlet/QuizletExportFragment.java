package com.bmb.kangaroo.quizlet;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.Key;

import java.io.IOException;
import java.util.Locale;

public class QuizletExportFragment extends Fragment
{

    private String accessToken;
    private String quizletUserId;

    private TextView messageView;

    private final static String EXPORT_URL = "https://api.quizlet.com/2.0/sets";
    private final static String USER_URL = "https://api.quizlet.com/2.0/users/";
    private final static String IMAGE_URL = "https://api.quizlet.com/2.0/images";

    private QuizletExportResult exportCallback = dummyResultHandler;

    private boolean accessGranted;

    private FlashcardSet setToExport = null;

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();

    private HttpRequestFactory requestFactory;

    public QuizletExportFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
            public void initialize(HttpRequest request) {
                request.setParser(new JsonObjectParser(JSON_FACTORY));
            }
        });

        if( getArguments() != null )
        {
            if( getArguments().containsKey(getString(R.string.quizlet_export_arg)) )
            {
                String flashcardSetId = getArguments().getString(getString(R.string.quizlet_export_arg));
                if( flashcardSetId != null && !flashcardSetId.isEmpty() )
                {
                    Id setId = IdService.resolveId(flashcardSetId);
                    setToExport = FlashcardSetService.getInstance().getFlashcardSet(setId);
                }
            }
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final LinearLayout quizletLayout = (LinearLayout) inflater.inflate(R.layout.quizlet_export_layout, null);

        messageView = (TextView) quizletLayout.findViewById(R.id.quizlet_export_error_message);

        final SharedPreferences pref = getActivity().getSharedPreferences(getString(R.string.preferences_key), Context.MODE_PRIVATE);
        accessToken = pref.getString(getString(R.string.quizlet_token_key), "");

        //Get the expiration time or set the default value to 1000 ms before current time
        long tokenExpirationTime = pref.getLong(getString(R.string.quizlet_token_expiration_key), System.currentTimeMillis() - 1000l);
        if( tokenExpirationTime < System.currentTimeMillis() )
        {
            accessToken = "";
        }

        quizletUserId = pref.getString(getString(R.string.quizlet_user_id_key), "");
        accessGranted = false;
        Button grantAccessButton = (Button) quizletLayout.findViewById(R.id.quizlet_export_login_button);

        if( (accessToken == null || accessToken.isEmpty())|| quizletUserId == null || quizletUserId.isEmpty() )
        {
            accessGranted = false;
            grantAccessButton.setText(R.string.grant_access);
        }
        else
        {
            accessGranted = true;
            grantAccessButton.setText(R.string.change_user);
            messageView.setVisibility(View.VISIBLE);
            messageView.setText(getString(R.string.logged_in_as) + " " + quizletUserId);
        }

        grantAccessButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( accessGranted )
                {
                    SharedPreferences.Editor e = pref.edit();
                    e.putString(getString(R.string.quizlet_token_key), "");
                    accessToken = "";

                    e.putString(getString(R.string.quizlet_user_id_key), "");
                    quizletUserId = "";

                    e.apply();
                    exportCallback.loginRequest();
                }
                else
                {
                    exportCallback.loginRequest();
                }
            }
        });

        Button exportSetsButton = (Button) quizletLayout.findViewById(R.id.quizlet_export_set_button);

        exportSetsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                new QuizletExportTask().execute(EXPORT_URL);
            }
        });

        exportSetsButton.setEnabled(accessGranted);

        return quizletLayout;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (!(activity instanceof QuizletExportResult))
        {
            throw new IllegalStateException(
                    "Activity must implement quizlet fragment callbacks.");
        }

        exportCallback = (QuizletExportResult) activity;
    }

    public static class ImageResponse
    {
        @Key("id")
        public String id;

        @Key("imageUrl")
        public String url;
    }

    private class QuizletExportTask extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... params)
        {
            String responseString = "";
            if( params.length > 0 )
            {
                try
                {
                    GenericUrl exportUrl = new GenericUrl(params[0]);

                    exportUrl.put("access_token", accessToken);
                    exportUrl.put("title", setToExport.getTitle());
                    exportUrl.put("lang_terms", Locale.getDefault().getLanguage());
                    exportUrl.put("lang_definitions", Locale.getDefault().getLanguage());

                    FlashcardDAO flashcardDAO = new FlashcardDAO();
                    flashcardDAO.open();

                    //Check is user has plus membership to upload image
                    GenericUrl userUrl = new GenericUrl(USER_URL + quizletUserId);
                    HttpRequest userProfileRequest = requestFactory.buildGetRequest(userUrl);
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType("application/json");
                    userProfileRequest.setHeaders(headers);

                    User userProfile = userProfileRequest.execute().parseAs(User.class);

                    Cursor flashcardCursor = flashcardDAO.findFlashcards(setToExport.getId(), false);
                    int i = 0;

                    while( flashcardCursor.moveToNext() )
                    {
                        String flashcardTerm = flashcardCursor.getString(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                        String flashcardDefintion = flashcardCursor.getString(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                        boolean isImageFlashcard = flashcardCursor.getInt(flashcardCursor.getColumnIndex(FlashcardOpenHelper.COLUMN_IS_PICTURE)) == 1;
                        exportUrl.put("terms[" + i + "]", flashcardTerm);
                        exportUrl.put("definitions[" + i + "]", flashcardDefintion);

                        if( isImageFlashcard && userProfile.accountType.equalsIgnoreCase("plus") )
                        {
                            String imageId = uploadImage(flashcardTerm);
                            exportUrl.put("images[" + i + "]", imageId);
                        }
                        else
                        {
                            exportUrl.put("images[" + i + "]", "");
                        }
                        i++;
                    }

                    flashcardDAO.close();

                    HttpRequest exportRequest = requestFactory.buildPostRequest(exportUrl, null);

                    HttpResponse response = exportRequest.execute();

                    if( response.getStatusCode() != 201 )
                    {
                        QuizletResponse errorResponse = response.parseAs(QuizletResponse.class);
                        if( errorResponse != null )
                        {
                            Log.e(getString(R.string.log_tag), "Error getting access token " + errorResponse.errorType + ": " + errorResponse.errorDescription);
                            responseString = "Error exporting flashcards to quizlet " + errorResponse.errorType + ": " + errorResponse.errorDescription;
                        }
                    }
                    else
                    {
                        Log.e(getString(R.string.log_tag), "Error submitting flashcard set: " + response.getStatusMessage());
                    }
                }
                catch( Exception e )
                {
                    Log.e(getString(R.string.log_tag), "Error exporting flashcards to quizlet " + e.getMessage());
                    responseString = "Error exporting flashcards to quizlet " + e.getMessage();
                }
            }

            return responseString;
        }

        private String uploadImage(String imageLocation) throws IOException
        {
            String imageId = "";

            GenericUrl uploadImageUrl = new GenericUrl(IMAGE_URL);

            uploadImageUrl.put("access_token", accessToken);
            uploadImageUrl.put("imageData[0]", imageLocation);

            HttpRequest imagePostRequest = requestFactory.buildPostRequest(uploadImageUrl, null);

            ImageResponse response = imagePostRequest.execute().parseAs(ImageResponse.class);
            if( response != null )
            {
                imageId = response.id;
            }

            return imageId;
        }

        @Override
        protected void onPostExecute(String resultString)
        {
            super.onPostExecute(resultString);
            if( resultString != null && !resultString.isEmpty() )
            {
                messageView.setText(resultString);
                messageView.setTextColor(Color.RED);
            }
            else
            {
                exportCallback.exportComplete();
            }
        }
    }

    private final static QuizletExportResult dummyResultHandler = new QuizletExportResult()
    {
        @Override
        public void loginRequest()
        {
        }

        @Override
        public void exportComplete()
        {
        }
    };

    public interface QuizletExportResult {

        void loginRequest();

        void exportComplete();
    }

    public static class User extends GenericJson
    {
        @Key("account_type")
        public String accountType;
    }

    public static class QuizletResponse extends GenericJson
    {
        @Key("error")
        public String errorType;

        @Key("error_description")
        public String errorDescription;
    }
}
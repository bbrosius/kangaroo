package com.bmb.kangaroo;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.views.NoteCardLayout;
import com.bmb.utils.Utils;

public class FlashcardFragment extends Fragment
{
    public static final int CORRECT = 0;
    public static final int INCORRECT = 1;
    private static final int NOT_SET = -1;

	public FlashcardFragment() 
	{
	}
	
	private String term = "";
	private String definition = "";
    private int correct = -1;
	private boolean isFlashcardFront = true;
    private boolean hasImage = false;
	private ViewFlipper flashcardFlipper;
    private boolean isFromQuizlet = false;

    private NoteCardLayout flashcardFlipFront;
    private NoteCardLayout flashcardFlipBack;

    private int flashcardColor;
	
	/**
	 * The fragment's current callback object, which is notified of list item
	 * clicks.
	 */
	private FlashcardActionCallback callBacks = flashcardCallbacks;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		if( getArguments() != null ) 
		{
			if( getArguments().containsKey(getString(R.string.flashcard_front_argument)) )
			{
				isFlashcardFront = getArguments().getBoolean(getString(R.string.flashcard_front_argument));
			}
			
			if( getArguments().containsKey(getString(R.string.flashcard_term_argument)) )
			{
				term = getArguments().getString(getString(R.string.flashcard_term_argument));
			}
			
			if( getArguments().containsKey(getString(R.string.flashcard_definition_argument)) )
			{
				definition = getArguments().getString(getString(R.string.flashcard_definition_argument));
			}

            if( getArguments().containsKey(getString(R.string.flashcard_correct_argument)) )
            {
                correct = getArguments().getInt(getString(R.string.flashcard_correct_argument));
            }

            if( getArguments().containsKey(getString(R.string.flashcard_has_image_argument)) )
            {
                hasImage = getArguments().getBoolean(getString(R.string.flashcard_has_image_argument));
            }

            if( getArguments().containsKey(getString(R.string.from_quizlet_argument)) )
            {
                isFromQuizlet = getArguments().getBoolean(getString(R.string.from_quizlet_argument));
            }
		}

        flashcardColor = Color.WHITE;
        SharedPreferences pref = getActivity().getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
        boolean setFlashcardColor = pref.getBoolean(getString(R.string.color_flashcards_key), false);
        if( setFlashcardColor )
        {
            flashcardColor = pref.getInt(getString(R.string.subject_color_key), getResources().getColor(R.color.primary_color));
        }
	}

	public boolean getIsFlashcardFront()
    {
		return isFlashcardFront;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		LinearLayout flashcardLayout = (LinearLayout) inflater.inflate(R.layout.flashcard_view_flipper, container, false);
		flashcardFlipFront = (NoteCardLayout) flashcardLayout.findViewById(R.id.flashcard_flip_front);
        flashcardFlipBack = (NoteCardLayout) flashcardLayout.findViewById(R.id.flashcard_flip_back);

        if( hasImage )
        {
            Uri imageUri = Uri.parse(term);
            String text = definition;
            boolean flipped = false;
            //In the event the card has been flipped flip the image uri and text
            if( imageUri.getScheme() == null || imageUri.getScheme().isEmpty() )
            {
                imageUri = Uri.parse(definition);
                text = term;
                flipped = true;
            }

            if( !flipped )
            {
                TextView termView = (TextView) flashcardLayout.findViewById(R.id.flashcard_front_term);
                termView.setVisibility(View.GONE);

                ImageView termImage = (ImageView) flashcardLayout.findViewById(R.id.flashcard_front_term_image);
                termImage.setVisibility(View.VISIBLE);
                GraphicUtils.loadBitmap(imageUri, 200, 200, getActivity(), termImage);

                TextView backTermView = (TextView) flashcardLayout.findViewById(R.id.flashcard_back_term);
                backTermView.setVisibility(View.GONE);

                ImageView backTermImage = (ImageView) flashcardLayout.findViewById(R.id.flashcard_back_term_image);
                backTermImage.setVisibility(View.VISIBLE);
                GraphicUtils.loadBitmap(imageUri, 200, 200, getActivity(), backTermImage);

                TextView definitionView = (TextView) flashcardLayout.findViewById(R.id.flashcard_back_defintion);
                definitionView.setText(text);
            }
            else
            {
                TextView termView = (TextView) flashcardLayout.findViewById(R.id.flashcard_front_term);
                termView.setVisibility(View.VISIBLE);
                termView.setText(text);

                ImageView termImage = (ImageView) flashcardLayout.findViewById(R.id.flashcard_front_term_image);
                termImage.setVisibility(View.GONE);
                //GraphicUtils.loadBitmap(imageUri, 200, 200, getActivity(), termImage);

                TextView backTermView = (TextView) flashcardLayout.findViewById(R.id.flashcard_back_term);
                backTermView.setVisibility(View.GONE);

                ImageView backTermImage = (ImageView) flashcardLayout.findViewById(R.id.flashcard_back_term_image);
                backTermImage.setVisibility(View.VISIBLE);
                GraphicUtils.loadBitmap(imageUri, 200, 200, getActivity(), backTermImage);

                TextView definitionView = (TextView) flashcardLayout.findViewById(R.id.flashcard_back_defintion);
                definitionView.setText(text);
            }

        }
        else
        {
            TextView termView = (TextView) flashcardLayout.findViewById(R.id.flashcard_front_term);
            termView.setText(term);
            termView.setVisibility(View.VISIBLE);

            ImageView termImage = (ImageView) flashcardLayout.findViewById(R.id.flashcard_front_term_image);
            termImage.setVisibility(View.GONE);

            TextView backTermView = (TextView) flashcardLayout.findViewById(R.id.flashcard_back_term);
            backTermView.setText(term);
            backTermView.setVisibility(View.VISIBLE);

            ImageView backTermImage = (ImageView) flashcardLayout.findViewById(R.id.flashcard_back_term_image);
            backTermImage.setVisibility(View.GONE);

            TextView definitionView = (TextView) flashcardLayout.findViewById(R.id.flashcard_back_defintion);
            definitionView.setText(definition);
        }

        ImageView correctCheck = (ImageView) flashcardLayout.findViewById(R.id.flashcard_correct);
        correctCheck.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( correct == 1)
                {
                    correct = -1;
                    flashcardFlipFront.setCardColor(flashcardColor);
                    flashcardFlipBack.setCardColor(flashcardColor);
                    flashcardFlipBack.postInvalidate();
                }
                else
                {
                    correct = 1;
                    flashcardFlipFront.setCardColor(Color.GREEN);
                    flashcardFlipBack.setCardColor(Color.GREEN);
                    flashcardFlipBack.postInvalidate();
                }

                callBacks.flashcardCorrectnessChanged(correct);
            }
        });

        ImageView incorrectX = (ImageView) flashcardLayout.findViewById(R.id.flashcard_incorrect);
        incorrectX.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( correct == 0 )
                {
                    correct = -1;
                    flashcardFlipFront.setCardColor(flashcardColor);
                    flashcardFlipBack.setCardColor(flashcardColor);
                    flashcardFlipBack.postInvalidate();
                }
                else
                {
                    correct = 0;
                    flashcardFlipFront.setCardColor(Color.RED);
                    flashcardFlipBack.setCardColor(Color.RED);
                    flashcardFlipBack.postInvalidate();
                }
                callBacks.flashcardCorrectnessChanged(correct);
            }
        });
			
		flashcardFlipper = (ViewFlipper) flashcardLayout.findViewById(R.id.flashcard_flipper);
		
		flashcardLayout.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				//callBacks.flashcardFlipped(!isFlashcardFront);
				
				flashcardFlipper.setInAnimation(Utils.getContext(), R.anim.grow_from_middle);
				flashcardFlipper.setOutAnimation(Utils.getContext(), R.anim.shrink_to_middle);
				if (flashcardFlipper.getDisplayedChild()==0) 
				{
					
					flashcardFlipper.showNext();
				}
				else
				{
					flashcardFlipper.showPrevious();
				}
				
			}
			
		});

        if( correct == -1 )
        {
            flashcardFlipFront.setCardColor(flashcardColor);
            flashcardFlipBack.setCardColor(flashcardColor);
        }
        else if( correct == 0 )
        {
            flashcardFlipFront.setCardColor(Color.RED);
            flashcardFlipBack.setCardColor(Color.RED);
        }
        else if( correct == 1 )
        {
            flashcardFlipFront.setCardColor(Color.GREEN);
            flashcardFlipBack.setCardColor(Color.GREEN);
        }

        ImageView fromQuizletFront = (ImageView) flashcardLayout.findViewById(R.id.flashcard_from_quizlet);
        ImageView fromQuizletBack = (ImageView) flashcardLayout.findViewById(R.id.flashcard_back_from_quizlet);
        if( isFromQuizlet )
        {
            fromQuizletFront.setVisibility(View.VISIBLE);
            fromQuizletBack.setVisibility(View.VISIBLE);
        }
        else
        {
            fromQuizletFront.setVisibility(View.GONE);
            fromQuizletBack.setVisibility(View.GONE);
        }

		return flashcardLayout;
	}

    public void updateFlashcardBackground(int correctness) {
        correct = correctness;
        if( correctness == NOT_SET )
        {
            flashcardFlipFront.setCardColor(flashcardColor);
            flashcardFlipBack.setCardColor(flashcardColor);
        }
        else if( correctness == CORRECT )
        {
            flashcardFlipFront.setCardColor(Color.GREEN);
            flashcardFlipBack.setCardColor(Color.GREEN);
        }
        else if( correctness == INCORRECT )
        {
            flashcardFlipFront.setCardColor(Color.RED);
            flashcardFlipBack.setCardColor(Color.RED);
        }
    }

    public void flipFlashcard() {

        if (flashcardFlipper.getDisplayedChild()==0)
        {

            flashcardFlipper.showNext();
        }
        else
        {
            flashcardFlipper.showPrevious();
        }
    }

    /**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface FlashcardActionCallback
	{
        /**
         * Called when the flashcards correctness value has been changed.
         * @param correctness -1 if no state, 0 if incorrect, 1 if correct
         */
        void flashcardCorrectnessChanged(int correctness);
	}
	
	/**
	 * A dummy implementation of the {@link com.bmb.kangaroo.FlashcardFragment.FlashcardActionCallback} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private final static FlashcardActionCallback flashcardCallbacks = new FlashcardActionCallback()
	{
        @Override
        public void flashcardCorrectnessChanged(int correctness)
        {
        }
	};

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof FlashcardActionCallback))
		{
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		callBacks = (FlashcardActionCallback) activity;
	}
	
	@Override
	public void onDetach()
	{
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		callBacks = flashcardCallbacks;
	}
}
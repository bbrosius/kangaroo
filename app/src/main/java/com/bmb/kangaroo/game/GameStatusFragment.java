package com.bmb.kangaroo.game;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.Id;
import com.bmb.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
import com.google.android.gms.games.snapshot.Snapshots;
import com.google.android.gms.plus.Plus;
import com.google.example.games.basegameutils.BaseGameUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Background fragment that retains the state of the game. This is used to keep the game running between orientation changes.
 */
public class GameStatusFragment extends Fragment implements RoomStatusUpdateListener, RealTimeMessageReceivedListener, RecognitionListener, RoomUpdateListener, OnInvitationReceivedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{

    // Client used to interact with Google APIs.
    private GoogleApiClient googleApiClient;

    // Room ID where the currently active game is taking place; null if we're
    // not playing.
    private String roomId = null;

    // flag indicating whether we're dismissing the waiting room because the
    // game is starting
    private boolean gameStartingWaitDismissed = false;

    // The participants in the currently active game
    private ArrayList<Participant> participants = null;

    // My participant ID in the currently active game
    private String myId = null;

    // Score of other participants. We update this as we receive their scores
    // from the network.
    private final Map<String, Integer> participantScore = new HashMap<>();


    private boolean useAllSubjects = false;
    private Id subjectId;
    private FlashcardDAO flashcardDAO;

    private Cursor flashcards;

    private FlashcardGameActivity gameActivity;

    // Message buffer for sending messages
    private final byte[] messageBuffer = new byte[2];

    // If non-null, this is the id of the invitation we received via the
    // invitation listener
    private String incomingInvitationId = null;

    private boolean isListening = false;
    private boolean autoSignIn = true;
    private boolean signInClicked = false;

    private int secondsLeft = FlashcardGameActivity.GAME_DURATION; // how long until the game ends (seconds)
    private GameHistory gameHistory;

    // Are we playing in multiplayer mode?
    private boolean multiplayer = false;

    private SpeechRecognizer recognizer;

    // Are we currently resolving a connection failure?
    private boolean resolvingConnectionFailure = false;

    private int questionsAnswered = 0;
    private int score = 0; // user's current score

    private boolean playingGame = false;

    public static GameStatusFragment newInstance()
    {
        return new GameStatusFragment();
    }

    public GameStatusFragment()
    {
        //Required empty constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        flashcardDAO = new FlashcardDAO();
        flashcardDAO.open();

        recognizer = SpeechRecognizer.createSpeechRecognizer(getContext());
        recognizer.setRecognitionListener(this);

        SharedPreferences sp = getActivity().getSharedPreferences(getString(R.string.preferences_key), Context.MODE_PRIVATE);
        autoSignIn = sp.getBoolean(getString(R.string.auto_login_preference), false);

        gameActivity.showWelcomeFragment(googleApiClient.isConnected());

        //Load the local saved history
        loadLocal();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        gameActivity = (FlashcardGameActivity) activity;
        if( googleApiClient == null )
        {
            // Create the Google Api Client with access to Plus and Games
            googleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
                    .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                    .addApi(Drive.API).addScope(Drive.SCOPE_APPFOLDER)
                    .build();
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        if( googleApiClient != null && !googleApiClient.isConnected() && autoSignIn )
        {
            gameActivity.runningAutoSignIn();
            signInClicked = true;
            googleApiClient.connect();
        }
    }

    public boolean isPlayingGame()
    {
        return playingGame;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        flashcardDAO.close();
    }

    public void handleSignIn()
    {
        signInClicked = true;
        googleApiClient.connect();
    }

    public void handleSignOut()
    {
        autoSignIn = false;
        googleApiClient.disconnect();
    }

    public void startSinglePlayer(boolean allSubjects, Id subjectId)
    {
        playingGame = true;
        useAllSubjects = allSubjects;
        this.subjectId = subjectId;
        startGame(false);
    }

    public GoogleApiClient getGoogleApiClient()
    {
        return googleApiClient;
    }

    public boolean getGameStartingWaitDismissed()
    {
        return gameStartingWaitDismissed;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        SharedPreferences sp = getActivity().getSharedPreferences(getString(R.string.preferences_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sp.edit();
        prefEditor.putBoolean(getString(R.string.auto_login_preference), autoSignIn);
        prefEditor.apply();
    }

    @Override
    public void onRoomConnecting(Room room)
    {
        updateRoom(room);
    }

    @Override
    public void onRoomAutoMatching(Room room)
    {
        updateRoom(room);
    }

    @Override
    public void onPeerInvitedToRoom(Room room, List<String> strings)
    {
        updateRoom(room);
    }

    @Override
    public void onPeerDeclined(Room room, List<String> strings)
    {
        updateRoom(room);
    }

    @Override
    public void onPeerJoined(Room room, List<String> strings)
    {
        updateRoom(room);
    }

    @Override
    public void onPeerLeft(Room room, List<String> strings)
    {
        updateRoom(room);
    }

    @Override
    public void onConnectedToRoom(Room room)
    {
        Log.d(getString(R.string.log_tag), "onConnectedToRoom.");

        // get room ID, participants and my ID:
        roomId = room.getRoomId();
        participants = room.getParticipants();
        myId = room.getParticipantId(Games.Players.getCurrentPlayerId(googleApiClient));

        // print out the list of participants (for debug purposes)
        Log.d(getString(R.string.log_tag), "Room ID: " + roomId);
        Log.d(getString(R.string.log_tag), "My ID " + myId);
        Log.d(getString(R.string.log_tag), "<< CONNECTED TO ROOM>>");
    }

    @Override
    public void onDisconnectedFromRoom(Room room)
    {
        roomId = null;
        showGameError();
    }

    @Override
    public void onPeersConnected(Room room, List<String> strings)
    {
        updateRoom(room);
    }

    @Override
    public void onPeersDisconnected(Room room, List<String> strings)
    {
        updateRoom(room);
    }

    @Override
    public void onP2PConnected(String s)
    {

    }

    @Override
    public void onP2PDisconnected(String s)
    {

    }

    @Override
    public void onRealTimeMessageReceived(RealTimeMessage realTimeMessage)
    {
        byte[] buf = realTimeMessage.getMessageData();
        String sender = realTimeMessage.getSenderParticipantId();
        Log.d(getString(R.string.log_tag), "Message received: " + (char) buf[0] + "/" + (int) buf[1]);

        if( buf[0] == 'F' || buf[0] == 'U' )
        {
            // score update.
            int existingScore = participantScore.containsKey(sender) ?
                    participantScore.get(sender) : 0;
            int thisScore = (int) buf[1];
            if( thisScore > existingScore )
            {
                // this check is necessary because packets may arrive out of
                // order, so we
                // should only ever consider the highest score we received, as
                // we know in our
                // game there is no way to lose points. If there was a way to
                // lose points,
                // we'd have to add a "serial number" to the packet.
                participantScore.put(sender, thisScore);
            }

            // update the scores on the screen
            updatePeerScoresDisplay();
        }
        else if( buf[0] == 'S' )
        {
            // someone else started to play -- so dismiss the waiting room and
            // get right to it!
            Log.d(getString(R.string.log_tag), "Starting game because we got a start message.");
            dismissWaitingRoom();
            startGame(true);
        }
    }

    // Forcibly dismiss the waiting room UI (this is useful, for example, if we realize the
    // game needs to start because someone else is starting to play).
    private void dismissWaitingRoom()
    {
        gameStartingWaitDismissed = true;
        gameActivity.finishActivity(FlashcardGameActivity.RC_WAITING_ROOM);
    }

    // updates the screen with the scores from our peers
    private void updatePeerScoresDisplay()
    {
        if( roomId != null )
        {
            for( Participant p : participants )
            {
                String pid = p.getParticipantId();
                if( pid.equals(myId) )
                {
                    continue;
                }
                if( p.getStatus() != Participant.STATUS_JOINED )
                {
                    continue;
                }
                int score = participantScore.containsKey(pid) ? participantScore.get(pid) : 0;
                gameActivity.updateScore(participants.indexOf(p), new Score(p.getDisplayName(), formatScore(score)));
            }
        }
    }

    // formats a score as a three-digit number
    private String formatScore(int i)
    {
        if( i < 0 )
        {
            i = 0;
        }
        String s = String.valueOf(i);
        return s.length() == 1 ? "00" + s : s.length() == 2 ? "0" + s : s;
    }

    @Override
    public void onRoomCreated(int statusCode, Room room)
    {
        Log.d(getString(R.string.log_tag), "onRoomCreated(" + statusCode + ", " + room + ")");
        if( statusCode != GamesStatusCodes.STATUS_OK )
        {
            Log.e(getString(R.string.log_tag), "*** Error: onRoomCreated, status " + statusCode);
            showGameError();
            return;
        }

        // show the waiting room UI
        showWaitingRoom(room);
    }

    @Override
    public void onJoinedRoom(int statusCode, Room room)
    {
        Log.d(getString(R.string.log_tag), "onRoomCreated(" + statusCode + ", " + room + ")");
        if( statusCode != GamesStatusCodes.STATUS_OK )
        {
            Log.e(getString(R.string.log_tag), "*** Error: onRoomCreated, status " + statusCode);
            showGameError();
            return;
        }

        // show the waiting room UI
        showWaitingRoom(room);
    }

    // Show the waiting room UI to track the progress of other players as they enter the
    // room and get connected.
    private void showWaitingRoom(Room room)
    {
        gameStartingWaitDismissed = false;

        // minimum number of players required for our game
        final int MIN_PLAYERS = 2;
        Intent i = Games.RealTimeMultiplayer.getWaitingRoomIntent(googleApiClient, room, MIN_PLAYERS);

        // show waiting room UI
        gameActivity.startActivityForResult(i, FlashcardGameActivity.RC_WAITING_ROOM);
    }

    @Override
    public void onLeftRoom(int statusCode, String s)
    {
        // we have left the room; return to main screen.
        Log.d(getString(R.string.log_tag), "onLeftRoom, code " + statusCode);
        gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.HOME_SCREEN);
    }

    @Override
    public void onRoomConnected(int statusCode, Room room)
    {
        Log.d(getString(R.string.log_tag), "onRoomConnected(" + statusCode + ", " + room + ")");
        if( statusCode != GamesStatusCodes.STATUS_OK )
        {
            Log.e(getString(R.string.log_tag), "*** Error: onRoomConnected, status " + statusCode);
            showGameError();
            return;
        }
        updateRoom(room);
    }

    @Override
    public void onInvitationReceived(Invitation invitation)
    {
        incomingInvitationId = invitation.getInvitationId();
        gameActivity.invitationRecieved(invitation);
    }

    @Override
    public void onInvitationRemoved(String s)
    {
    }

    // Show error message about game being cancelled and return to main screen.
    private void showGameError()
    {
        if( isAdded() )
        {
            BaseGameUtils.makeSimpleDialog(gameActivity, getString(R.string.game_problem));
            gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.HOME_SCREEN);
        }
    }

    private void updateRoom(Room room)
    {
        participants = room.getParticipants();
        updatePeerScoresDisplay();
    }

    @Override
    public void onReadyForSpeech(Bundle params)
    {
    }

    @Override
    public void onBeginningOfSpeech()
    {
    }

    @Override
    public void onRmsChanged(float rmsdB)
    {
    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {
    }

    @Override
    public void onEndOfSpeech()
    {
        isListening = false;
    }

    @Override
    public void onError(int error)
    {
        isListening = false;
    }

    @Override
    public void onResults(Bundle results)
    {
        isListening = false;
        String str = "";
        ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        if( data != null && !data.isEmpty() )
        {
            str = data.get(0).trim();
        }

        String correctAnswer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
        if( str.matches(".*\\d.*") && !correctAnswer.matches(".*\\d.*") )
        {
            final Pattern pattern = Pattern.compile("\\d+"); // the regex
            final Matcher matcher = pattern.matcher(str); // your string

            while( matcher.find() )
            {
                String numberString = Utils.convertNumberToString(Long.parseLong(matcher.group()));
                str = str.replace(matcher.group(), numberString);
            }
        }

        questionAnswered(str);
    }

    @Override
    public void onPartialResults(Bundle partialResults)
    {
    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {
    }

    public String getCurrentTerm()
    {
        return flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
    }

    public void questionAnswered(String answer)
    {
        String correctAnswer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
        questionsAnswered++;
        boolean correct = false;

        if( correctAnswer.equalsIgnoreCase(answer) )
        {
            correct = true;
            score++;
        }

        String userName = getString(R.string.player);
        if( googleApiClient.isConnected() )
        {
            userName = Games.Players.getCurrentPlayer(googleApiClient).getDisplayName();
        }
        Score myScore = new Score(userName, formatScore(score));
        gameActivity.updateScoreDisplay(myScore);

        if( flashcards.moveToNext() )
        {
            String term = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));

            broadcastScore(false);

            gameActivity.moveToNextFlashcard(correct, term, correctAnswer, false);
        }
        else
        {
            if( !multiplayer )
            {
                broadcastScore(true);
                endGame();
            }
            else
            {
                broadcastScore(false);
                gameActivity.moveToNextFlashcard(correct, getString(R.string.waiting_for_other_players_to_finish), correctAnswer, true);
            }
        }
    }

    @Override
    public void onConnected(Bundle connectionHint)
    {
        gameActivity.loggedIn(googleApiClient);
        autoSignIn = true;
        // install invitation listener so we get notified if we receive an
        // invitation to play
        // a game.
        Games.Invitations.registerInvitationListener(googleApiClient, this);

        // if we received an invite via notification, accept it; otherwise, go
        // to main screen
        if (connectionHint != null) {
            Log.d(getString(R.string.log_tag), "onConnected: connection hint provided. Checking for invite.");
            Invitation inv = connectionHint
                    .getParcelable(Multiplayer.EXTRA_INVITATION);
            if (inv != null && inv.getInvitationId() != null) {
                // retrieve and cache the invitation ID
                Log.d(getString(R.string.log_tag),"onConnected: connection hint has a room invite!");
                acceptInviteToRoom(inv.getInvitationId());
                return;
            }
        }

        loadSavedGame();
    }

    private void loadSavedGame()
    {
        gameActivity.getLoadingDialog().setMessage(getString(R.string.loading_save_game_data));
        gameActivity.getLoadingDialog().show();

        AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                Snapshots.OpenSnapshotResult result;

                result = Games.Snapshots.open(googleApiClient, getString(R.string.saved_game_name), true).await();

                int status = result.getStatus().getStatusCode();

                Snapshot snapshot = null;
                if (status == GamesStatusCodes.STATUS_OK) {
                    snapshot = result.getSnapshot();
                } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONFLICT) {

                    // if there is a conflict  - then resolve it.
                    snapshot = processSnapshotOpenResult(result);

                    // if it resolved OK, change the status to Ok
                    if (snapshot != null) {
                        status = GamesStatusCodes.STATUS_OK;
                    }
                    else {
                        Log.w(getString(R.string.log_tag), "Conflict was not resolved automatically");
                    }
                } else {
                    Log.e(getString(R.string.log_tag), "Error while loading: " + status);
                }

                if (snapshot != null) {
                    try {
                        readSavedGame(snapshot);
                    } catch (IOException e) {
                        Log.e(getString(R.string.log_tag), getString(R.string.load_game_error) + e.getMessage());
                    }
                }
                return status;
            }

            @Override
            protected void onPostExecute(Integer status) {
                Log.i(getString(R.string.log_tag), "Snapshot loaded: " + status);

                gameActivity.getLoadingDialog().dismiss();

                // Note that showing a toast is done here for debugging. Your application should
                // resolve the error appropriately to your app.
                if (status == GamesStatusCodes.STATUS_SNAPSHOT_NOT_FOUND) {
                    Log.i(getString(R.string.log_tag), "Error: Snapshot not found");
                    gameActivity.showErrorMessage(R.string.snapshot_not_found);
                } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONTENTS_UNAVAILABLE) {
                    Log.i(getString(R.string.log_tag), "Error: Snapshot contents unavailable");
                    gameActivity.showErrorMessage(R.string.snapshot_unavailable);
                } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_FOLDER_UNAVAILABLE) {
                    Log.i(getString(R.string.log_tag), "Error: Snapshot folder unavailable");
                    gameActivity.showErrorMessage(R.string.snapshot_folder_unavailable);
                } else {
                    gameActivity.hideErrorMessage();
                }
            }
        };

        task.execute();
    }

    private void readSavedGame(Snapshot snapshot) throws IOException
    {
        gameHistory = new GameHistory(snapshot.getSnapshotContents().readFully());
    }

    /**
     * Conflict resolution for when Snapshots are opened.
     *
     * @param result The open snapshot result to resolve on open.
     * @return The opened Snapshot on success; otherwise, returns null.
     */
    private Snapshot processSnapshotOpenResult(Snapshots.OpenSnapshotResult result) {

        int status = result.getStatus().getStatusCode();

        Log.i(getString(R.string.log_tag), "Save Result status: " + status);

        if (status == GamesStatusCodes.STATUS_OK) {
            return result.getSnapshot();
        } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONTENTS_UNAVAILABLE) {
            return result.getSnapshot();
        } else if (status == GamesStatusCodes.STATUS_SNAPSHOT_CONFLICT) {
            final Snapshot snapshot = result.getSnapshot();
            final Snapshot conflictSnapshot = result.getConflictingSnapshot();

            //Union the two conflicting game history objects
            GameHistory conflictingHistory;
            GameHistory history = null;
            try
            {
                history = new GameHistory(snapshot.getSnapshotContents().readFully());
                conflictingHistory = new GameHistory(conflictSnapshot.getSnapshotContents().readFully());

                history = history.unionWith(conflictingHistory);

            }
            catch( IOException e )
            {
                Log.e(getString(R.string.log_tag), getString(R.string.load_game_error));
            }

            Snapshot mergedSnapshot = snapshot;
            if( snapshot.getMetadata().getLastModifiedTimestamp() < conflictSnapshot.getMetadata().getLastModifiedTimestamp() )
            {
                mergedSnapshot = conflictSnapshot;
            }

            if( history != null )
            {
                mergedSnapshot.getSnapshotContents().writeBytes(history.toBytes());
            }

            return mergedSnapshot;
        }
        // Fail, return null.
        return null;
    }

    @Override
    public void onConnectionSuspended(int i)
    {
        gameActivity.loggedOut();
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        gameActivity.loggedOut();
        Log.d(getString(R.string.log_tag), "onConnectionFailed() called, result: " + connectionResult);

        if (resolvingConnectionFailure) {
            Log.d(getString(R.string.log_tag), "onConnectionFailed() ignoring connection failure; already resolving.");
            return;
        }

        if (signInClicked || autoSignIn) {
            autoSignIn = false;
            signInClicked = false;
            resolvingConnectionFailure = BaseGameUtils.resolveConnectionFailure(gameActivity, googleApiClient,
                    connectionResult, FlashcardGameActivity.RC_SIGN_IN, getString(R.string.sign_in_error));
        }
    }

    protected void setResolvingConnectionFailure(boolean status)
    {
        resolvingConnectionFailure = status;
    }

    // Start the gameplay phase of the game.
    protected void startGame(boolean multiplayer)
    {
        this.multiplayer = multiplayer;
        playingGame = true;
        List<String> displayNames = new ArrayList<>();
        if( multiplayer )
        {
            for( Participant participant : participants )
            {
                if( participantScore.get(participant.getParticipantId()) == null )
                {
                    participantScore.put(participant.getParticipantId(), 0);
                }
                displayNames.add(participant.getDisplayName());
            }
        }
        else
        {
            String userName = getString(R.string.player);
            if( googleApiClient.isConnected() )
            {
                userName = Games.Players.getCurrentPlayer(googleApiClient).getDisplayName();
            }

            displayNames.add(userName);
        }
        //updateScoreDisplay();
        broadcastScore(false);
        gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.GAME_SCREEN);

        if( useAllSubjects )
        {
            flashcards = flashcardDAO.getAllFlashcards();
        }
        else
        {
            flashcards = flashcardDAO.findFlashcards(subjectId, true);
        }

        flashcards.moveToFirst();
        String term = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
        gameActivity.startGame(term, displayNames, secondsLeft);
    }

    private void broadcastScore(boolean finalScore)
    {
        if( !multiplayer )
        {
            return; // playing single-player mode
        }

        // First byte in message indicates whether it's a final score or not
        messageBuffer[0] = (byte) (finalScore ? 'F' : 'U');

        // Second byte is the score.
        messageBuffer[1] = (byte) score;

        // Send to every other participant.
        for( Participant p : participants )
        {
            if( p.getParticipantId().equals(myId) )
            {
                continue;
            }
            if( p.getStatus() != Participant.STATUS_JOINED )
            {
                continue;
            }
            if( finalScore )
            {
                // final score notification must be sent via reliable message
                Games.RealTimeMultiplayer.sendReliableMessage(googleApiClient, null, messageBuffer, roomId,
                        p.getParticipantId());
            }
            else
            {
                // it's an interim score notification, so we can use unreliable
                Games.RealTimeMultiplayer.sendReliableMessage(googleApiClient, null, messageBuffer, roomId,
                        p.getParticipantId());
            }
        }
    }

    // Broadcast a message indicating that we're starting to play. Everyone else
    // will react
    // by dismissing their waiting room UIs and starting to play too.
    protected void broadcastStart()
    {
        if( !multiplayer )
        {
            return; // playing single-player mode
        }

        messageBuffer[0] = 'S';
        messageBuffer[1] = (byte) 0;
        for( Participant p : participants )
        {
            if( p.getParticipantId().equals(myId) )
            {
                continue;
            }
            if( p.getStatus() != Participant.STATUS_JOINED )
            {
                continue;
            }
            Games.RealTimeMultiplayer.sendReliableMessage(googleApiClient, null, messageBuffer, roomId,
                    p.getParticipantId());
        }
    }

    // Leave the room.
    protected void leaveRoom()
    {
        Log.d(getString(R.string.log_tag), "Leaving room.");
        secondsLeft = FlashcardGameActivity.GAME_DURATION;
        gameActivity.stopKeepingScreenOn();
        if( roomId != null )
        {
            Games.RealTimeMultiplayer.leave(googleApiClient, this, roomId);
            roomId = null;
            gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.WAITING_SCREEN);
        }
        else
        {
            gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.HOME_SCREEN);
        }
    }

    protected void handleSelectPlayersResult(int response, Intent data)
    {
        if( response != Activity.RESULT_OK )
        {
            Log.w(getString(R.string.log_tag), "*** select players UI cancelled, " + response);
            gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.HOME_SCREEN);
            return;
        }

        Log.d(getString(R.string.log_tag), "Select players UI succeeded.");

        // get the invitee list
        final ArrayList<String> invitees = data.getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);
        Log.d(getString(R.string.log_tag), "Invitee count: " + invitees.size());

        // get the automatch criteria
        Bundle autoMatchCriteria = null;
        int minAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MIN_AUTOMATCH_PLAYERS, 0);
        int maxAutoMatchPlayers = data.getIntExtra(Multiplayer.EXTRA_MAX_AUTOMATCH_PLAYERS, 0);
        if( minAutoMatchPlayers > 0 || maxAutoMatchPlayers > 0 )
        {
            autoMatchCriteria = RoomConfig.createAutoMatchCriteria(
                    minAutoMatchPlayers, maxAutoMatchPlayers, 0);
            Log.d(getString(R.string.log_tag), "Automatch criteria: " + autoMatchCriteria);
        }

        // create the room
        Log.d(getString(R.string.log_tag), "Creating room...");
        RoomConfig.Builder rtmConfigBuilder = RoomConfig.builder(this);
        rtmConfigBuilder.addPlayersToInvite(invitees);
        rtmConfigBuilder.setMessageReceivedListener(this);
        rtmConfigBuilder.setRoomStatusUpdateListener(this);
        if( autoMatchCriteria != null )
        {
            rtmConfigBuilder.setAutoMatchCriteria(autoMatchCriteria);
        }

        gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.WAITING_SCREEN);
        gameActivity.keepScreenOn();
        resetGameVars();
        Games.RealTimeMultiplayer.create(googleApiClient, rtmConfigBuilder.build());
        Log.d(getString(R.string.log_tag), "Room created, waiting for it to be ready...");
    }

    protected void endGame()
    {
        playingGame = false;
        String placeString = "";

        if( multiplayer )
        {
            placeString = calculatePlace(score);
            if( placeString.equalsIgnoreCase(getString(R.string.first_place)) && !gameHistory.isHeadOfTheClassUnlocked() )
            {
                Games.Achievements.unlock(googleApiClient, getString(R.string.achievement_head_of_the_class));
                gameHistory.setHeadOfTheClassUnlocked(true);
            }
        }

        gameActivity.endGame(multiplayer, String.valueOf(score), placeString);

        if( googleApiClient.isConnected() )
        {
            Games.Leaderboards.submitScore(googleApiClient, getString(R.string.single_round_leaderboard_key),
                    score);
        }

        int previousQuestionsAsked = gameHistory.getQuestionsAsked();

        gameHistory.addCorrectAnswers(score);
        gameHistory.addQuestionsAsked(questionsAnswered);

        if( googleApiClient.isConnected() )
        {
            Games.Leaderboards.submitScore(googleApiClient, getString(R.string.total_correct_leaderboard_key), gameHistory.getCorrectAnswers());
        }
        int accuracy = (int) (((float) (gameHistory.getCorrectAnswers()) / (float) (gameHistory.getQuestionsAsked())) * 100);

        if( googleApiClient.isConnected() )
        {
            Games.Leaderboards.submitScore(googleApiClient, getString(R.string.career_accuracy_leaderboard_key), accuracy);
        }

        //Check for any achievements earned in this play through.
        if( previousQuestionsAsked == 0 && !gameHistory.isGreenhornUnlocked() )
        {
            if( googleApiClient.isConnected() )
            {
                Games.Achievements.unlock(googleApiClient, getString(R.string.achievement_green_learner));
            }
            gameHistory.setGreenhornUnlocked(true);
        }

        if( score == questionsAnswered && !gameHistory.isAcedTheTestUnlocked() )
        {
            if( googleApiClient.isConnected() )
            {
                Games.Achievements.unlock(googleApiClient, getString(R.string.achievement_ace_the_test));
            }
            gameHistory.setAcedTheTestUnlocked(true);
        }

        if( !gameHistory.isTeachersPetUnlocked() )
        {
            if( googleApiClient.isConnected() )
            {
                if( score > 0 )
                {
                    Games.Achievements.increment(googleApiClient, getString(R.string.achievement_teachers_pet), score);
                }
            }
            if( gameHistory.getCorrectAnswers() >= 100 )
            {
                gameHistory.setTeachersPetUnlocked(true);
            }
        }

        if( !gameHistory.isValedictorianUnlocked() )
        {
            if( googleApiClient.isConnected() )
            {
                if( score > 0 )
                {
                    Games.Achievements.increment(googleApiClient, getString(R.string.achievement_valedictorian), score);
                }
            }
            if( gameHistory.getCorrectAnswers() >= 1000 )
            {
                gameHistory.setValedictorianUnlocked(true);
            }
        }

        if( score >= 20 && !gameHistory.isToughTestUnlocked() )
        {
            if( googleApiClient.isConnected() )
            {
                Games.Achievements.unlock(googleApiClient, getString(R.string.achievement_tough_test));
            }
            gameHistory.setToughTestUnlocked(true);
        }

        if( googleApiClient.isConnected() )
        {
            saveSnapshot();
        }
        saveLocal();
    }

    private void saveLocal()
    {
        SharedPreferences sp = getActivity().getSharedPreferences(getString(R.string.preferences_key), Context.MODE_PRIVATE);
        gameHistory.save(sp, getString(R.string.note_shared_prefs));
    }

    /**
     * Prepares saving Snapshot to the user's synchronized storage, conditionally resolves errors,
     * and stores the Snapshot.
     */
    private void saveSnapshot() {
        AsyncTask<Void, Void, Snapshots.OpenSnapshotResult> task =
            new AsyncTask<Void, Void, Snapshots.OpenSnapshotResult>() {
                @Override
                protected Snapshots.OpenSnapshotResult doInBackground(Void... params) {

                    return Games.Snapshots.open(googleApiClient, getString(R.string.saved_game_name), true)
                            .await();
                }

                @Override
                protected void onPostExecute(Snapshots.OpenSnapshotResult result) {
                    Snapshot toWrite = processSnapshotOpenResult(result);
                    if (toWrite != null) {
                        Log.i(getString(R.string.log_tag), writeSnapshot(toWrite));
                    }
                    else {
                        Log.e(getString(R.string.log_tag), "Error opening snapshot: " + result.toString());
                    }
                }
            };

        task.execute();
    }

    /**
     * Generates metadata, takes a screenshot, and performs the write operation for saving a
     * snapshot.
     */
    private String writeSnapshot(Snapshot snapshot) {
        // Set the data payload for the snapshot.
        snapshot.getSnapshotContents().writeBytes(gameHistory.toBytes());

        // Save the snapshot.
        SnapshotMetadataChange metadataChange = new SnapshotMetadataChange.Builder()
                .setCoverImage(getScreenShot())
                .setDescription("Modified data at: " + Calendar.getInstance().getTime())
                .build();
        Games.Snapshots.commitAndClose(googleApiClient, snapshot, metadataChange);
        return snapshot.toString();
    }


    /**
     * Gets a screenshot to use with snapshots. Note that in practice you probably do not want to
     * use this approach because tablet screen sizes can become pretty large and because the image
     * will contain any UI and layout surrounding the area of interest.
     */
    private Bitmap getScreenShot() {
        View root = gameActivity.getRootView();
        Bitmap coverImage;
        try {
            root.setDrawingCacheEnabled(true);
            Bitmap base = root.getDrawingCache();
            coverImage = base.copy(base.getConfig(), false /* isMutable */);
        } catch (Exception ex) {
            Log.i(getString(R.string.log_tag), "Failed to create screenshot", ex);
            coverImage = null;
        } finally {
            root.setDrawingCacheEnabled(false);
        }
        return coverImage;
    }

    private String calculatePlace(int myScore)
    {
        List<Integer> scores = new ArrayList<>();
        scores.add(myScore);

        for( Participant p : participants )
        {
            String pid = p.getParticipantId();
            if( pid.equals(myId) )
            {
                continue;
            }
            if( p.getStatus() != Participant.STATUS_JOINED )
            {
                continue;
            }
            int score = participantScore.containsKey(pid) ? participantScore.get(pid) : 0;
            scores.add(score);
        }

        Collections.sort(scores, new Comparator<Integer>()
        {
            @Override
            public int compare(Integer scoreOne, Integer scoreTwo)
            {
                return scoreTwo.compareTo(scoreOne);
            }
        });

        int place = scores.indexOf(myScore);
        String placeString = "";
        //Switch on place and branch in case of ties.
        switch( place )
        {
            case 0:
                placeString = getString(R.string.first_place);
                break;
            case 1:
                if( myScore == scores.get(0) )
                {
                    placeString = getString(R.string.first_place);
                }
                else
                {
                    placeString = getString(R.string.second_place);
                }
                break;
            case 2:
                if( myScore == scores.get(0) )
                {
                    placeString = getString(R.string.first_place);
                }
                else if( myScore == scores.get(1) )
                {
                    placeString = getString(R.string.second_place);
                }
                else
                {
                    placeString = getString(R.string.third_place);
                }
                break;
            case 3:
                if( myScore == scores.get(0) )
                {
                    placeString = getString(R.string.first_place);
                }
                else if( myScore == scores.get(1) )
                {
                    placeString = getString(R.string.second_place);
                }
                else if( myScore == scores.get(2) )
                {
                    placeString = getString(R.string.third_place);
                }
                else
                {
                    placeString = getString(R.string.fourth_place);
                }
                break;
            case 4:
                if( myScore == scores.get(0) )
                {
                    placeString = getString(R.string.first_place);
                }
                else if( myScore == scores.get(1) )
                {
                    placeString = getString(R.string.second_place);
                }
                else if( myScore == scores.get(2) )
                {
                    placeString = getString(R.string.third_place);
                }
                else if( myScore == scores.get(3) )
                {
                    placeString = getString(R.string.fourth_place);
                }
                else
                {
                    placeString = getString(R.string.fifth_place);
                }
                break;
        }

        return placeString;
    }

    public void setMultiplayerGameState(boolean useAllSubjects, Id subjectId)
    {
        this.useAllSubjects = useAllSubjects;
        this.subjectId = subjectId;
    }

    public String getIncomingInvitationId()
    {
        return incomingInvitationId;
    }

    public boolean isMultiplayer()
    {
        return multiplayer;
    }

    public void gameVoiceAnswer()
    {
        if( !isListening )
        {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "com.bmb.kangaroo");
            intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
            recognizer.startListening(intent);
            isListening = true;
        }
        else
        {
            recognizer.stopListening();
            isListening = false;
        }
    }

    protected void loadLocal()
    {
        SharedPreferences sp = getActivity().getSharedPreferences(getString(R.string.preferences_key), Context.MODE_PRIVATE);
        gameHistory = new GameHistory(sp, getString(R.string.note_shared_prefs));
    }

    protected void acceptInviteToRoom(String invId)
    {
        // accept the invitation
        Log.d(getString(R.string.log_tag), "Accepting invitation: " + invId);
        RoomConfig.Builder roomConfigBuilder = RoomConfig.builder(this);
        roomConfigBuilder.setInvitationIdToAccept(invId)
                .setMessageReceivedListener(this)
                .setRoomStatusUpdateListener(this);
        gameActivity.switchToScreen(FlashcardGameActivity.SCREEN.WAITING_SCREEN);
        gameActivity.keepScreenOn();
        resetGameVars();
        Games.RealTimeMultiplayer.join(googleApiClient, roomConfigBuilder.build());

        //If we accepted the incoming invite clear it out
        if( invId.equalsIgnoreCase(incomingInvitationId) )
        {
            incomingInvitationId = null;
        }
    }

    // Reset game variables in preparation for a new game.
    private void resetGameVars()
    {
        secondsLeft = FlashcardGameActivity.GAME_DURATION;
        score = 0;
        questionsAnswered = 0;
        participantScore.clear();
    }
}

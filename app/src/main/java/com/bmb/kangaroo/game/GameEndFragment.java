package com.bmb.kangaroo.game;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.views.NoteCardLayout;

public class GameEndFragment extends Fragment
{
    private GameEndCallback callback = gameEndCallback;
    public final static String SCORE_ARGUMENT = "final_score";
    public final static String PLACE_ARGUMENT = "place_argument";

    private String finalScore;
    private String place = "";

    public GameEndFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments().containsKey(SCORE_ARGUMENT) )
        {
            finalScore = getArguments().getString(SCORE_ARGUMENT);
        }

        if( getArguments().containsKey(PLACE_ARGUMENT) )
        {
            place = getArguments().getString(PLACE_ARGUMENT);
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        NoteCardLayout gameEndLayout = (NoteCardLayout) inflater.inflate(R.layout.game_end_layout, null);

        final TextView finalScoreView = (TextView) gameEndLayout.findViewById(R.id.game_final_score);
        finalScoreView.setText(finalScore);

        final TextView placeView = (TextView) gameEndLayout.findViewById(R.id.game_place);
        if( !place.isEmpty() )
        {
            placeView.setText(place);
        }

        final Button endGameButton = (Button) gameEndLayout.findViewById(R.id.exit_game);
        endGameButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.gameFinished();
            }
        });

        return gameEndLayout;

    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if( !(activity instanceof GameEndCallback) )
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callback = (GameEndCallback) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        callback = gameEndCallback;
    }

    public interface GameEndCallback
    {
        void gameFinished();
    }

    private final static GameEndCallback gameEndCallback = new GameEndCallback()
    {

        @Override
        public void gameFinished()
        {

        }
    };

}

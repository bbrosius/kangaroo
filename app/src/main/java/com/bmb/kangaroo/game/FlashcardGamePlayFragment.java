package com.bmb.kangaroo.game;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.R;

public class FlashcardGamePlayFragment extends Fragment
{
    public static final String CORRECT_ANSWER_ARGUMENT = "correct_arg";
    public static final String GAME_START = "game_start";
    public static final String GAME_WAIT = "game_wait";

    private GameQuestionAnsweredCallback callback = gameQuestionAnsweredCallback;

    private String term;

    private String correctAnswerString;
    private boolean previousCorrect = false;
    private boolean gameStart = true;
    private boolean gameWait = false;

    public FlashcardGamePlayFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments().containsKey(getString(R.string.flashcard_term_argument)) )
        {
            term = getArguments().getString(getString(R.string.flashcard_term_argument));
        }

        if( getArguments().containsKey(GAME_START) )
        {
            gameStart = getArguments().getBoolean(GAME_START);
        }

        if( getArguments().containsKey(GAME_WAIT) )
        {
            gameWait = getArguments().getBoolean(GAME_WAIT);
        }

        if( !gameStart )
        {
            gameStart = false;
            if( getArguments().containsKey(CORRECT_ANSWER_ARGUMENT) )
            {
                previousCorrect = false;
                correctAnswerString = getString(R.string.incorrect) + " " + getArguments().getString(CORRECT_ANSWER_ARGUMENT);
            }
            else
            {
                previousCorrect = true;
                correctAnswerString = getString(R.string.correct);
            }
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ScrollView questionLayout = (ScrollView) inflater.inflate(R.layout.game_flashcard_layout, container, false);

        final EditText questionAnswer = (EditText) questionLayout.findViewById(R.id.game_flashcard_answer);

        final TextView correctAnswer = (TextView) questionLayout.findViewById(R.id.game_last_result);
        correctAnswer.setText(correctAnswerString);

        if( gameStart )
        {
            correctAnswer.setVisibility(View.GONE);
        }
        else
        {
            correctAnswer.setVisibility(View.VISIBLE);
            if( previousCorrect )
            {
                correctAnswer.setTextColor(Color.GREEN);
            }
            else
            {
                correctAnswer.setTextColor(Color.RED);
            }

        }


        final TextView questionTerm = (TextView) questionLayout.findViewById(R.id.game_flashcard_term);
        final ImageView questionTermImageView = (ImageView) questionLayout.findViewById(R.id.game_flashcard_term_image);

        if( termIsPicture(term) )
        {
            GraphicUtils.loadBitmap(Uri.parse(term), 200, 200, getActivity(), questionTermImageView);
            questionTerm.setVisibility(View.GONE);
            questionTermImageView.setVisibility(View.VISIBLE);
        }
        else
        {
            questionTerm.setText(term);
            questionTerm.setVisibility(View.VISIBLE);
            questionTermImageView.setVisibility(View.GONE);
        }


        final Button submitAnswer = (Button) questionLayout.findViewById(R.id.game_submit_answer);
        submitAnswer.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                callback.questionAnswered(questionAnswer.getText().toString());
                //Hide the keyboard if it was shown
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(questionAnswer.getWindowToken(), 0);
            }
        });

        if( gameWait )
        {
            questionAnswer.setVisibility(View.GONE);
            submitAnswer.setVisibility(View.GONE);
            questionLayout.findViewById(R.id.game_speak_answer).setVisibility(View.GONE);
        }

        return questionLayout;
    }

    private boolean termIsPicture(String term)
    {
        return term.startsWith("content:") || term.startsWith("file:");
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if( !(activity instanceof GameQuestionAnsweredCallback) )
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callback = (GameQuestionAnsweredCallback) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        callback = gameQuestionAnsweredCallback;
    }

    public interface GameQuestionAnsweredCallback
    {
        void questionAnswered(String answer);
    }

    private final static GameQuestionAnsweredCallback gameQuestionAnsweredCallback = new GameQuestionAnsweredCallback()
    {

        @Override
        public void questionAnswered(String answer)
        {

        }
    };

}

package com.bmb.kangaroo.game;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bmb.kangaroo.BaseActivity;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.SubjectPreferenceActivity;
import com.bmb.kangaroo.utils.Id;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesActivityResultCodes;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.example.games.basegameutils.BaseGameUtils;

import java.util.List;

/**
 * This class handles communication between the various UI fragments and the GameStatusFragment which maintains the state of the game between orientation changes.
 */
public class FlashcardGameActivity extends BaseActivity implements GameWelcomeFragment.WelcomeScreenAction,
        FlashcardGamePlayFragment.GameQuestionAnsweredCallback,
        GameEndFragment.GameEndCallback, ScoreboardFragment.TimeUpCallback, ActivityCompat.OnRequestPermissionsResultCallback
{
    protected enum SCREEN
    {
        HOME_SCREEN,
        WAITING_SCREEN,
        GAME_SCREEN
    }

    // Request codes for the UIs that we show with startActivityForResult:
    protected final static int RC_SELECT_PLAYERS = 10000;
    protected final static int RC_INVITATION_INBOX = 10001;
    protected final static int RC_WAITING_ROOM = 10002;
    protected final static int RC_UNUSED = 10003;

    private static final int REQUEST_MICROPHONE = 1;

    private ProgressDialog loadingDialog = null;

    // Current state of the game:
    protected final static int GAME_DURATION = 90000; // game duration, seconds.

    private SCREEN currentScreen = SCREEN.HOME_SCREEN;
    private GameWelcomeFragment welcomeFragment;
    private ScoreboardFragment scoreboardFragment;
    private GameStatusFragment gameStatusFragment;

    // Request code used to invoke sign in user interactions.
    protected static final int RC_SIGN_IN = 9001;

    private DrawerLayout gameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        SharedPreferences pref = getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
        if( pref.getBoolean(getString(R.string.light_theme_key), false) )
        {
            setTheme(R.style.GameTheme_Light);
        }
        else
        {
            setTheme(R.style.GameTheme);
        }

        setContentView(R.layout.game_layout);

        loadingDialog = new ProgressDialog(this);

        Toolbar toolBar = (Toolbar) findViewById(R.id.game_toolbar);
        setSupportActionBar(toolBar);

        gameLayout = (DrawerLayout) findViewById(R.id.game_layout);

        //Set the nav menu icon on the tool bar
        toolBar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                gameLayout.openDrawer(GravityCompat.START);
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        gameStatusFragment = (GameStatusFragment) fragmentManager.findFragmentByTag(getString(R.string.game_status_tag));
        if( gameStatusFragment == null )
        {
            gameStatusFragment = GameStatusFragment.newInstance();

            fragmentManager.beginTransaction()
                    .add(gameStatusFragment, getString(R.string.game_status_tag))
                    .commit();
        }

        if( gameStatusFragment.isPlayingGame() )
        {
            scoreboardFragment = (ScoreboardFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.game_scoreboard_tag));
            if( scoreboardFragment == null )
            {
                scoreboardFragment = new ScoreboardFragment();
            }

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.scoreboard, scoreboardFragment, getString(R.string.game_scoreboard_tag))
                    .commit();

            FlashcardGamePlayFragment gamePlayFragment = (FlashcardGamePlayFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.game_play_tag));
            if( gamePlayFragment == null )
            {
                gamePlayFragment = new FlashcardGamePlayFragment();

                Bundle arguments = new Bundle();
                arguments.putString(getString(R.string.flashcard_term_argument), gameStatusFragment.getCurrentTerm());
                arguments.putBoolean(FlashcardGamePlayFragment.GAME_START, true);
                gamePlayFragment.setArguments(arguments);
            }

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.game_content, gamePlayFragment, getString(R.string.game_play_tag))
                    .commit();
        }
    }

    protected void showWelcomeFragment(boolean connected)
    {
        welcomeFragment = (GameWelcomeFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.game_welcome_tag));
        if( welcomeFragment == null )
        {
            welcomeFragment = new GameWelcomeFragment();
        }

        Bundle arguments = new Bundle();
        arguments.putBoolean(getString(R.string.logged_in_argument), connected);
        welcomeFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.game_content, welcomeFragment, getString(R.string.game_welcome_tag))
                .commit();
    }

    @Override
    public DrawerLayout getDrawerLayout()
    {
        return gameLayout;
    }

    protected void runningAutoSignIn()
    {
        welcomeFragment.runningAutoSignIn();
    }

    @Override
    public void signInPressed()
    {
        gameStatusFragment.handleSignIn();
    }

    @Override
    public void signOutPressed()
    {
        gameStatusFragment.handleSignOut();
    }

    @Override
    public void singlePlayerPressed(boolean allSubjects, Id subjectId)
    {
        gameStatusFragment.startSinglePlayer(allSubjects, subjectId);
    }

    @Override
    public void inviteOtherPlayersPressed(boolean allSubjects, Id subjectId)
    {
        gameStatusFragment.setMultiplayerGameState(allSubjects, subjectId);
        Intent invitePlayersIntent = Games.RealTimeMultiplayer.getSelectOpponentsIntent(gameStatusFragment.getGoogleApiClient(), 1, 3);
        switchToScreen(SCREEN.WAITING_SCREEN);
        startActivityForResult(invitePlayersIntent, RC_SELECT_PLAYERS);
    }

    @Override
    public void viewInvitesPressed(boolean allSubjects, Id subjectId)
    {
        gameStatusFragment.setMultiplayerGameState(allSubjects, subjectId);
        Intent viewInvitationsIntent = Games.Invitations.getInvitationInboxIntent(gameStatusFragment.getGoogleApiClient());
        switchToScreen(SCREEN.WAITING_SCREEN);
        startActivityForResult(viewInvitationsIntent, RC_INVITATION_INBOX);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if( loadingDialog != null )
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    public View getRootView()
    {
        return findViewById(R.id.game_layout);
    }

    @Override
    public void showLeaderboard()
    {
        if( gameStatusFragment.getGoogleApiClient() != null && gameStatusFragment.getGoogleApiClient().isConnected() )
        {
            startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(gameStatusFragment.getGoogleApiClient()),
                    RC_UNUSED);
        }
        else
        {
            BaseGameUtils.showAlert(this, getString(R.string.leaderboard_not_available));
        }
    }

    @Override
    public void showAchievements()
    {
        if( gameStatusFragment.getGoogleApiClient() != null && gameStatusFragment.getGoogleApiClient().isConnected() )
        {
            startActivityForResult(Games.Achievements.getAchievementsIntent(gameStatusFragment.getGoogleApiClient()),
                    RC_UNUSED);
        }
        else
        {
            BaseGameUtils.showAlert(this, getString(R.string.leaderboard_not_available));
        }
    }

    @Override
    public void invitePopupAccepted()
    {
        gameStatusFragment.acceptInviteToRoom(gameStatusFragment.getIncomingInvitationId());
    }

    // Handle back key to make sure we cleanly leave a game if we are in the middle of one
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e)
    {
        if( keyCode == KeyEvent.KEYCODE_BACK && currentScreen == SCREEN.GAME_SCREEN )
        {
            gameStatusFragment.leaveRoom();
            return true;
        }
        return super.onKeyDown(keyCode, e);
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode,
                                 Intent intent)
    {
        super.onActivityResult(requestCode, responseCode, intent);

        switch( requestCode )
        {
            case RC_SELECT_PLAYERS:
                // we got the result from the "select players" UI -- ready to create the room
                gameStatusFragment.handleSelectPlayersResult(responseCode, intent);
                break;
            case RC_INVITATION_INBOX:
                // we got the result from the "select invitation" UI (invitation inbox). We're
                // ready to accept the selected invitation:
                handleInvitationInboxResult(responseCode, intent);
                break;
            case RC_WAITING_ROOM:

                // ignore result if we dismissed the waiting room from code:
                if( gameStatusFragment.getGameStartingWaitDismissed() )
                {
                    break;
                }

                // we got the result from the "waiting room" UI.
                if( responseCode == Activity.RESULT_OK )
                {
                    // player wants to start playing
                    Log.d(getString(R.string.log_tag), "Starting game because user requested via waiting room UI.");

                    // let other players know we're starting.
                    gameStatusFragment.broadcastStart();

                    // start the game!
                    gameStatusFragment.startGame(true);
                }
                else if( responseCode == GamesActivityResultCodes.RESULT_LEFT_ROOM )
                {
                    // player actively indicated that they want to leave the room
                    gameStatusFragment.leaveRoom();
                }
                else if( responseCode == Activity.RESULT_CANCELED )
                {
                    /* Dialog was cancelled (user pressed back key, for
                     * instance). In our game, this means leaving the room too. In more
                     * elaborate games,this could mean something else (like minimizing the
                     * waiting room UI but continue in the handshake process). */
                    gameStatusFragment.leaveRoom();
                }
                break;
            case RC_SIGN_IN:
                Log.d(getString(R.string.log_tag), "onActivityResult with requestCode == RC_SIGN_IN, responseCode="
                        + responseCode + ", intent=" + intent);
                gameStatusFragment.setResolvingConnectionFailure(false);
                if (responseCode == RESULT_OK) {
                    gameStatusFragment.getGoogleApiClient().connect();
                } else {
                    BaseGameUtils.showActivityResultError(this, requestCode, responseCode, R.string.sign_in_error);
                }
                break;
        }
    }

    @Override
    public void timeUp()
    {
        gameStatusFragment.endGame();
    }

    @Override
    public void gameFinished()
    {
        Bundle arguments = new Bundle();
        arguments.putBoolean(getString(R.string.logged_in_argument), gameStatusFragment.getGoogleApiClient().isConnected());
        welcomeFragment.setArguments(arguments);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.game_content, welcomeFragment, getString(R.string.game_welcome_tag))
                .commit();

        getSupportFragmentManager().beginTransaction()
                .remove(scoreboardFragment)
                .commit();
    }

    @Override
    public void questionAnswered(String answer)
    {
        gameStatusFragment.questionAnswered(answer);
    }

    protected void startGame(String firstTerm, List<String> displayNames, int secondsLeft)
    {
        scoreboardFragment = (ScoreboardFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.game_scoreboard_tag));
        if( scoreboardFragment == null )
        {
            scoreboardFragment = new ScoreboardFragment();
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.scoreboard, scoreboardFragment, getString(R.string.game_scoreboard_tag))
                .commit();

        FlashcardGamePlayFragment gamePlayFragment = (FlashcardGamePlayFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.game_play_tag));
        if( gamePlayFragment == null )
        {
            gamePlayFragment = new FlashcardGamePlayFragment();
        }
        Bundle arguments = new Bundle();
        arguments.putString(getString(R.string.flashcard_term_argument), firstTerm);
        arguments.putBoolean(FlashcardGamePlayFragment.GAME_START, true);
        gamePlayFragment.setArguments(arguments);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.game_content, gamePlayFragment, getString(R.string.game_play_tag))
                .commit();

        scoreboardFragment.setPlayerNames(displayNames);
        scoreboardFragment.startGameClock(secondsLeft);
    }

    // updates the label that shows my score
    protected void updateScoreDisplay(Score newScore)
    {
        scoreboardFragment.updateScore(0, newScore);
    }

    protected void endGame(boolean multiplayer, String finalScore, String placeString)
    {
        scoreboardFragment.hideClock();

        GameEndFragment gameEndFragment = (GameEndFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.game_end_tag));
        if( gameEndFragment == null )
        {
            gameEndFragment = new GameEndFragment();
        }

        Bundle arguments = new Bundle();

        if( multiplayer )
        {
            arguments.putString(GameEndFragment.PLACE_ARGUMENT, placeString);
        }

        arguments.putString(GameEndFragment.SCORE_ARGUMENT, finalScore);
        gameEndFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.game_content, gameEndFragment, getString(R.string.game_end_tag))
                .commit();
    }

    //Moves the game to the next flashcard
    protected void moveToNextFlashcard(boolean correct, String term, String previousCorrectAnswer, boolean waitForOtherPlayers)
    {
        FlashcardGamePlayFragment gamePlayFragment = new FlashcardGamePlayFragment();

        Bundle arguments = new Bundle();
        arguments.putString(getString(R.string.flashcard_term_argument), term);
        arguments.putBoolean(FlashcardGamePlayFragment.GAME_START, false);
        arguments.putBoolean(FlashcardGamePlayFragment.GAME_WAIT, waitForOtherPlayers);
        if( !correct )
        {
            arguments.putString(FlashcardGamePlayFragment.CORRECT_ANSWER_ARGUMENT, previousCorrectAnswer);
        }

        gamePlayFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.game_content, gamePlayFragment, getString(R.string.game_play_tag))
                .commit();

    }

    // Handle the result of the invitation inbox UI, where the player can pick an invitation
    // to accept. We react by accepting the selected invitation, if any.
    private void handleInvitationInboxResult(int response, Intent data)
    {
        if( response != Activity.RESULT_OK )
        {
            Log.w(getString(R.string.log_tag), "*** invitation inbox UI cancelled, " + response);
            switchToScreen(SCREEN.HOME_SCREEN);
            return;
        }

        Log.d(getString(R.string.log_tag), "Invitation inbox UI succeeded.");
        Invitation inv = data.getExtras().getParcelable(Multiplayer.EXTRA_INVITATION);

        // accept invitation
        if( inv != null )
        {
            gameStatusFragment.acceptInviteToRoom(inv.getInvitationId());
        }
    }

    // Sets the flag to keep this screen on, because if the screen turns off, the
    // game will be cancelled.
    protected void keepScreenOn()
    {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    // Clears the flag that keeps the screen on.
    protected void stopKeepingScreenOn()
    {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    protected void switchToScreen(SCREEN screen)
    {
        currentScreen = screen;

        // should we show the invitation popup?
        boolean showInvPopup;
        if( gameStatusFragment.getIncomingInvitationId() == null )
        {
            // no invitation, so no popup
            showInvPopup = false;
        }
        else if( gameStatusFragment.isMultiplayer() )
        {
            // if in multiplayer, only show invitation on main screen
            showInvPopup = (currentScreen == SCREEN.HOME_SCREEN);
        }
        else
        {
            // single-player: show on main screen and gameplay screen
            showInvPopup = (currentScreen == SCREEN.GAME_SCREEN || currentScreen == SCREEN.HOME_SCREEN);
        }

        LinearLayout popup = (LinearLayout) findViewById(R.id.invitation_popup);
        if( popup != null )
        {
            popup.setVisibility(showInvPopup ? View.VISIBLE : View.GONE);
        }
    }

    public void gameVoiceAnswer(View view)
    {
        gameStatusFragment.gameVoiceAnswer();
    }

    public void voiceAnswer(View v)
    {
        if ( ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED )
        {
            requestMicrophonePermission();
        }
        else
        {
            gameStatusFragment.gameVoiceAnswer();
        }
    }

    /**
     * Requests the microphone permission for listening to the answer.
     */
    private void requestMicrophonePermission() {
        Log.i(getString(R.string.log_tag), "Requesting storage permission");

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 )
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_MICROPHONE);
        }
    }

    //Handles request to get the storage permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_MICROPHONE)
        {
            //Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                gameStatusFragment.gameVoiceAnswer();
            }
        }
        else
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    protected void updateScore(int postion, Score newScore)
    {
        if( scoreboardFragment != null )
        {
            scoreboardFragment.updateScore(postion, newScore);
        }
    }

    protected void invitationRecieved(Invitation invitation)
    {
        TextView inviteText = (TextView) findViewById(R.id.incoming_invitation_text);
        if( inviteText != null )
        {
            inviteText.setText(
                    invitation.getInviter().getDisplayName() + " " +
                            getString(R.string.is_inviting_you)
            );
            switchToScreen(currentScreen);
        }
    }

    protected void loggedIn(GoogleApiClient googleApiClient)
    {
        welcomeFragment.toggleLoggedInState(true, Games.Players.getCurrentPlayer(googleApiClient).getDisplayName());
    }

    protected void loggedOut()
    {
        welcomeFragment.toggleLoggedInState(false, "");
    }

    protected void showErrorMessage(int message)
    {
        welcomeFragment.showErrorMessage(message);
    }

    protected void hideErrorMessage()
    {
        welcomeFragment.hideErrorMessage();
    }

    protected ProgressDialog getLoadingDialog()
    {
        if( loadingDialog == null )
        {
            loadingDialog = new ProgressDialog(this);
        }
        return loadingDialog;
    }
}
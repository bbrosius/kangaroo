package com.bmb.kangaroo.game;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bmb.kangaroo.R;

import java.util.ArrayList;
import java.util.List;

public class ScoreboardFragment extends Fragment
{

    private RelativeLayout scoreboardLayout;

    public ScoreboardFragment()
    {

    }

    private TimeUpCallback callback = timeUpCallback;

    private ImageView gameClockMinutes;
    private ImageView gameClockTenSeconds;
    private ImageView gameClockSeconds;
    private LinearLayout clockLayout;

    private List<String> names = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        scoreboardLayout = (RelativeLayout) inflater.inflate(R.layout.game_scoreboard_layout, container, false);
        clockLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.game_clock);
        gameClockMinutes = (ImageView) clockLayout.findViewById(R.id.game_clock_minutes);
        gameClockTenSeconds = (ImageView) clockLayout.findViewById(R.id.game_clock_tens_seconds);
        gameClockSeconds = (ImageView) clockLayout.findViewById(R.id.game_clock_seconds);

        LinearLayout scoreLayout = null;
        for( int player = 0; player < names.size(); player++ )
        {
            switch( player )
            {
                case 0:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_one_score);
                    break;
                case 1:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_two_score);
                    break;
                case 2:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_three_score);
                    break;
                case 3:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_four_score);
                    break;
            }

            TextView playerName = (TextView) scoreLayout.findViewById(R.id.score_name);
            if( playerName != null )
            {
                playerName.setText(names.get(player));
            }
        }

        return scoreboardLayout;
    }

    public void setPlayerNames(List<String> displayNames)
    {
        names = displayNames;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if( !(activity instanceof TimeUpCallback) )
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callback = (TimeUpCallback) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        callback = timeUpCallback;
    }

    public void startGameClock(int time)
    {
        CountDownTimer gameClock = new CountDownTimer(time, 1000)
        {
            @Override
            public void onFinish()
            {
                gameClockSeconds.setImageResource(R.drawable.zero);
                callback.timeUp();
            }

            @Override
            public void onTick(long millisUntilFinished)
            {
                parseTimeLeft(millisUntilFinished);
            }
        };

        gameClock.start();
    }

    public void hideClock()
    {
        clockLayout.setVisibility(View.INVISIBLE);
    }

    private void parseTimeLeft(long millisUntilFinished)
    {
        double seconds = (millisUntilFinished / 1000d);
        int minutes = (int) Math.floor(seconds/60.0);

        if( seconds > 60 )
        {
            seconds = (int) (seconds % 60d);
        }

        if( gameClockSeconds != null && gameClockMinutes != null && gameClockTenSeconds != null )
        {
            gameClockMinutes.setImageResource(convertNumberToImage(minutes));
            gameClockTenSeconds.setImageResource(convertNumberToImage((int) (seconds / 10)));
            gameClockSeconds.setImageResource(convertNumberToImage((int) (seconds % 10)));
        }
    }

    public void updateScore(int player, Score score)
    {
        LinearLayout scoreLayout = null;
        if( score != null && score.getScore() != null && !score.getScore().isEmpty() )
        {
            int scoreValue = Integer.parseInt(score.getScore());

            switch( player )
            {
                case 0:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_one_score);
                    break;
                case 1:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_two_score);
                    break;
                case 2:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_three_score);
                    break;
                case 3:
                    scoreLayout = (LinearLayout) scoreboardLayout.findViewById(R.id.player_four_score);
                    break;
            }

            if( scoreLayout != null )
            {
                TextView playerName = (TextView) scoreLayout.findViewById(R.id.score_name);
                if( playerName != null )
                {
                    playerName.setText(score.getName());
                }

                int scoreOnesDigit = scoreValue;
                if( scoreValue > 10 )
                {
                    ImageView myScoreTens = (ImageView) scoreboardLayout.findViewById(R.id.score_tens_value);
                    myScoreTens.setImageResource(convertNumberToImage(scoreValue / 10));
                    scoreOnesDigit = (int) (((double) scoreValue) % 10);
                }

                ImageView myScoreSingleDigits = (ImageView) scoreboardLayout.findViewById(R.id.score_ones_value);
                myScoreSingleDigits.setImageResource(convertNumberToImage(scoreOnesDigit));
            }
        }
        scoreboardLayout.postInvalidate();
    }

    public interface TimeUpCallback
    {
        void timeUp();
    }

    private final static TimeUpCallback timeUpCallback = new TimeUpCallback()
    {

        @Override
        public void timeUp()
        {

        }
    };

    private int convertNumberToImage(int number)
    {
        switch( number )
        {
            case 0:
                return R.drawable.zero;
            case 1:
                return R.drawable.one;
            case 2:
                return R.drawable.two;
            case 3:
                return R.drawable.three;
            case 4:
                return R.drawable.four;
            case 5:
                return R.drawable.five;
            case 6:
                return R.drawable.six;
            case 7:
                return R.drawable.seven;
            case 8:
                return R.drawable.eight;
            case 9:
                return R.drawable.nine;
        }

        return R.drawable.zero;
    }
}

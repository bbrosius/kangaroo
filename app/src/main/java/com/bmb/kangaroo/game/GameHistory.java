package com.bmb.kangaroo.game;

import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A class for storing the history of the game. Namely number of questions answered correctly and total number answered.
 */
public class GameHistory
{
    // serialization format version
    private static final String VERSION = "1.0";

    private static final String JSON_VERSION = "version";
    private static final String JSON_ANSWERED = "answered";
    private static final String JSON_ASKED = "asked";
    private static final String JSON_GREENHORN = "greenhorn";
    private static final String JSON_ACED_THE_TEST = "aced_the_test";
    private static final String JSON_HEAD_OF_THE_CLASS = "head_of_the_class";
    private static final String JSON_TEACHERS_PET = "teachers_pet";
    private static final String JSON_TOUGH_TEST = "tough_test";
    private static final String JSON_VALEDICTORIAN = "valedictorian";

    private int correctAnswers;
    private int questionsAsked;
    private boolean greenhornUnlocked = false;
    private boolean acedTheTestUnlocked = false;
    private boolean headOfTheClassUnlocked = false;
    private boolean teachersPetUnlocked = false;
    private boolean toughTestUnlocked = false;
    private boolean valedictorianUnlocked = false;

    public GameHistory()
    {
    }

    /**
     * Constructs a GameHistory object from serialized data.
     */
    public GameHistory(byte[] data)
    {
        if( data == null )
        {
            return;
        }
        loadFromJson(new String(data));
    }

    /**
     * Constructs a GameHistory object from a JSON string.
     */
    public GameHistory(String json)
    {
        if( json == null )
        {
            return;
        }
        loadFromJson(json);
    }

    /**
     * Constructs a GameHistory object by reading from a SharedPreferences.
     */
    public GameHistory(SharedPreferences sp, String key)
    {
        loadFromJson(sp.getString(key, ""));
    }

    /**
     * Replaces this GameHistory's content with the content loaded from the given JSON string.
     */
    private void loadFromJson(String json)
    {
        zero();
        if( json == null || json.trim().equals("") )
        {
            return;
        }

        try
        {
            JSONObject obj = new JSONObject(json);
            String format = obj.getString(JSON_VERSION);
            if( !format.equals(VERSION) )
            {
                throw new RuntimeException("Unexpected game history format " + format);
            }
            if( obj.has(JSON_ANSWERED) )
            {
                correctAnswers = obj.getInt(JSON_ANSWERED);
            }

            if( obj.has(JSON_ASKED) )
            {
                questionsAsked = obj.getInt(JSON_ASKED);
            }

            if( obj.has(JSON_GREENHORN) )
            {
                greenhornUnlocked = obj.getBoolean(JSON_GREENHORN);
            }

            if( obj.has(JSON_ACED_THE_TEST) )
            {
                acedTheTestUnlocked = obj.getBoolean(JSON_ACED_THE_TEST);
            }

            if( obj.has(JSON_HEAD_OF_THE_CLASS) )
            {
                headOfTheClassUnlocked = obj.getBoolean(JSON_HEAD_OF_THE_CLASS);
            }

            if( obj.has(JSON_TEACHERS_PET) )
            {
                teachersPetUnlocked = obj.getBoolean(JSON_TEACHERS_PET);
            }

            if( obj.has(JSON_TOUGH_TEST) )
            {
                toughTestUnlocked = obj.getBoolean(JSON_TOUGH_TEST);
            }

            if( obj.has(JSON_VALEDICTORIAN) )
            {
                valedictorianUnlocked = obj.getBoolean(JSON_VALEDICTORIAN);
            }
        }
        catch( JSONException ex )
        {
            ex.printStackTrace();
            throw new RuntimeException("Save data has a syntax error: " + json, ex);
        }
        catch( NumberFormatException ex )
        {
            ex.printStackTrace();
            throw new RuntimeException("Save data has an invalid number in it: " + json, ex);
        }
    }

    /**
     * Serializes this GameHistory to an array of bytes.
     */
    public byte[] toBytes()
    {
        return toString().getBytes();
    }

    /**
     * Serializes this GameHistory to a JSON string.
     */
    @Override
    public String toString()
    {
        try
        {
            JSONObject obj = new JSONObject();
            obj.put(JSON_VERSION, VERSION);
            obj.put(JSON_ANSWERED, correctAnswers);
            obj.put(JSON_ASKED, questionsAsked);
            obj.put(JSON_GREENHORN, greenhornUnlocked);
            obj.put(JSON_ACED_THE_TEST, acedTheTestUnlocked);
            obj.put(JSON_HEAD_OF_THE_CLASS, headOfTheClassUnlocked);
            obj.put(JSON_TEACHERS_PET, teachersPetUnlocked);
            obj.put(JSON_TOUGH_TEST, toughTestUnlocked);
            obj.put(JSON_VALEDICTORIAN, valedictorianUnlocked);

            return obj.toString();
        }
        catch( JSONException ex )
        {
            ex.printStackTrace();
            throw new RuntimeException("Error converting save data to JSON.", ex);
        }
    }

    /**
     * Computes the union of this GameHistory with the given GameHistory. The union take the highest of questions asked and correct answers.
     *
     * @param other The other operand with which to compute the union.
     * @return The result of the union.
     */
    public GameHistory unionWith(GameHistory other)
    {
        GameHistory result = clone();
        result.setCorrectAnswers(Math.max(result.getCorrectAnswers(), other.getCorrectAnswers()));
        result.setQuestionsAsked(Math.max(result.getQuestionsAsked(), other.getQuestionsAsked()));
        if( greenhornUnlocked || other.isGreenhornUnlocked() )
        {
            result.setGreenhornUnlocked(true);
        }
        else
        {
            result.setGreenhornUnlocked(false);
        }

        if( acedTheTestUnlocked || other.isAcedTheTestUnlocked() )
        {
            result.setAcedTheTestUnlocked(true);
        }
        else
        {
            result.setAcedTheTestUnlocked(false);
        }

        if( headOfTheClassUnlocked || other.isHeadOfTheClassUnlocked() )
        {
            result.setHeadOfTheClassUnlocked(true);
        }
        else
        {
            result.setHeadOfTheClassUnlocked(false);
        }

        if( teachersPetUnlocked || other.isTeachersPetUnlocked() )
        {
            result.setTeachersPetUnlocked(true);
        }
        else
        {
            result.setTeachersPetUnlocked(false);
        }

        if( toughTestUnlocked || other.isToughTestUnlocked() )
        {
            result.setToughTestUnlocked(true);
        }
        else
        {
            result.setToughTestUnlocked(false);
        }

        if( valedictorianUnlocked || other.isValedictorianUnlocked() )
        {
            result.setValedictorianUnlocked(true);
        }
        else
        {
            result.setValedictorianUnlocked(false);
        }
        return result;
    }

    /**
     * Returns a clone of this GameHistory object.
     */
    public GameHistory clone()
    {
        GameHistory result = new GameHistory();
        result.setCorrectAnswers(getCorrectAnswers());
        result.setQuestionsAsked(getQuestionsAsked());
        result.setGreenhornUnlocked(isGreenhornUnlocked());
        result.setAcedTheTestUnlocked(isAcedTheTestUnlocked());
        result.setTeachersPetUnlocked(isTeachersPetUnlocked());
        result.setHeadOfTheClassUnlocked(isHeadOfTheClassUnlocked());
        result.setToughTestUnlocked(isToughTestUnlocked());
        result.setValedictorianUnlocked(isValedictorianUnlocked());

        return result;
    }

    /**
     * Resets this GameHistory object to be empty. Empty means no stars on no levels.
     */
    private void zero()
    {
        correctAnswers = 0;
        questionsAsked = 0;
        greenhornUnlocked = false;
        acedTheTestUnlocked = false;
        teachersPetUnlocked = false;
        headOfTheClassUnlocked = false;
        toughTestUnlocked = false;
        valedictorianUnlocked = false;
    }

    /**
     * Returns whether or not this SaveGame is empty. Empty means no stars on no levels.
     */
    public boolean isZero()
    {
        return correctAnswers == 0 && questionsAsked == 0;
    }

    /**
     * Save this SaveGame object to a SharedPreferences.
     */
    public void save(SharedPreferences sp, String key)
    {
        SharedPreferences.Editor spe = sp.edit();
        spe.putString(key, toString());
        spe.apply();
    }

    /**
     * Gets how many stars the player has on the given level. If the level does not exist
     * in the save game, will return 0.
     */
    public int getCorrectAnswers()
    {
        return correctAnswers;
    }

    public int getQuestionsAsked()
    {
        return questionsAsked;
    }

    /**
     * Set the number of total correct answers the user got
     */
    private void setCorrectAnswers(int correctAnswers)
    {
        this.correctAnswers = correctAnswers;
    }

    /**
     * Set the number of questions the user was asked .
     */
    private void setQuestionsAsked(int questionsAsked)
    {
        this.questionsAsked = questionsAsked;
    }

    /**
     * Add to the number of correct answer the user got.
     */
    public void addCorrectAnswers(int correctAnswers)
    {
        this.correctAnswers += correctAnswers;
    }

    /**
     * Add to number of questions that have been asked of the user
     */
    public void addQuestionsAsked(int questionsAsked)
    {
        this.questionsAsked += questionsAsked;
    }

    public boolean isGreenhornUnlocked()
    {
        return greenhornUnlocked;
    }

    public void setGreenhornUnlocked(boolean greenhornUnlocked)
    {
        this.greenhornUnlocked = greenhornUnlocked;
    }

    public boolean isAcedTheTestUnlocked()
    {
        return acedTheTestUnlocked;
    }

    public void setAcedTheTestUnlocked(boolean acedTheTestUnlocked)
    {
        this.acedTheTestUnlocked = acedTheTestUnlocked;
    }

    public boolean isHeadOfTheClassUnlocked()
    {
        return headOfTheClassUnlocked;
    }

    public void setHeadOfTheClassUnlocked(boolean headOfTheClassUnlocked)
    {
        this.headOfTheClassUnlocked = headOfTheClassUnlocked;
    }

    public boolean isTeachersPetUnlocked()
    {
        return teachersPetUnlocked;
    }

    public void setTeachersPetUnlocked(boolean teachersPetUnlocked)
    {
        this.teachersPetUnlocked = teachersPetUnlocked;
    }

    public boolean isToughTestUnlocked()
    {
        return toughTestUnlocked;
    }

    public void setToughTestUnlocked(boolean toughTestUnlocked)
    {
        this.toughTestUnlocked = toughTestUnlocked;
    }

    public boolean isValedictorianUnlocked()
    {
        return valedictorianUnlocked;
    }

    public void setValedictorianUnlocked(boolean valedictorianUnlocked)
    {
        this.valedictorianUnlocked = valedictorianUnlocked;
    }

}

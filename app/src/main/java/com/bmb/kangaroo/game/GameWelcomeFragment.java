package com.bmb.kangaroo.game;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.SubjectSpinnerAdapter;
import com.google.android.gms.common.SignInButton;

import java.util.List;

public class GameWelcomeFragment extends Fragment
{
    public GameWelcomeFragment()
    {

    }

    private WelcomeScreenAction callback = gameSignInChangedCallback;
    private boolean loggedIn = false;
    private Button signOutButton;
    private SignInButton signInButton;
    private Button inviteOtherPlayersButton;
    private Button acceptInvitesButton;
    private Button showLeaderboardButton;
    private Button showAchievementsButton;
    private TextView signInExplanation;

    private TextView errorMessage;
    private String userName;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments().containsKey(getString(R.string.logged_in_argument)) )
        {
            loggedIn = getArguments().getBoolean(getString(R.string.logged_in_argument));
        }

        if( getArguments().containsKey(getString(R.string.player_name_argument)) )
        {
            userName = getArguments().getString(getString(R.string.player_name_argument));
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        FrameLayout welcomeLayout = (FrameLayout) inflater.inflate(R.layout.game_welcome_layout, null);

        signInButton = (SignInButton) welcomeLayout.findViewById(R.id.sign_in_button);
        signOutButton = (Button) welcomeLayout.findViewById(R.id.sign_out_button);

        signInButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.signInPressed();
            }
        });

        signOutButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                callback.signOutPressed();
                signOutButton.setVisibility(View.GONE);
                signInExplanation.setText(getString(R.string.sign_in_explanation));
                signInButton.setVisibility(View.VISIBLE);
                signInButton.setEnabled(true);
                signOutButton.setVisibility(View.GONE);
                inviteOtherPlayersButton.setEnabled(false);
                acceptInvitesButton.setEnabled(false);
                showLeaderboardButton.setEnabled(false);
                showAchievementsButton.setEnabled(false);
            }
        });

        signInExplanation = (TextView) welcomeLayout.findViewById(R.id.sign_in_explanation);

        errorMessage = (TextView) welcomeLayout.findViewById(R.id.game_error_message);

        final Spinner gameSubjectSpinner = (Spinner) welcomeLayout.findViewById(R.id.game_subject_spinner);
        final List<Subject> subjects = SubjectService.getInstance().getSubjects();
        SubjectSpinnerAdapter subjectSpinnerAdapter = new SubjectSpinnerAdapter(getActivity(), subjects);
        gameSubjectSpinner.setAdapter(subjectSpinnerAdapter);
        gameSubjectSpinner.setSelection(subjects.indexOf(SubjectService.getInstance().getCurrentSubject()));

        final CheckBox allSubjects = (CheckBox) welcomeLayout.findViewById(R.id.game_all_flashcards);
        Button singlePlayerButton = (Button) welcomeLayout.findViewById(R.id.play_now);
        singlePlayerButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.singlePlayerPressed(allSubjects.isChecked(), ((Subject)gameSubjectSpinner.getSelectedItem()).getId());
            }
        });

        inviteOtherPlayersButton = (Button) welcomeLayout.findViewById(R.id.invite_players_button);
        inviteOtherPlayersButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.inviteOtherPlayersPressed(allSubjects.isChecked(), ((Subject)gameSubjectSpinner.getSelectedItem()).getId());
            }
        });

        acceptInvitesButton = (Button) welcomeLayout.findViewById(R.id.view_invites);
        acceptInvitesButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.viewInvitesPressed(allSubjects.isChecked(), ((Subject)gameSubjectSpinner.getSelectedItem()).getId());
            }
        });

        showLeaderboardButton = (Button) welcomeLayout.findViewById(R.id.show_leaderboard);
        showLeaderboardButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.showLeaderboard();
            }
        });

        showAchievementsButton = (Button) welcomeLayout.findViewById(R.id.show_achievements);
        showAchievementsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.showAchievements();
            }
        });

        Button invitePopupButton = (Button) welcomeLayout.findViewById(R.id.button_accept_popup_invitation);
        invitePopupButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.invitePopupAccepted();
            }
        });

        if( loggedIn )
        {
            signInExplanation.setText(getString(R.string.welcome) + " " + userName);
            signInButton.setVisibility(View.GONE);
            signInButton.setEnabled(true);
            signOutButton.setVisibility(View.VISIBLE);
            inviteOtherPlayersButton.setEnabled(true);
            acceptInvitesButton.setEnabled(true);
            showLeaderboardButton.setEnabled(true);
            showAchievementsButton.setEnabled(true);
        }
        else
        {
            signInExplanation.setText(getString(R.string.sign_in_explanation));
            signInButton.setVisibility(View.VISIBLE);
            signInButton.setEnabled(true);
            signOutButton.setVisibility(View.GONE);
            inviteOtherPlayersButton.setEnabled(false);
            acceptInvitesButton.setEnabled(false);
            showLeaderboardButton.setEnabled(false);
            showAchievementsButton.setEnabled(false);
        }

        return welcomeLayout;
    }

    public void runningAutoSignIn()
    {
        signInButton.setEnabled(false);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if( !(activity instanceof WelcomeScreenAction) )
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callback = (WelcomeScreenAction) activity;
    }

    /**
     * Adjusts the buttons to show correct button for correct logged in state
     *
     * @param loggedIn boolean flag marking if user is logged in or not.
     */
    public void toggleLoggedInState(boolean loggedIn, String playerName)
    {
        this.loggedIn = loggedIn;
        this.userName = playerName;
        if( loggedIn )
        {
            signInExplanation.setText(getString(R.string.welcome) + " " + playerName);
            signInButton.setVisibility(View.GONE);
            signInButton.setEnabled(true);
            signOutButton.setVisibility(View.VISIBLE);
            inviteOtherPlayersButton.setEnabled(true);
            acceptInvitesButton.setEnabled(true);
            showLeaderboardButton.setEnabled(true);
            showAchievementsButton.setEnabled(true);
        }
        else
        {
            signInExplanation.setText(getString(R.string.sign_in_explanation));
            signInButton.setVisibility(View.VISIBLE);
            signInButton.setEnabled(true);
            signOutButton.setVisibility(View.GONE);
            inviteOtherPlayersButton.setEnabled(false);
            acceptInvitesButton.setEnabled(false);
            showLeaderboardButton.setEnabled(false);
            showAchievementsButton.setEnabled(false);
        }
    }

    public void showErrorMessage(int resId)
    {
        if( errorMessage != null )
        {
            errorMessage.setText(getString(resId));
            errorMessage.setVisibility(View.VISIBLE);
        }
    }

    public void hideErrorMessage() {
        errorMessage.setVisibility(View.GONE);
    }

    public interface WelcomeScreenAction
    {
        void signInPressed();

        void signOutPressed();

        void singlePlayerPressed(boolean allSubjects, Id subjectId);

        void inviteOtherPlayersPressed(boolean allSubjects, Id subjectId);

        void viewInvitesPressed(boolean allSubjects, Id subjectId);

        void showLeaderboard();

        void showAchievements();

        void invitePopupAccepted();
    }

    /**
     * A dummy implementation of the {@link com.bmb.kangaroo.bluetooth.BluetoothHostStudyFragment.BluetoothFlashcardAnsweredCallback} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private final static WelcomeScreenAction gameSignInChangedCallback = new WelcomeScreenAction()
    {
        @Override
        public void signInPressed() {}

        @Override
        public void signOutPressed() {}

        @Override
        public void singlePlayerPressed(boolean allSubjects, Id subjectId) {}

        @Override
        public void inviteOtherPlayersPressed(boolean allSubjects, Id subjectId) {}

        @Override
        public void viewInvitesPressed(boolean allSubjects, Id subjectId) {}

        @Override
        public void showLeaderboard() {}

        @Override
        public void showAchievements() {}

        @Override
        public void invitePopupAccepted() {}
    };
}

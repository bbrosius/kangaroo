package com.bmb.kangaroo.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateUtils;

import com.bmb.kangaroo.MainFlashcardSetListActivity;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.SubjectPreferenceActivity;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.ExamService;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;

import java.util.Calendar;
import java.util.List;

public class ExamNotificationService extends IntentService
{
	private final static int EXAM_NOTIFICATION_ID = 456;
	public final static String EXAM_ID_EXTRA = "EXAM_ID";

	public ExamNotificationService() 
	{
		super("ExamNotificationService");
	}

	@Override
	protected void onHandleIntent(Intent intent) 
	{
		SharedPreferences pref = getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
        boolean notificationsEnabled = pref.getBoolean(getString(R.string.notification_enabled_key), true);
        if( notificationsEnabled )
        {
        	NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        	
        	Intent notificationIntent = new Intent(this, MainFlashcardSetListActivity.class);
        	PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent , 0);
        	
        	if(intent.hasExtra(EXAM_ID_EXTRA))
        	{
        		Id examId = IdService.resolveId(intent.getStringExtra(EXAM_ID_EXTRA));
        		Exam exam = ExamService.getInstance().getExam(examId);
        		Subject examSubject = SubjectService.getInstance().getSubject(exam.getSubjectId());

                Calendar nextExamDate = exam.getExamDate();
                long millisInTheFuture = nextExamDate.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        		String examDateString = parseTimeLeft(millisInTheFuture);
                NotificationCompat.Builder builder =  new NotificationCompat.Builder(this)
        		.setContentTitle(exam.getTitle() + " " + getString(R.string.in) + " " + examDateString)
        		.setContentText(getString(R.string.exam_notification_subject) + " " + examSubject.getTitle() + " " + getString(R.string.goal) + ": " + exam.getGradeGoal())
        		.setSmallIcon(R.drawable.icon)
        		.setContentIntent(contentIntent);
        		
        		NotificationCompat.InboxStyle inboxStyle =
        				new NotificationCompat.InboxStyle();
        		
        		int notificationNoteListSize = 5;
                List<Id> flashcardSetIds = SubjectService.getInstance().getFlashcardSetsForExam(exam);

        		if(flashcardSetIds.size() < 5)
        		{
        			notificationNoteListSize = flashcardSetIds.size();
        		}
        		else
        		{
        			inboxStyle.setSummaryText(flashcardSetIds.size() - 5 + getString(R.string.exam_notification_additional_flashcards));
        		}
        		
        		for(int i = 0; i < notificationNoteListSize; i++)
                {
        			Id flashcardSetId = flashcardSetIds.get(i);
        			FlashcardSet set = FlashcardSetService.getInstance().getFlashcardSet(flashcardSetId);
        			inboxStyle.addLine(set.getTitle());
        		}
        		
        		builder.setStyle(inboxStyle);
        		Notification notification = builder.build();
        		
        		nm.notify(EXAM_NOTIFICATION_ID, notification);
        	}
        }
	}

    private String parseTimeLeft(long millisUntilFinished)
    {
        StringBuilder time = new StringBuilder();
        time.setLength(0);
        // Use days if appropriate
        if(millisUntilFinished > DateUtils.DAY_IN_MILLIS) {
            long count = millisUntilFinished / DateUtils.DAY_IN_MILLIS;
            if(count > 1)
                time.append(count).append(" ").append(getString(R.string.days)).append(" ");
            else
                time.append(count).append(" ").append(getString(R.string.day)).append(" ");

            millisUntilFinished %= DateUtils.DAY_IN_MILLIS;
        }

        time.append(DateUtils.formatElapsedTime(Math.round(millisUntilFinished / 1000d)));

        return time.toString();
    }
	
}
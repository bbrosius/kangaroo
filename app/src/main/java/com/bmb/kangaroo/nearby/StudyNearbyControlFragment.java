package com.bmb.kangaroo.nearby;

import android.app.Activity;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.NearbyMessagesStatusCodes;
import com.google.android.gms.nearby.messages.Strategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class StudyNearbyControlFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    protected static final String ARG_STUDY_ID = "arg_studyId";

    private final static String FLASHCARD_TERM_IMAGE = "termImage";
    private final static String FLASHCARD_TERM = "term";
    private final static String SCORE_UPDATE = "score";

    private ArrayList<Integer> incorrectPositions;
    private ArrayList<Integer> skippedPositions;

    private boolean host = true;

    private Id studyId;
    private GoogleApiClient googleApiClient;

    private Cursor flashcards;

    private FlashcardDAO flashcardDao;
    private int questionsCompleted = 0;
    private int questionsCorrect = 0;

    private boolean roleSelected = false;
    private StudyNearbyActivity studyActivity;

    private String currentTerm;
    private String currentDefinition;

    private boolean currentTermIsImage = false;

    private Message currentMessage;
    private MessageListener messageListener;
    private boolean resolvingNearbyPermissionError;

    private static final Strategy PUB_SUB_STRATEGY = new Strategy.Builder()
            .setTtlSeconds(StudyNearbyActivity.TTL_IN_SECONDS).build();

    public static StudyNearbyControlFragment newInstance(Id studyId)
    {
        StudyNearbyControlFragment fragment = new StudyNearbyControlFragment();
        Bundle args = new Bundle();
        if( studyId != null )
        {
            args.putString(ARG_STUDY_ID, studyId.toString());
        }
        fragment.setArguments(args);
        return fragment;
    }

    public StudyNearbyControlFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        if( getArguments() != null )
        {
            studyId = IdService.resolveId(getArguments().getString(ARG_STUDY_ID));
        }

        if( googleApiClient == null )
        {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(Nearby.MESSAGES_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }

        messageListener = new MessageListener() {
            @Override
            public void onFound(final Message message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String messageJSONString = new String(message.getContent()).trim();
                        handleJSONMessage(messageJSONString);
                    }
                });
            }

            @Override
            public void onLost(final Message message) {
                // Called when a message is no longer detectable nearby.
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //TODO notify guest fragment of message loss?
                    }
                });
            }
        };
        studyActivity.showRoleSelectionFragment();
    }

    private void handleJSONMessage(String jsonString)
    {
        try
        {
            JSONObject messageObject = new JSONObject(jsonString);
            if( messageObject.has(SCORE_UPDATE) )
            {
                studyActivity.setTitle(messageObject.getString(SCORE_UPDATE));
            }
            else if( messageObject.has(FLASHCARD_TERM) )
            {
                currentTerm = messageObject.getString(FLASHCARD_TERM);
                currentTermIsImage = false;
                studyActivity.newFlashcardReceived(currentTerm, false);
            }
            else if( messageObject.has(FLASHCARD_TERM_IMAGE) )
            {
                currentTerm = messageObject.getString(FLASHCARD_TERM_IMAGE);
                currentTermIsImage = true;
                studyActivity.newFlashcardReceived(currentTerm, true);
            }
        } catch( JSONException e )
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        studyActivity = (StudyNearbyActivity) activity;
        if( googleApiClient == null )
        {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(Nearby.MESSAGES_API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
    }

    @Override
    public void onStop() {
        if (googleApiClient.isConnected() && !getActivity().isChangingConfigurations()) {
            // Using Nearby is battery intensive. To preserve battery, stop subscribing or
            // publishing when the fragment is inactive.
            unsubscribe();
            unpublish();

            googleApiClient.disconnect();
        }

        if( flashcardDao != null )
        {
            flashcardDao.close();
        }
        super.onStop();
    }

    private void unsubscribe() {
        Log.i(getString(R.string.log_tag), "trying to unsubscribe");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!googleApiClient.isConnected()) {
            if (!googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        }
        else
        {
            Nearby.Messages.unsubscribe(googleApiClient, messageListener)
                    .setResultCallback(new ResultCallback<Status>()
                    {

                        @Override
                        public void onResult(Status status)
                        {
                            if( isAdded() )
                            {
                                if( status.isSuccess() )
                                {
                                    Log.i(getString(R.string.log_tag), "unsubscribed successfully");
                                }
                                else
                                {
                                    Log.i(getString(R.string.log_tag), "could not unsubscribe");
                                    handleUnsuccessfulNearbyResult(status);
                                }
                            }
                        }
                    });
        }
    }

    private void unpublish() {
        Log.i(getString(R.string.log_tag), "trying to unpublish");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!googleApiClient.isConnected()) {
            if( !googleApiClient.isConnecting() )
            {
                googleApiClient.connect();
            }
        } else {
            if( currentMessage != null )
            {
                Nearby.Messages.unpublish(googleApiClient, currentMessage)
                        .setResultCallback(new ResultCallback<Status>()
                        {

                            @Override
                            public void onResult(Status status)
                            {
                                if( status.isSuccess() )
                                {
                                    Log.i(getString(R.string.log_tag), "unpublished successfully");
                                }
                                else
                                {
                                    Log.i(getString(R.string.log_tag), "could not unpublish");
                                    handleUnsuccessfulNearbyResult(status);
                                }
                            }
                        });
            }
        }
    }

    protected void finishedResolvingNearbyPermissionError() {
        resolvingNearbyPermissionError = false;
    }

    private void handleUnsuccessfulNearbyResult(Status status) {
        Log.i(getString(R.string.log_tag), "processing error, status = " + status);
        if (status.getStatusCode() == NearbyMessagesStatusCodes.APP_NOT_OPTED_IN) {
            if (!resolvingNearbyPermissionError ) {
                try {
                    resolvingNearbyPermissionError = true;
                    status.startResolutionForResult(getActivity(),
                            StudyNearbyActivity.REQUEST_RESOLVE_ERROR);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (status.getStatusCode() == ConnectionResult.NETWORK_ERROR) {
                Toast.makeText(getActivity().getApplicationContext(),
                        "No connectivity, cannot proceed. Fix in 'Settings' and try again.",
                        Toast.LENGTH_LONG).show();
                resetToDefaultState(true);
            } else {
                // To keep things simple, pop a toast for all other error messages.
                Toast.makeText(getActivity().getApplicationContext(), "Unsuccessful: " +
                        status.getStatusMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        Log.i(getString(R.string.log_tag), "GoogleApiClient connected");
        executePendingTasks();
    }

    @Override
    public void onConnectionSuspended(int i)
    {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {

    }

    public boolean roleSelected()
    {
        return roleSelected;
    }

    public void setRoleSelected(boolean selected)
    {
        roleSelected = selected;
    }

    protected void executePendingTasks()
    {
        if( host )
        {
            sendText(currentTerm);
        }
        else
        {
            subscribe();
        }
    }
    public void onRoleSelected(boolean host)
    {
        this.host = host;
        roleSelected = true;

        if( host )
        {
            incorrectPositions = new ArrayList<>();
            skippedPositions = new ArrayList<>();
            flashcardDao = new FlashcardDAO();
            flashcardDao.open();

            if( studyId != null )
            {
                flashcards = flashcardDao.findFlashcards(studyId, true);
                flashcards.moveToFirst();
                currentTerm = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                currentDefinition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                sendText(currentTerm);

                studyActivity.updateFlashcard();
            }
            else
            {
                Snackbar.make(studyActivity.getDrawerLayout(), R.string.nearby_no_subjects,
                        Snackbar.LENGTH_LONG).show();
            }
        }
        else
        {
            studyActivity.newFlashcardReceived(getString(R.string.awaiting_host), false);
            subscribe();
        }
    }

    public boolean isHost()
    {
        return host;
    }

    public String getCurrentTerm()
    {
        return currentTerm;
    }

    public String getCurrentDefinition()
    {
        return currentDefinition;
    }

    public boolean isCurrentTermImage()
    {
        return currentTermIsImage;
    }

    protected void restartFlashcards()
    {
        if( studyId != null )
        {
            flashcards = flashcardDao.findFlashcards(studyId, true);
            flashcards.moveToFirst();
            currentTerm = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
            currentDefinition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
            sendText(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
        }
        resetToDefaultState(false);
    }

    protected String flashcardAnswered(boolean correct, boolean skipped)
    {
        String scoreLine;
        if( !skipped )
        {
            if( questionsCompleted < flashcards.getCount() )
            {
                questionsCompleted++;
            }

            if( correct )
            {
                questionsCorrect++;
            }
            else
            {
                incorrectPositions.add(flashcards.getPosition());
            }
        }
        else
        {
            skippedPositions.add(flashcards.getPosition());
        }

        scoreLine = String.format(getString(R.string.quiz_score), questionsCorrect, questionsCompleted);

        sendText("#score:" + scoreLine);

        if( questionsCompleted < flashcards.getCount() )
        {
            if( flashcards.isLast() )
            {
                if( !skippedPositions.isEmpty() )
                {
                    flashcards.moveToPosition(skippedPositions.get(0));
                    currentTerm = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                    currentDefinition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                    studyActivity.updateFlashcard();
                    skippedPositions.remove(0);

                    sendText(currentTerm);
                }
                else if( !incorrectPositions.isEmpty() )
                {
                    flashcards.moveToPosition(incorrectPositions.get(0));
                    currentTerm = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                    currentDefinition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                    studyActivity.updateFlashcard();
                    incorrectPositions.remove(0);

                    sendText(currentTerm);
                }
            }
            else
            {
                flashcards.moveToNext();
                currentTerm = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                currentDefinition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                sendText(currentTerm);
                studyActivity.updateFlashcard();
            }
        }
        else
        {
            String restartMessage = getString(R.string.restart_flashcards) + scoreLine;
            studyActivity.showRestartCard(restartMessage);
        }

        return scoreLine;
    }

    private void sendText(String text)
    {
        JSONObject messageObject = createJsonForTerm(text);
        currentMessage = new Message(messageObject.toString().getBytes(Charset.forName("UTF-8")));
        if (!googleApiClient.isConnected()) {
            if (!googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        } else {
            Nearby.Messages.publish(googleApiClient, currentMessage, PUB_SUB_STRATEGY)
                    .setResultCallback(new ResultCallback<Status>()
                    {

                        @Override
                        public void onResult(Status status)
                        {
                            if( status.isSuccess() )
                            {
                                Log.i(getString(R.string.log_tag), "published successfully");
                            }
                            else
                            {
                                Log.i(getString(R.string.log_tag), "could not publish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }

    private JSONObject createJsonForTerm(String text)
    {
        JSONObject messageObject = new JSONObject();

        try
        {
            if( text.startsWith("#score") )
            {
                messageObject.put(SCORE_UPDATE, text.substring(7));
            }
            else
            {
                if( termIsPicture(text) )
                {
                    Bitmap image = GraphicUtils.loadBitmap(Uri.parse(text), 300, 300, getActivity());
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 30, os);

                    byte[] array = os.toByteArray();
                    String imageString = Base64.encodeToString(array, 0);
                    messageObject.put(FLASHCARD_TERM_IMAGE, imageString);
                }
                else
                {
                    messageObject.put(FLASHCARD_TERM, text);
                }
            }
        }
        catch( JSONException | FileNotFoundException e)
        {
            Log.e(getString(R.string.log_tag), "Error creating message for flashcard: " + e.getMessage());
        }

        return messageObject;
    }

    private boolean termIsPicture(String term)
    {
        return term.startsWith("content:") || term.startsWith("file:");
    }

    protected void resetToDefaultState(boolean close)
    {
        incorrectPositions = new ArrayList<>();
        skippedPositions = new ArrayList<>();
        questionsCompleted = 0;
        questionsCorrect = 0;
        if( close && flashcardDao != null )
        {
            flashcardDao.close();
        }
    }

    private void subscribe() {
        Log.i(getString(R.string.log_tag), "trying to subscribe");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!googleApiClient.isConnected()) {
            if (!googleApiClient.isConnecting()) {
                googleApiClient.connect();
            }
        } else {
            Nearby.Messages.subscribe(googleApiClient, messageListener, PUB_SUB_STRATEGY)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(getString(R.string.log_tag), "subscribed successfully");
                            } else {
                                Log.i(getString(R.string.log_tag), "could not subscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }


}

package com.bmb.kangaroo.nearby;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.R;


/**
 * Fragment that handles the display of the guest flashcard for the study nearby.
 */
public class StudyNearbyGuestFragment extends Fragment
{
    protected static final String ARG_TERM = "arg_term";
    protected static final String ARG_IMAGE = "arg_image";

    private String term = "";
    private boolean isImage = false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param term The term to display on the flashcard
     * @return A new instance of fragment StudyNearbyQuestFragment.
     */
    public static StudyNearbyGuestFragment newInstance(String term, boolean image)
    {
        StudyNearbyGuestFragment fragment = new StudyNearbyGuestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TERM, term);
        args.putBoolean(ARG_IMAGE, image);
        fragment.setArguments(args);
        return fragment;
    }

    public StudyNearbyGuestFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments().containsKey(ARG_TERM) )
        {
            term = getArguments().getString(ARG_TERM);
        }

        if( getArguments().containsKey(ARG_IMAGE) )
        {
            isImage = getArguments().getBoolean(ARG_IMAGE);
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final ScrollView guestLayout = (ScrollView) inflater.inflate(R.layout.study_nearby_guest_fragment, null);

        TextView termView = (TextView) guestLayout.findViewById(R.id.nearby_guest_term);
        ImageView termImageView = (ImageView) guestLayout.findViewById(R.id.nearby_guest_term_image);

        if( isImage )
        {
            termView.setVisibility(View.GONE);
            termImageView.setVisibility(View.VISIBLE);
            Bitmap flashcardImage = GraphicUtils.getBitmapFromString(term);
            termImageView.setImageBitmap(flashcardImage);
        }
        else
        {
            termView.setVisibility(View.VISIBLE);
            termImageView.setVisibility(View.GONE);
            termView.setText(term);
        }
        termView.setText(term);

        return guestLayout;
    }
}

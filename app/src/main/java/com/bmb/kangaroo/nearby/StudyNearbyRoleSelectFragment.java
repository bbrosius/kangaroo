package com.bmb.kangaroo.nearby;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bmb.kangaroo.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnRoleSelectedListener} interface
 * to handle interaction events.
 * Use the {@link StudyNearbyRoleSelectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StudyNearbyRoleSelectFragment extends Fragment
{
    private OnRoleSelectedListener roleSelectedListener;

    public final static String HOST = "HOST";
    private final static String GUEST = "GUEST";

    public static StudyNearbyRoleSelectFragment newInstance()
    {
        return new StudyNearbyRoleSelectFragment();
    }

    public StudyNearbyRoleSelectFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final LinearLayout selectRoleLayout = (LinearLayout) inflater.inflate(R.layout.study_nearby_main_screen, null);
        Button hostButton = (Button) selectRoleLayout.findViewById(R.id.host_button);
        hostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                roleSelectedListener.onRoleSelected(HOST);
            }
        });
        Button guestButton = (Button) selectRoleLayout.findViewById(R.id.guest_button);
        guestButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View arg0)
            {
                roleSelectedListener.onRoleSelected(GUEST);
            }

        });
        return selectRoleLayout;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            roleSelectedListener = (OnRoleSelectedListener) activity;
        } catch( ClassCastException e )
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        roleSelectedListener = null;
    }

    public interface OnRoleSelectedListener
    {
        void onRoleSelected(String role);
    }

}

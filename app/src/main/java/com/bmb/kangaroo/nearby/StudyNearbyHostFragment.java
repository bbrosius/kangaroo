package com.bmb.kangaroo.nearby;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.R;

public class StudyNearbyHostFragment extends Fragment
{
    protected static final String ARG_TERM = "arg_term";
    protected static final String ARG_DEFINITION = "arg_defintion";

    private String term = "";
    private String definition = "";

    private Button correctButton;
    private Button incorrectButton;
    private Button skipButton;
    private Button restartButton;
    private TextView termView;
    private TextView definitionView;
    private ImageView termImageView;

    public enum QUESTION_RESPONSE {
        SKIPPED,
        INCORRECT,
        CORRECT
    }

    private NearbyFlashcardAnsweredCallback listener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param term The term to display on the host card
     * @param definition The defintion to display on the host card
     * @return A new instance of fragment StudyNearbyHostFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StudyNearbyHostFragment newInstance(String term, String definition)
    {
        StudyNearbyHostFragment fragment = new StudyNearbyHostFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TERM, term);
        args.putString(ARG_DEFINITION, definition);
        fragment.setArguments(args);
        return fragment;
    }

    public StudyNearbyHostFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments() != null )
        {
            if( getArguments().containsKey(ARG_TERM) )
            {
                term = getArguments().getString(ARG_TERM);
            }

            if( getArguments().containsKey(ARG_DEFINITION) )
            {
                definition = getArguments().getString(ARG_DEFINITION);
            }
        }

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final ScrollView hostLayout = (ScrollView) inflater.inflate(R.layout.study_nearby_host_fragment, container, false);

        termView = (TextView) hostLayout.findViewById(R.id.flashcard_host_term);

        termImageView = (ImageView) hostLayout.findViewById(R.id.flashcard_host_term_image);

        if( termIsPicture(term) )
        {
            GraphicUtils.loadBitmap(Uri.parse(term), 200, 200, getActivity(), termImageView);
            termView.setVisibility(View.GONE);
            termImageView.setVisibility(View.VISIBLE);
        }
        else
        {
            termView.setVisibility(View.VISIBLE);
            termImageView.setVisibility(View.GONE);
            termView.setText(term);
        }

        definitionView = (TextView) hostLayout.findViewById(R.id.flashcard_host_defintion);
        definitionView.setText(definition);

        correctButton = (Button) hostLayout.findViewById(R.id.flashcard_host_correct);
        correctButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if( listener != null )
                {
                    listener.onFlashcardAnswered(QUESTION_RESPONSE.CORRECT);
                }
            }
        });

        incorrectButton = (Button) hostLayout.findViewById(R.id.flashcard_host_incorrect);
        incorrectButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View arg0)
            {
                if( listener != null )
                {
                    listener.onFlashcardAnswered(QUESTION_RESPONSE.INCORRECT);
                }
            }

        });

        skipButton = (Button) hostLayout.findViewById(R.id.flashcard_host_skip);
        skipButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if( listener != null )
                {
                    listener.onFlashcardAnswered(QUESTION_RESPONSE.SKIPPED);
                }
            }
        });

        restartButton = (Button) hostLayout.findViewById(R.id.flashcard_host_restart);
        restartButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                restartButton.setVisibility(View.GONE);
                skipButton.setVisibility(View.VISIBLE);
                correctButton.setVisibility(View.VISIBLE);
                incorrectButton.setVisibility(View.VISIBLE);
                if( listener != null )
                {
                    listener.restartFlashcards();
                }
            }
        });
        return hostLayout;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            listener = (NearbyFlashcardAnsweredCallback) activity;
        } catch( ClassCastException e )
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface NearbyFlashcardAnsweredCallback
    {
        /**
         * Callback for when an item has been selected.
         */
        void onFlashcardAnswered(QUESTION_RESPONSE response);

        void restartFlashcards();
    }

    public void setNewFlashcard(String term, String definition)
    {
        this.term = term;
        this.definition = definition;

        if( termIsPicture(term) )
        {
            GraphicUtils.loadBitmap(Uri.parse(term), 200, 200, getActivity(), termImageView);
            termView.setVisibility(View.GONE);
            termImageView.setVisibility(View.VISIBLE);
        }
        else
        {
            termView.setVisibility(View.VISIBLE);
            termImageView.setVisibility(View.GONE);
            termView.setText(term);
        }
        definitionView.setText(definition);
    }

    public void toggleButtons(boolean enable)
    {
        if( correctButton != null )
        {
            correctButton.setEnabled(enable);
        }

        if( incorrectButton != null )
        {
            incorrectButton.setEnabled(enable);
        }

        if( skipButton != null )
        {
            skipButton.setEnabled(enable);
        }
    }

    public void showRestartCard(String message)
    {
        correctButton.setVisibility(View.GONE);
        incorrectButton.setVisibility(View.GONE);
        skipButton.setVisibility(View.GONE);
        restartButton.setVisibility(View.VISIBLE);
        termView.setText(message);
        termImageView.setVisibility(View.GONE);
        definitionView.setText("");
    }

    private boolean termIsPicture(String term)
    {
        return term.startsWith("content:") || term.startsWith("file:");
    }

}

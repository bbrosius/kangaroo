package com.bmb.kangaroo.nearby;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bmb.kangaroo.BaseActivity;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;

public class StudyNearbyActivity extends BaseActivity implements StudyNearbyRoleSelectFragment.OnRoleSelectedListener, StudyNearbyHostFragment.NearbyFlashcardAnsweredCallback
{
    private DrawerLayout mainLayout;
    private Id studyId;

    static final int REQUEST_RESOLVE_ERROR = 1001;

    // The time-to-live when subscribing or publishing in this sample. Three minutes.
    static final int TTL_IN_SECONDS = 3 * 60;

    private StudyNearbyControlFragment controlFragment;
    private StudyNearbyHostFragment hostFragment;
    private StudyNearbyGuestFragment guestFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.default_activity_layout);

        Toolbar toolBar = (Toolbar) findViewById(R.id.default_toolbar);
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.drawable.ic_nearby);
        toolBar.addView(imageView);
        setSupportActionBar(toolBar);

        mainLayout  = (DrawerLayout) findViewById(R.id.default_layout);

        //Set the nav menu icon on the tool bar
        toolBar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mainLayout.openDrawer(GravityCompat.START);
            }
        });

        if( getIntent().hasExtra(getString(R.string.study_id)) )
        {
            studyId = IdService.resolveId(getIntent().getStringExtra(getString(R.string.study_id)));
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        controlFragment = (StudyNearbyControlFragment) fragmentManager.findFragmentByTag(getString(R.string.nearby_conrol_tag));
        if( controlFragment == null )
        {
            controlFragment = StudyNearbyControlFragment.newInstance(studyId);

            fragmentManager.beginTransaction()
                    .add(controlFragment, getString(R.string.nearby_conrol_tag))
                    .commit();
        }

        if( controlFragment.roleSelected() )
        {
            if( controlFragment.isHost() )
            {
                hostFragment = (StudyNearbyHostFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.nearby_host_tag));
                if( hostFragment == null )
                {
                    hostFragment = StudyNearbyHostFragment.newInstance(controlFragment.getCurrentTerm(), controlFragment.getCurrentDefinition());
                }

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_content, hostFragment, getString(R.string.nearby_host_tag))
                        .commit();
            }
            else
            {
                guestFragment = (StudyNearbyGuestFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.nearby_guest_tag));
                if( guestFragment == null )
                {
                    guestFragment = StudyNearbyGuestFragment.newInstance(controlFragment.getCurrentTerm(), controlFragment.isCurrentTermImage());
                }

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_content, guestFragment, getString(R.string.nearby_host_tag))
                        .commit();
            }
        }
    }

    public void showRoleSelectionFragment()
    {
        StudyNearbyRoleSelectFragment startFragment = (StudyNearbyRoleSelectFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.nearby_role_select_tag));
        if( startFragment == null )
        {
            startFragment = StudyNearbyRoleSelectFragment.newInstance();
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, startFragment, getString(R.string.nearby_role_select_tag))
                .commit();
    }

    @Override
    protected DrawerLayout getDrawerLayout()
    {
        return mainLayout;
    }

    @Override
    public void onRoleSelected(String role)
    {
        controlFragment.onRoleSelected(role.equalsIgnoreCase(StudyNearbyRoleSelectFragment.HOST));
    }

    @Override
    public void onFlashcardAnswered(StudyNearbyHostFragment.QUESTION_RESPONSE response)
    {
        boolean correct = false;
        boolean skipped = false;

        switch( response )
        {
            case CORRECT:
                correct = true;
                skipped = false;
                break;
            case INCORRECT:
                correct = false;
                skipped = false;
                break;
            case SKIPPED:
                correct = false;
                skipped = true;
                break;
        }

        String scoreLine = controlFragment.flashcardAnswered(correct, skipped);

        setTitle(scoreLine);
    }

    public void showRestartCard(String message)
    {
        hostFragment.showRestartCard(message);
    }

    @Override
    public void restartFlashcards()
    {
        setTitle("");
        controlFragment.restartFlashcards();

        hostFragment = (StudyNearbyHostFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.nearby_host_tag));
        if( hostFragment == null )
        {
            hostFragment = StudyNearbyHostFragment.newInstance(controlFragment.getCurrentTerm(), controlFragment.getCurrentDefinition());
        }
        else
        {
            hostFragment.setNewFlashcard(controlFragment.getCurrentTerm(), controlFragment.getCurrentDefinition());
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, hostFragment, getString(R.string.nearby_host_tag))
                .commit();

        hostFragment.toggleButtons(true);
    }

    public void updateFlashcard()
    {
        hostFragment = (StudyNearbyHostFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.nearby_host_tag));
        if( hostFragment == null )
        {
            hostFragment = StudyNearbyHostFragment.newInstance(controlFragment.getCurrentTerm(), controlFragment.getCurrentDefinition());
        }
        else
        {
            hostFragment.setNewFlashcard(controlFragment.getCurrentTerm(), controlFragment.getCurrentDefinition());
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, hostFragment, getString(R.string.nearby_host_tag))
                .commit();
    }

    public void newFlashcardReceived(String term, boolean image)
    {
        guestFragment = (StudyNearbyGuestFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.nearby_guest_tag));
        if( guestFragment == null )
        {
            guestFragment = StudyNearbyGuestFragment.newInstance(term, image);
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, guestFragment, getString(R.string.nearby_host_tag))
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controlFragment.finishedResolvingNearbyPermissionError();
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            // User was presented with the Nearby opt-in dialog and pressed "Allow".
            if (resultCode == Activity.RESULT_OK) {
                // We track the pending subscription and publication tasks in MainFragment. Once
                // a user gives consent to use Nearby, we execute those tasks.
                controlFragment.executePendingTasks();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // User was presented with the Nearby opt-in dialog and pressed "Deny". We cannot
                // proceed with any pending subscription and publication tasks. Reset state.
                Snackbar permissionBar = Snackbar.make(findViewById(R.id.fragment_content), getString(R.string.nearby_denied), Snackbar.LENGTH_LONG);
                permissionBar.show();

                controlFragment.resetToDefaultState(true);
                showRoleSelectionFragment();
                controlFragment.setRoleSelected(false);
            } else {
                Log.e(getString(R.string.log_tag), "Failed to resolve error with code " + resultCode);
                Snackbar errorBar = Snackbar.make(findViewById(R.id.fragment_content), getString(R.string.nearby_error), Snackbar.LENGTH_LONG);
                errorBar.show();
            }
        }
    }
}

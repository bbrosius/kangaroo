package com.bmb.kangaroo.bluetooth;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.views.NoteCardLayout;

import java.util.Timer;
import java.util.TimerTask;


public class BluetoothGuestStudyFragment extends Fragment
{
    private String term = "";
    private TextView termView;
    private Timer timer;
    private int countdown;

    public BluetoothGuestStudyFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments().containsKey(getString(R.string.flashcard_term_argument)) )
        {
            term = getArguments().getString(getString(R.string.flashcard_term_argument));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final NoteCardLayout guestLayout = (NoteCardLayout) inflater.inflate(R.layout.bluetooth_guest_layout, null);

        termView = (TextView) guestLayout.findViewById(R.id.bluetooth_guest_term);
        termView.setText(term);

        return guestLayout;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    public void setDiscoverable()
    {
        timer = new Timer();
        int DISCOVERABLE_TIME = 300;
        countdown = DISCOVERABLE_TIME;
        if( termView != null )
        {
            termView.setText(getActivity().getString(R.string.awaiting_host) + countdown + "s");
            timer.scheduleAtFixedRate(new TimerTask()
            {
                public void run()
                {
                    if( countdown == 1 )
                    {
                        timer.cancel();
                    }
                    --countdown;
                    termView.setText(getString(R.string.awaiting_host) + countdown + "s");
                }
            }, 1000, 1000);
        }
    }
}

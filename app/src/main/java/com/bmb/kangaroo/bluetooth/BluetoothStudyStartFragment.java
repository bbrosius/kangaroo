package com.bmb.kangaroo.bluetooth;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.bmb.kangaroo.R;

public class BluetoothStudyStartFragment extends Fragment
{
    public final static String HOST = "HOST";
    private final static String GUEST = "GUEST";

    private BluetoothRoleSelectedCallback callbacks = bluetoothRoleSelectedCallback;

    public BluetoothStudyStartFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final LinearLayout selectRoleLayout = (LinearLayout) inflater.inflate(R.layout.bluetooth_start_layout, null);
        Button hostButton = (Button) selectRoleLayout.findViewById(R.id.host_button);
        hostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                callbacks.onRoleSelected(HOST);
            }
        });
        Button guestButton = (Button) selectRoleLayout.findViewById(R.id.guest_button);
        guestButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                callbacks.onRoleSelected(GUEST);
            }

        });
        return selectRoleLayout;
    }


    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface BluetoothRoleSelectedCallback
    {
        /**
         * Callback for when an item has been selected.
         */
        void onRoleSelected(String role);
    }

    /**
     * A dummy implementation of the {@link com.bmb.kangaroo.bluetooth.BluetoothStudyStartFragment.BluetoothRoleSelectedCallback} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private final static BluetoothRoleSelectedCallback bluetoothRoleSelectedCallback = new BluetoothRoleSelectedCallback()
    {
        @Override
        public void onRoleSelected(String string)
        {
        }
    };

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof BluetoothStudyStartFragment.BluetoothRoleSelectedCallback))
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callbacks = (BluetoothRoleSelectedCallback) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        callbacks = bluetoothRoleSelectedCallback;
    }
}

package com.bmb.kangaroo.bluetooth;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.bmb.kangaroo.BaseActivity;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;

import java.util.ArrayList;

@SuppressLint("HandlerLeak")
public class BluetoothStudyActivity extends BaseActivity implements BluetoothStudyStartFragment.BluetoothRoleSelectedCallback, BluetoothHostStudyFragment.BluetoothFlashcardAnsweredCallback
{
    private BluetoothAdapter adapter;
    private StudyService studyService;

    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 3;

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_TOAST = 3;

    // Key names received from the BluetoothChatService Handler
    public static final String TOAST = "toast";

    private ArrayList<Integer> incorrectPositions;
    private ArrayList<Integer> skippedPositions;

    private boolean host = true;
    private BluetoothHostStudyFragment hostStudyFragment;
    private BluetoothGuestStudyFragment guestStudyFragment;

    private Id studyId;
    private Cursor flashcards;

    private FlashcardDAO flashcardDao;
    private int questionsCompleted = 0;
    private int questionsCorrect = 0;

    private boolean connected = false;

    private DrawerLayout mainLayout;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.default_activity_layout);

        Toolbar toolBar = (Toolbar) findViewById(R.id.default_toolbar);
        setSupportActionBar(toolBar);

        mainLayout  = (DrawerLayout) findViewById(R.id.default_layout);

        //Set the nav menu icon on the tool bar
        toolBar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mainLayout.openDrawer(GravityCompat.START);
            }
        });

        adapter = BluetoothAdapter.getDefaultAdapter();
        incorrectPositions = new ArrayList<>();
        skippedPositions = new ArrayList<>();
        checkBlueToothEnabled();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, new BluetoothStudyStartFragment())
                .commit();

        flashcardDao = new FlashcardDAO();
        flashcardDao.open();

        if( getIntent().hasExtra(getString(R.string.study_subject_id)) )
        {
            studyId = IdService.resolveId(getIntent().getStringExtra(getString(R.string.study_subject_id)));
            Subject subject = SubjectService.getInstance().getSubject(studyId);
            Exam exam = subject.getNextExam();
            if( exam != null )
            {
                studyId = exam.getId();
            }
        }

        if( studyId != null )
        {
            flashcards = flashcardDao.findFlashcards(studyId, true);
        }
    }

    @Override
    public DrawerLayout getDrawerLayout()
    {
        return mainLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        // If BT is not on, request that it be enabled.
        checkBlueToothEnabled();
    }

    private boolean checkBlueToothEnabled()
    {
        boolean enabled = true;
        if( !adapter.isEnabled() )
        {
            enabled = false;
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        else
        {
            setupStudyService();
        }

        return enabled;
    }

    private void startupPairing()
    {
        if(host)
        {
            Intent serverIntent = new Intent(this, DeviceDialog.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
        }
        else
        {
            ensureDiscoverable();
        }
    }

    private void ensureDiscoverable()
    {
        if( adapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE )
        {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);

            guestStudyFragment.setDiscoverable();
        }
    }

    private void setupStudyService()
    {
        studyService = new StudyService(this, handler);
    }

    private void connectDevice(Intent data) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceDialog.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = adapter.getRemoteDevice(address);
        // Attempt to connect to the device
        studyService.connect(device);
    }

    private void sendText(String text)
    {
        byte[] send = text.getBytes();
        studyService.write(send);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupStudyService();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    finish();
                }
        }
    }

    private void setStatus(int resId) {
        if( getSupportActionBar() != null )
        {
            getSupportActionBar().setSubtitle(resId);
        }
    }

    private void setTitle(String title) {
        if( getSupportActionBar() != null )
        {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth chat services
        if( studyService != null )
        {
            studyService.stop();
        }

        flashcardDao.close();
    }

    // The Handler that gets information back from the BluetoothChatService
    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case StudyService.STATE_CONNECTED:
                            connected = true;
                            if( flashcards == null || flashcards.getCount() == 0 )
                            {
                                Bundle arguments = new Bundle();
                                arguments.putString(getString(R.string.flashcard_term_argument), getString(R.string.need_flashcards_to_study));
                                hostStudyFragment = new BluetoothHostStudyFragment();
                                hostStudyFragment.setArguments(arguments);

                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_content, hostStudyFragment)
                                        .commit();

                                hostStudyFragment.toggleButtons(false);

                                sendText(getString(R.string.host_contains_no_flashcards));
                            }
                            else
                            {
                                flashcards.moveToFirst();
                                Bundle arguments = new Bundle();
                                arguments.putString(getString(R.string.flashcard_term_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
                                arguments.putString(getString(R.string.flashcard_definition_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION)));
                                hostStudyFragment = new BluetoothHostStudyFragment();
                                hostStudyFragment.setArguments(arguments);

                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_content, hostStudyFragment)
                                        .commit();

                                hostStudyFragment.toggleButtons(true);

                                sendText(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
                            }

                            setStatus(R.string.connected);
                            break;
                        case StudyService.STATE_CONNECTING:
                            connected = false;
                            if( host  && hostStudyFragment != null)
                            {
                                hostStudyFragment.toggleButtons(false);
                            }
                            setStatus(R.string.connecting);
                            break;
                        case StudyService.STATE_LISTEN:
                        case StudyService.STATE_NONE:
                            setStatus(R.string.not_connected);
                            connected = false;
                            break;
                    }
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    String scorePrefix = "#score:";
                    if( readMessage.startsWith(scorePrefix) )
                    {
                        setTitle(readMessage.substring(scorePrefix.length()));
                    }
                    else
                    {
                        guestStudyFragment = new BluetoothGuestStudyFragment();
                        Bundle arguments = new Bundle();
                        arguments.putString(getString(R.string.flashcard_term_argument), readMessage);
                        guestStudyFragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_content, guestStudyFragment)
                                .setCustomAnimations(R.anim.grow_from_middle, R.anim.card_flip_left_out)
                                .commit();
                    }
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onRoleSelected(String role)
    {
        if( role.equalsIgnoreCase(BluetoothStudyStartFragment.HOST))
        {
            host = true;
        }
        else
        {
            host = false;
            guestStudyFragment = new BluetoothGuestStudyFragment();
            Bundle arguments = new Bundle();
            arguments.putString(getString(R.string.flashcard_term_argument), getString(R.string.awaiting_host));
            guestStudyFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_content, guestStudyFragment)
                    .commit();

            Runnable timeoutRunnable = new Runnable()
            {
                @Override
                public void run()
                {
                    //If we haven't connected restart at the role selection screen
                    if( !connected )
                    {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_content, new BluetoothStudyStartFragment())
                                .commit();
                    }
                }
            };

            Handler timeoutHandler = new Handler();
            timeoutHandler.postDelayed(timeoutRunnable, 300000);
        }

        startupPairing();
    }

    @Override
    public void onFlashcardAnswered(BluetoothHostStudyFragment.QUESTION_RESPONSE response)
    {
        String scoreLine = "";
        switch( response )
        {
            case CORRECT:
                //In the event we're repeating incorrect cards
                if( questionsCompleted < flashcards.getCount() )
                {
                    questionsCompleted++;
                }
                questionsCorrect++;
                scoreLine = String.format(getString(R.string.quiz_score), questionsCorrect, questionsCompleted);
                break;
            case INCORRECT:
                //In the event we're repeating incorrect cards
                if( questionsCompleted < flashcards.getCount() )
                {
                    questionsCompleted++;
                }
                incorrectPositions.add(flashcards.getPosition());
                scoreLine = String.format(getString(R.string.quiz_score), questionsCorrect, questionsCompleted);
                break;
            case SKIPPED:
                skippedPositions.add(flashcards.getPosition());
                break;
        }

        setTitle(scoreLine);
        sendText("#score:" + scoreLine);

        if( flashcards.isLast() )
        {
            if( !skippedPositions.isEmpty() )
            {
                flashcards.moveToPosition(skippedPositions.get(0));
                updatedFlashcard();
                skippedPositions.remove(0);
            }
            else if( !incorrectPositions.isEmpty() )
            {
                flashcards.moveToPosition(incorrectPositions.get(0));
                updatedFlashcard();
                incorrectPositions.remove(0);
            }
            else
            {
                String restartMessage = getString(R.string.restart_flashcards) + scoreLine;
                hostStudyFragment.showRestartCard(restartMessage);
            }
        }
        else
        {
            flashcards.moveToNext();
            updatedFlashcard();
        }
    }

    @Override
    public void restartFlashcards()
    {
        flashcards.moveToFirst();
        Bundle arguments = new Bundle();
        arguments.putString(getString(R.string.flashcard_term_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
        arguments.putString(getString(R.string.flashcard_definition_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION)));
        hostStudyFragment = new BluetoothHostStudyFragment();
        hostStudyFragment.setArguments(arguments);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, hostStudyFragment)
                .commit();

        hostStudyFragment.toggleButtons(true);

        sendText(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
    }

    private void updatedFlashcard()
    {
        Bundle arguments = new Bundle();
        arguments.putString(getString(R.string.flashcard_term_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
        arguments.putString(getString(R.string.flashcard_definition_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION)));
        hostStudyFragment = new BluetoothHostStudyFragment();
        hostStudyFragment.setArguments(arguments);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_content, hostStudyFragment)
                .setCustomAnimations(R.anim.grow_from_middle, R.anim.card_flip_left_out)
                .commit();

        sendText(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
    }
}
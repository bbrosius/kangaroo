package com.bmb.kangaroo.bluetooth;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bmb.kangaroo.R;

public class BluetoothHostStudyFragment extends Fragment
{
    public enum QUESTION_RESPONSE {
        SKIPPED,
        INCORRECT,
        CORRECT
    }

    private String term = "";
    private String definition = "";

    private Button correctButton;
    private Button incorrectButton;
    private Button skipButton;
    private Button restartButton;
    private TextView termView;
    private TextView definitionView;

    private BluetoothFlashcardAnsweredCallback callback = bluetoothFlashcardAnsweredCallback;

    public BluetoothHostStudyFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments().containsKey(getString(R.string.flashcard_term_argument)) )
        {
            term = getArguments().getString(getString(R.string.flashcard_term_argument));
        }

        if( getArguments().containsKey(getString(R.string.flashcard_definition_argument)) )
        {
            definition = getArguments().getString(getString(R.string.flashcard_definition_argument));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final LinearLayout hostLayout = (LinearLayout) inflater.inflate(R.layout.bluetooth_host_layout, container);

        termView = (TextView) hostLayout.findViewById(R.id.flashcard_host_term);
        termView.setText(term);

        definitionView = (TextView) hostLayout.findViewById(R.id.flashcard_host_defintion);
        definitionView.setText(definition);

        correctButton = (Button) hostLayout.findViewById(R.id.flashcard_host_correct);
        correctButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                callback.onFlashcardAnswered(QUESTION_RESPONSE.CORRECT);
            }
        });

        incorrectButton = (Button) hostLayout.findViewById(R.id.flashcard_host_incorrect);
        incorrectButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View arg0)
            {
                callback.onFlashcardAnswered(QUESTION_RESPONSE.INCORRECT);
            }

        });

        skipButton = (Button) hostLayout.findViewById(R.id.flashcard_host_skip);
        skipButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                callback.onFlashcardAnswered(QUESTION_RESPONSE.SKIPPED);
            }
        });

        restartButton = (Button) hostLayout.findViewById(R.id.flashcard_host_restart);
        restartButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                restartButton.setVisibility(View.GONE);
                skipButton.setVisibility(View.VISIBLE);
                correctButton.setVisibility(View.VISIBLE);
                incorrectButton.setVisibility(View.VISIBLE);
                callback.restartFlashcards();
            }
        });
        return hostLayout;
    }
    /**
     * Callback that handles answer responses entered by the host
     */
    public interface BluetoothFlashcardAnsweredCallback
    {
        /**
         * Callback for when an item has been selected.
         */
        void onFlashcardAnswered(QUESTION_RESPONSE response);

        void restartFlashcards();
    }

    /**
     * A dummy implementation of the {@link com.bmb.kangaroo.bluetooth.BluetoothHostStudyFragment.BluetoothFlashcardAnsweredCallback} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private final static BluetoothFlashcardAnsweredCallback bluetoothFlashcardAnsweredCallback = new BluetoothFlashcardAnsweredCallback()
    {

        @Override
        public void onFlashcardAnswered(QUESTION_RESPONSE response)
        {

        }

        @Override
        public void restartFlashcards()
        {

        }
    };

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof BluetoothStudyStartFragment.BluetoothRoleSelectedCallback))
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callback = (BluetoothFlashcardAnsweredCallback) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        callback = bluetoothFlashcardAnsweredCallback;
    }

    public void toggleButtons(boolean enable)
    {
        if( correctButton != null )
        {
            correctButton.setEnabled(enable);
        }

        if( incorrectButton != null )
        {
            incorrectButton.setEnabled(enable);
        }

        if( skipButton != null )
        {
            skipButton.setEnabled(enable);
        }
    }

    public void showRestartCard(String message)
    {
        correctButton.setVisibility(View.GONE);
        incorrectButton.setVisibility(View.GONE);
        skipButton.setVisibility(View.GONE);
        restartButton.setVisibility(View.VISIBLE);
        termView.setText(message);
        definitionView.setText("");
    }
}

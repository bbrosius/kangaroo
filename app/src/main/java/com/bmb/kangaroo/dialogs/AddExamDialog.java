package com.bmb.kangaroo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.bmb.kangaroo.SubjectPreferenceActivity;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.ExamService;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.utils.Id;
import com.bmb.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class AddExamDialog extends DialogFragment
{
	private EditText examNameEntry;
	private TextView examDate;
	private TextView examReminderDate;
	private CheckBox cumulativeExam;
	private DateFormat dateFormat;
	private CheckBox enableReminder;
	private CheckBox addToCalendar;

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) 
	{
		dateFormat = new SimpleDateFormat(Utils.getContext().getString(R.string.date_format), Locale.getDefault());
		
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final LinearLayout addExamLayout = (LinearLayout) inflater.inflate(R.layout.add_exam_dialog, null);
		examNameEntry = (EditText) addExamLayout.findViewById(R.id.add_exam_name);
		
		examDate = (TextView) addExamLayout.findViewById(R.id.add_exam_date_value);
		examDate.setText(dateFormat.format(new GregorianCalendar().getTime()));
		examDate.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				LinearLayout dialogLayout = (LinearLayout) inflater.inflate(R.layout.date_time_dialog, null);
				final DatePicker datePicker = (DatePicker) dialogLayout.findViewById(R.id.date_picker);
		        final TimePicker timePicker = (TimePicker) dialogLayout.findViewById(R.id.time_picker);
		        
		        Calendar currentDateTime = new GregorianCalendar();
		        try {
					currentDateTime.setTime(dateFormat.parse(examDate.getText().toString()));
				} 
		        catch (ParseException e) 
				{
					Log.e(getString(R.string.log_tag), "Error parsing exam date.");
				}
		        datePicker.updateDate(currentDateTime.get(Calendar.YEAR), currentDateTime.get(Calendar.MONTH), currentDateTime.get(Calendar.DAY_OF_MONTH));
	        	timePicker.setCurrentHour(currentDateTime.get(Calendar.HOUR));
	        	timePicker.setCurrentMinute(currentDateTime.get(Calendar.MINUTE));
	        	
				builder.setView(dialogLayout);
				builder.setTitle(R.string.set_exam_date);
				
				builder.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int arg1) 
					{
						Calendar cal = new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());
						examDate.setText(dateFormat.format(cal.getTime()));
						dialog.dismiss();						
					}					
				});
				
				builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						dialog.dismiss();						
					}
				});
				
				Dialog dialog = builder.create();
				dialog.show();
			}			
		});
		
		examReminderDate = (TextView) addExamLayout.findViewById(R.id.add_reminder_date_value);
		examReminderDate.setText(dateFormat.format(new GregorianCalendar().getTime()));

		SharedPreferences pref = Utils.getContext().getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
        boolean notificationsEnabled = pref.getBoolean(getString(R.string.notification_enabled_key), true);
        
		enableReminder = (CheckBox) addExamLayout.findViewById(R.id.enable_reminder_notification);
		enableReminder.setChecked(true);
		enableReminder.setEnabled(notificationsEnabled);
		
		enableReminder.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton button, boolean checked) {
				examReminderDate.setEnabled(checked);
			}
			
		});
	
		examReminderDate.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				LinearLayout dialogLayout = (LinearLayout) inflater.inflate(R.layout.date_time_dialog, null);
				final DatePicker datePicker = (DatePicker) dialogLayout.findViewById(R.id.date_picker);
		        final TimePicker timePicker = (TimePicker) dialogLayout.findViewById(R.id.time_picker);
		        
		        Calendar currentDateTime = new GregorianCalendar();
		        try {
					currentDateTime.setTime(dateFormat.parse(examDate.getText().toString()));
				} 
		        catch (ParseException e) 
				{
					Log.e(getString(R.string.log_tag), "Error parsing exam date.");
				}
		        datePicker.updateDate(currentDateTime.get(Calendar.YEAR), currentDateTime.get(Calendar.MONTH), currentDateTime.get(Calendar.DAY_OF_MONTH));
	        	timePicker.setCurrentHour(currentDateTime.get(Calendar.HOUR));
	        	timePicker.setCurrentMinute(currentDateTime.get(Calendar.MINUTE));
	        	
				builder.setView(dialogLayout);
				builder.setTitle(R.string.set_reminder_date);
				builder.setPositiveButton(R.string.set, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int arg1) 
					{
						Calendar cal = new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute());
						examReminderDate.setText(dateFormat.format(cal.getTime()));
						dialog.dismiss();						
					}					
				});
				
				builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() 
				{
					@Override
					public void onClick(DialogInterface dialog, int which) 
					{
						dialog.dismiss();						
					}
				});
				
				Dialog dialog = builder.create();
				dialog.show();
			}			
		});
		
		cumulativeExam = (CheckBox) addExamLayout.findViewById(R.id.cumulative_exam);
		addToCalendar = (CheckBox) addExamLayout.findViewById(R.id.add_to_calendar);
		
		builder.setView(addExamLayout);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
        	public void onClick(DialogInterface dialog, int id)
        	{
        		Exam exam = new Exam(examNameEntry.getText().toString());
        		Calendar examDateCalendar = new GregorianCalendar();
				try 
				{
					examDateCalendar.setTime(dateFormat.parse(examDate.getText().toString()));
				} 
				catch (ParseException e) 
				{
					Log.e(getString(R.string.log_tag), "Error parsing exam date: " + e.getMessage());
				}
        		exam.setExamDate( examDateCalendar );

				Calendar reminderDate = new GregorianCalendar();
				try 
				{
					reminderDate.setTime(dateFormat.parse(examReminderDate.getText().toString()));
				}
				catch (ParseException e) 
				{
					Log.e(getString(R.string.log_tag), "Error parsing reminder date: " + e.getMessage());
				}
        		
        		exam.setReminderDate( reminderDate );
        		
        		exam.setIsCumulative(cumulativeExam.isChecked());
        		
        		exam.setSubjectId(SubjectService.getInstance().getCurrentSubjectId());
        		Subject examSubject = SubjectService.getInstance().getSubject(SubjectService.getInstance().getCurrentSubjectId());
        		Id examId = ExamService.getInstance().addExam(exam, enableReminder.isChecked(), addToCalendar.isChecked());
        		
        		if(examId != null)
        		{
        			examSubject.addExamId(examId);
        		}

                if( AddExamDialog.this.getActivity() instanceof ExamCreatedListener )
                {
                    ((ExamCreatedListener) AddExamDialog.this.getActivity()).onExamCreated();
                }
        		AddExamDialog.this.getDialog().dismiss();
        	}
        });
        
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
        	public void onClick(DialogInterface dialog, int id)
        	{
        		AddExamDialog.this.getDialog().cancel();
        	}
        });
        
        return builder.create();
    }

    public interface ExamCreatedListener {

        void onExamCreated();
    }
}
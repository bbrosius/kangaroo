package com.bmb.kangaroo.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.FlashcardActivity;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AddFlashcardDialog extends DialogFragment implements ActivityCompat.OnRequestPermissionsResultCallback
{
    private static final int REQUEST_STORAGE = 0;

    private EditText subjectEntry;
    private EditText definitionEntry;
    private ImageView subjectImage;

    private Uri imageUri;
    private boolean isImage = false;
    private Long currentId = null;

    private Id noteId;
    private Id subjectId;
    private Id examId;
    private Id flashcardSetId;

    private LinearLayout addFlashcardLayout;
    private Button textFlashcard;

    private static final int REQUEST_IMAGE_CAPTURE = 1001;
    private static final int REQUEST_IMAGE_SELECT = 2002;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        addFlashcardLayout = (LinearLayout) inflater.inflate(R.layout.enter_flashcard, null);

        String term = "";
        String definition = "";
        if (getArguments().containsKey(getString(R.string.flashcard_term_argument)))
        {
            term = getArguments().getString(getString(R.string.flashcard_term_argument));
        }

        if (getArguments().containsKey(getString(R.string.flashcard_definition_argument)))
        {
            definition = getArguments().getString(getString(R.string.flashcard_definition_argument));
        }

        currentId = getArguments().getLong(getString(R.string.flashcard_id_argument), -999);

        noteId = IdService.resolveId(getArguments().getString(getString(R.string.study_note_id)));
        subjectId = IdService.resolveId(getArguments().getString(getString(R.string.study_subject_id)));
        examId = IdService.resolveId(getArguments().getString(getString(R.string.study_exam_id)));
        flashcardSetId = IdService.resolveId(getArguments().getString(getString(R.string.flashcard_set_id)));

        subjectEntry = (EditText) addFlashcardLayout.findViewById(R.id.flashcard_term);
        if ( term != null && term.isEmpty())
        {
            subjectEntry.setHint(R.string.term);
        }
        else
        {
            subjectEntry.setText(term);
        }

        definitionEntry = (EditText) addFlashcardLayout.findViewById(R.id.flashcard_definition);
        if ( definition != null && definition.isEmpty())
        {
            definitionEntry.setHint(R.string.definition);
        }
        else
        {
            definitionEntry.setText(definition);
        }

        subjectImage = (ImageView) addFlashcardLayout.findViewById(R.id.flashcard_term_image);

        textFlashcard = (Button) addFlashcardLayout.findViewById(R.id.select_text_flashcard);
        textFlashcard.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                textFlashcard.setVisibility(View.GONE);
                subjectEntry.setVisibility(View.VISIBLE);
                subjectImage.setVisibility(View.GONE);
                isImage = false;
            }
        });

        Button selectPicture = (Button) addFlashcardLayout.findViewById(R.id.select_picture_button);
        selectPicture.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                textFlashcard.setVisibility(View.VISIBLE);
                subjectEntry.setVisibility(View.GONE);
                subjectImage.setVisibility(View.VISIBLE);
                isImage = true;

                Intent photoPickerIntent = new Intent();
                if( Build.VERSION.SDK_INT < 19 )
                {
                    photoPickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, REQUEST_IMAGE_SELECT);
                }
                else
                {
                    photoPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    photoPickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, REQUEST_IMAGE_SELECT);
                }

                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_SELECT);
            }
        });

        Button takePicture = (Button) addFlashcardLayout.findViewById(R.id.take_picture_button);
        takePicture.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                textFlashcard.setVisibility(View.VISIBLE);
                subjectEntry.setVisibility(View.GONE);
                subjectImage.setVisibility(View.VISIBLE);
                isImage = true;
                if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED)
                {
                    dispatchTakePictureIntent();
                }
                else
                {
                    requestStoragePermission();
                }
            }
        });

        if (!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            takePicture.setVisibility(View.GONE);
        }

        builder.setView(addFlashcardLayout);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                //This is handled in on start so we can keep the dialog open if it's not completely filled out.
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                AddFlashcardDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

    /**
     * Requests the Storage permissions for saving and loading the subjects and flashcard sets permission.
     */
    private void requestStoragePermission()
    {
        Log.i(getString(R.string.log_tag), "Requesting storage permission");

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.i(getString(R.string.log_tag),
                    "Displaying storage permission rationale to provide additional context.");
            Snackbar.make(addFlashcardLayout, R.string.image_storage_phrase,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(getActivity(),
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_STORAGE);
                        }
                    })
                    .show();
        } else {

            //Storage permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        }
    }

    //Handles request to get the storage permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_STORAGE)
        {
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                dispatchTakePictureIntent();
            }
            else
            {
                Log.e(getString(R.string.log_tag), "Storage permission was NOT granted.");
                Snackbar.make(addFlashcardLayout, R.string.image_storage_rejected,
                        Snackbar.LENGTH_SHORT).show();

                textFlashcard.setVisibility(View.VISIBLE);
                subjectEntry.setVisibility(View.GONE);
                subjectImage.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    @Override
    public void onStart()
    {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        AlertDialog d = (AlertDialog) getDialog();
        if (d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if ((!subjectEntry.getText().toString().isEmpty() || (imageUri != null && !imageUri.toString().isEmpty())) && !definitionEntry.getText().toString().isEmpty())
                    {
                        FlashcardDAO flashcardDao = new FlashcardDAO();
                        flashcardDao.open();
                        int isImageFlashcard = isImage ? 1 : 0;

                        if (currentId != null && currentId != -999)
                        {
                            if (isImage)
                            {
                                flashcardDao.updateFlashcardText(imageUri.toString(), definitionEntry.getText().toString(), isImageFlashcard, currentId);
                            }
                            else
                            {
                                flashcardDao.updateFlashcardText(subjectEntry.getText().toString(), definitionEntry.getText().toString(), isImageFlashcard, currentId);
                            }
                            FlashcardActivity parent = (FlashcardActivity) getActivity();
                            parent.flashcardChanged();
                        }
                        else
                        {
                            FlashcardSet set = FlashcardSetService.getInstance().getFlashcardSet(flashcardSetId);
                            long newId;
                            if (isImage)
                            {
                                newId = flashcardDao.addFlashcard(subjectId, examId, flashcardSetId, noteId, imageUri.toString(), definitionEntry.getText().toString(), isImageFlashcard);
                            }
                            else
                            {
                                newId = flashcardDao.addFlashcard(subjectId, examId, flashcardSetId, noteId, subjectEntry.getText().toString(), definitionEntry.getText().toString(), isImageFlashcard);
                            }
                            set.addFlashcardId(newId);

                            FlashcardActivity parent = (FlashcardActivity) getActivity();
                            parent.flashcardChanged();
                        }
                        flashcardDao.close();
                        AddFlashcardDialog.this.getDialog().dismiss();
                    }
                    else
                    {
                        Toast missingFieldToast = Toast.makeText(getActivity(), getString(R.string.term_and_definition_must_be_entered), Toast.LENGTH_SHORT);
                        missingFieldToast.show();
                    }
                }
            });
        }
    }

    private void dispatchTakePictureIntent()
    {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Ensure that there's a storage activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null)
        {
            // Create the File where the photo should go
            File photoFile = null;
            try
            {
                photoFile = createImageFile();
            } catch (IOException ex)
            {
                Log.e(getString(R.string.log_tag), "Error creating file for picture: " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null)
            {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException
    {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "FLASHCARD_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        imageUri = Uri.fromFile(image);
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            GraphicUtils.loadBitmap(imageUri, 200, 200, getActivity(), subjectImage);

            addPictureToGallery();
        }
        else if (requestCode == REQUEST_IMAGE_SELECT && resultCode == RESULT_OK)
        {
            imageUri = data.getData();

            if( Build.VERSION.SDK_INT >= 19 )
            {
                final int takeFlags = data.getFlags() & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                // Check for the freshest data.
                getActivity().getContentResolver().takePersistableUriPermission(imageUri, takeFlags);
            }
            if( imageUri != null )
            {
                GraphicUtils.loadBitmap(imageUri, 200, 200, getActivity(), subjectImage);
            }
            else
            {
                Log.e(getString(R.string.log_tag), "Error loading a picture returned uri was null");
                Toast imageSelectErrorToast = Toast.makeText(getActivity(), getString(R.string.error_loading_picture), Toast.LENGTH_SHORT);
                imageSelectErrorToast.show();
            }
        }
    }

    private void addPictureToGallery()
    {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(imageUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }
}

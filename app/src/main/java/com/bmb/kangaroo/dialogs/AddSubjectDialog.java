package com.bmb.kangaroo.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.utils.Id;

/**
 * Dialog for adding a new subject.
 */
public class AddSubjectDialog extends DialogFragment
{
    private EditText subjectNameEntry;

    private AddSubjectListener callbacks = addSubjectCallback;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final LinearLayout addSubjectLayout = (LinearLayout) inflater.inflate(R.layout.add_subject_dialog, null);
        subjectNameEntry = (EditText) addSubjectLayout.findViewById(R.id.add_subject_name);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(addSubjectLayout);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                Id newSubjectId = SubjectService.getInstance().addSubject(new Subject(subjectNameEntry.getText().toString()));
                callbacks.onSubjectAdded(SubjectService.getInstance().getSubject(newSubjectId));

                AddSubjectDialog.this.getDialog().dismiss();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                AddSubjectDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof AddSubjectListener))
        {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        callbacks = (AddSubjectListener) activity;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        callbacks = addSubjectCallback;
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface AddSubjectListener
    {
        /**
         * Callback for when an item has been selected.
         */
        void onSubjectAdded(Subject subject);
    }

    /**
     * A dummy implementation of the interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private final static AddSubjectListener addSubjectCallback = new AddSubjectListener()
    {
        @Override
        public void onSubjectAdded(Subject subject)
        {
        }
    };

}

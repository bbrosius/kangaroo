package com.bmb.kangaroo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.ExamService;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.utils.Id;

import java.util.ArrayList;

public class SubjectGoalDialog extends DialogFragment
{
    private Subject currentSubject;
    private String calculatedExamGoal;
    private String subjectGoal;
    private ArrayList<TextView> examGoals;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout subjectGoalLayout = (LinearLayout) inflater.inflate(R.layout.subject_goal_dialog, null);

        currentSubject = SubjectService.getInstance().getSubject(SubjectService.getInstance().getCurrentSubjectId());

        final TextView subjectName = (TextView)subjectGoalLayout.findViewById(R.id.goal_subject_name);
        subjectName.setText(getString(R.string.goal_for_subject) + " " + currentSubject.getTitle());

        final EditText subjectGradeGoal = (EditText)subjectGoalLayout.findViewById(R.id.goal_entry);

        examGoals = new ArrayList<>();

        if( currentSubject.getGradeGoal() != -1 )
        {
            subjectGoal = String.valueOf(currentSubject.getGradeGoal());
            subjectGradeGoal.setText(subjectGoal);
            updateExamGoals();
        }

        subjectGradeGoal.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if( s.length() > 0 )
                {
                    subjectGoal = s.toString();
                    updateExamGoals();
                }
            }
        });

        if( !currentSubject.getExamIds().isEmpty() )
        {
            TextView noExamMessage = (TextView)subjectGoalLayout.findViewById(R.id.no_exams_message);
            noExamMessage.setVisibility(View.INVISIBLE);
            subjectGoalLayout.removeView(noExamMessage);

            for( Id examId : currentSubject.getExamIds() )
            {
                final Exam currentExam = ExamService.getInstance().getExam(examId);
                LinearLayout examRow = (LinearLayout) inflater.inflate(R.layout.exam_grade_goal_row, null);
                TextView examName = (TextView)examRow.findViewById(R.id.exam_name);
                examName.setText(currentExam.getTitle());

                TextView examGoal = (TextView)examRow.findViewById(R.id.exam_goal_label);
                int previousGoal = currentExam.getGradeGoal();
                if( previousGoal == -1 )
                {
                    if( calculatedExamGoal != null && !calculatedExamGoal.isEmpty() )
                    {
                        previousGoal = Integer.valueOf(calculatedExamGoal);
                        currentExam.setGradeGoal(Integer.valueOf(calculatedExamGoal));
                    }

                    examGoals.add(examGoal);

                }
                String goal;
                if(previousGoal != -1)
                {
                    goal = getString(R.string.goal) + ": " + previousGoal;
                }
                else
                {
                    goal = getString(R.string.goal) + ": ";
                }
                examGoal.setText(goal);

                EditText examGrade = (EditText)examRow.findViewById(R.id.exam_grade_entry);
                if( currentExam.getGrade() != -1  )
                {
                    examGrade.setText(String.valueOf(currentExam.getGrade()));
                }
                examGrade.addTextChangedListener(new TextWatcher()
                {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after)
                    {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count)
                    {

                    }

                    @Override
                    public void afterTextChanged(Editable s)
                    {
                        if( s.length() > 0 )
                        {
                            currentExam.setGrade(Integer.parseInt(s.toString()));
                            updateExamGoals();
                        }
                    }
                });
                subjectGoalLayout.addView(examRow);
            }
        }

        builder.setView(subjectGoalLayout);

        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                currentSubject.setGradeGoal(Integer.parseInt(subjectGoal));
                SubjectGoalDialog.this.getDialog().dismiss();
            }
        });

        return builder.create();
    }

    private void updateExamGoals()
    {
        ArrayList<Integer> grades = new ArrayList<>();
        for( Id examId : currentSubject.getExamIds() )
        {
            int grade = ExamService.getInstance().getExam(examId).getGrade();
            if( grade != -1 )
            {
                grades.add(grade);
            }
        }

        calculatedExamGoal = subjectGoal;
        if( !grades.isEmpty() )
        {
            float examCount = currentSubject.getExamIds().size();
            float remainingExams = examCount - grades.size();
            float remainingExamWeight = remainingExams / examCount;
            float totalGrade = 0;
            for( float grade : grades )
            {
                totalGrade += grade;
            }
            float currentAverage = totalGrade / grades.size();

            float gradeDelta = Integer.valueOf(subjectGoal) - currentAverage;

            calculatedExamGoal = String.valueOf(Math.round(gradeDelta / remainingExamWeight + currentAverage));
        }

        for( TextView exam : examGoals)
        {
            exam.setText(getString(R.string.goal) + ": " + calculatedExamGoal);
        }
    }
}

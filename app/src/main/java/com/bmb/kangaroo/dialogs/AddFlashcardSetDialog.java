package com.bmb.kangaroo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.services.FlashcardSetService;

public class AddFlashcardSetDialog extends DialogFragment
{
    private EditText titleEntry;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final LinearLayout addFlashcardSetLayout = (LinearLayout) inflater.inflate(R.layout.add_flashcardset_dialog, null);
        titleEntry = (EditText) addFlashcardSetLayout.findViewById(R.id.add_flashcard_set_title);

        builder.setView(addFlashcardSetLayout);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                FlashcardSet flashcardSet = new FlashcardSet(titleEntry.getText().toString());
                FlashcardSetService.getInstance().addFlashcardSet(flashcardSet);
                if( AddFlashcardSetDialog.this.getActivity() instanceof FlashcardSetCreatedListener )
                {
                    ((FlashcardSetCreatedListener) AddFlashcardSetDialog.this.getActivity()).onFlashcardSetCreated(flashcardSet);
                }

                AddFlashcardSetDialog.this.getDialog().dismiss();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                AddFlashcardSetDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

    public interface FlashcardSetCreatedListener {

        void onFlashcardSetCreated(FlashcardSet flashcardSet);
    }
}

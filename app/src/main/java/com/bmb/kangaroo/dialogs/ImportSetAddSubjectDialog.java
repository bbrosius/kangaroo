package com.bmb.kangaroo.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.utils.Id;

public class ImportSetAddSubjectDialog extends DialogFragment
{
    private EditText subjectNameEntry;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final LinearLayout addSubjectLayout = (LinearLayout) inflater.inflate(R.layout.add_subject_dialog, null);
        subjectNameEntry = (EditText) addSubjectLayout.findViewById(R.id.add_subject_name);

        builder.setView(addSubjectLayout);
        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                Subject subject = new Subject(subjectNameEntry.getText().toString());

                Id subjectId = SubjectService.getInstance().addSubject(subject);

                if( subjectId != null && ImportSetAddSubjectDialog.this.getActivity() instanceof SubjectCreatedListener )
                {
                    ((SubjectCreatedListener) ImportSetAddSubjectDialog.this.getActivity()).onSubjectCreated(subject);
                }
                ImportSetAddSubjectDialog.this.getDialog().dismiss();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                ImportSetAddSubjectDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

    public interface SubjectCreatedListener {

        void onSubjectCreated(Subject newSubject);
    }
}
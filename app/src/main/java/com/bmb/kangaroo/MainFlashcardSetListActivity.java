package com.bmb.kangaroo;

import android.Manifest;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bmb.kangaroo.dialogs.AddExamDialog;
import com.bmb.kangaroo.dialogs.AddFlashcardSetDialog;
import com.bmb.kangaroo.dialogs.AddSubjectDialog;
import com.bmb.kangaroo.dialogs.SubjectGoalDialog;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.services.ExamService;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.SubjectSpinnerAdapter;
import com.bmb.kangaroo.views.NotePaperLayout;
import com.bmb.kangaroo.views.NotePaperTextView;
import com.bmb.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * An activity representing a list of Subjects.
 * The activity makes heavy use of fragments. The list of items is a
 * {@code SubjectListFragment} and the item details (if present) is a
 * {@code NoteListFragment}.
 * <p>
 */
public class MainFlashcardSetListActivity extends BaseActivity implements
        AddSubjectDialog.AddSubjectListener,
		SharedPreferences.OnSharedPreferenceChangeListener, AddExamDialog.ExamCreatedListener, SubjectContentFragment.SubjectChildSelectedCallback,
        AddFlashcardSetDialog.FlashcardSetCreatedListener, ActivityCompat.OnRequestPermissionsResultCallback
{
    private SubjectContentFragment subjectContentFragment;

    private DrawerLayout mainLayout;
    private Spinner subjectSpinner;
    private ArrayAdapter<Subject> subjectSpinnerAdapter;
    private List<Subject> subjects;
    private Toolbar toolBar;

    private static final int REQUEST_STORAGE = 0;

    @Override
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_layout);

        SharedPreferences pref = getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
        pref.registerOnSharedPreferenceChangeListener(this);

        mainLayout = (DrawerLayout) findViewById(R.id.main_subject_layout);
        toolBar = (Toolbar) findViewById(R.id.main_toolbar);
        subjectSpinner = (Spinner) findViewById(R.id.subject_spinner);

        subjects = SubjectService.getInstance().getSubjects();

        subjectSpinnerAdapter = new SubjectSpinnerAdapter(this, subjects);

        subjectSpinner.setAdapter(subjectSpinnerAdapter);

        setSupportActionBar(toolBar);

        //Set the nav menu icon on the tool bar
        toolBar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mainLayout.openDrawer(GravityCompat.START);
            }
        });

        if( getSupportActionBar() != null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Bundle arguments = new Bundle();
        subjectContentFragment = new SubjectContentFragment();

        if( SubjectService.getInstance().getCurrentSubject() == null )
        {
            if( SubjectService.getInstance().getSubjects() != null && !SubjectService.getInstance().getSubjects().isEmpty() )
            {
                SubjectService.getInstance().setCurrentSubject(SubjectService.getInstance().getSubjects().get(0).getId());
            }
        }

        if( SubjectService.getInstance().getCurrentSubjectId() != null )
        {
            arguments.putString(getString(R.string.study_subject_id), String.valueOf(SubjectService.getInstance().getCurrentSubjectId()));
            subjectContentFragment.setArguments(arguments);
        }

        getFragmentManager().beginTransaction()
                .replace(R.id.main_content, subjectContentFragment).commit();

        if ( ActivityCompat.checkSelfPermission(Utils.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            requestStoragePermission();
        }
	}

    @Override
    protected void onPostResume()
    {
        super.onPostResume();
        subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Bundle arguments = new Bundle();
                subjectContentFragment = new SubjectContentFragment();
                arguments.putString(getString(R.string.study_subject_id), subjects.get(position).getId().toString());
                subjectContentFragment.setArguments(arguments);

                SubjectService.getInstance().setCurrentSubject(subjects.get(position).getId());

                getFragmentManager().beginTransaction()
                        .replace(R.id.main_content, subjectContentFragment).commitAllowingStateLoss();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
    }

    @Override
    public DrawerLayout getDrawerLayout()
    {
        return mainLayout;
    }

    @Override
	protected void onDestroy() 
	{
		super.onDestroy();
		SubjectService.getInstance().saveSubjectsToFile();
		ExamService.getInstance().saveExamsToFile();
        FlashcardSetService.getInstance().saveFlashcardSetsToFile();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu)
	{
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_subject_list_menu, menu);

	    Subject subject = SubjectService.getInstance().getSubject(SubjectService.getInstance().getCurrentSubjectId());
		if( subject == null || subject.title.equalsIgnoreCase(this.getString(R.string.add_subject)) )
		{
	        MenuItem addExam = menu.findItem(R.id.add_exam);
	        if( addExam != null )
	        {
	        	addExam.setEnabled(false);
	        }

            MenuItem setGoal = menu.findItem(R.id.set_study_goal);
            if( setGoal != null )
            {
                setGoal.setEnabled(false);
            }
		}

	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
	    switch( item.getItemId() )
	    {
            case R.id.add_subject:
                DialogFragment subjectDialog = new AddSubjectDialog();
                subjectDialog.show(getFragmentManager(), getString(R.string.add_subject_dialog_tag));
                return true;
	        case R.id.add_exam:
	        	DialogFragment dialog = new AddExamDialog();
		        dialog.show( getFragmentManager(), getString(R.string.add_exam_dialog_tag) );
	        	return true;
            case R.id.set_study_goal:
                SubjectGoalDialog goalDialog = new SubjectGoalDialog();
                goalDialog.show( getFragmentManager(), getString(R.string.study_goal_dialog_id) );
                return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) 
	{

	}

	@Override
	public void onSubjectAdded(Subject subject)
    {
        Bundle arguments = new Bundle();
        subjectContentFragment = new SubjectContentFragment();
        arguments.putString(getString(R.string.study_subject_id), subject.getId().toString());
        subjectContentFragment.setArguments(arguments);

        getFragmentManager().beginTransaction()
                .replace(R.id.main_content, subjectContentFragment).commit();

        //Add the new subject to the spinner
        subjectSpinnerAdapter.add(subject);
        subjectSpinnerAdapter.notifyDataSetChanged();
        invalidateOptionsMenu();

        subjectSpinner.invalidate();

        //Navigate to the newly added subject.
        subjectSpinner.setSelection(subjects.size() - 1);
	}

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if( key.equalsIgnoreCase(getString(R.string.subject_color_key)))
        {
            int actionbarColor = sharedPreferences.getInt(key, getResources().getColor(R.color.primary_color));

            if( getSupportActionBar() != null )
            {
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(actionbarColor));
            }
        }
        else if( key.equalsIgnoreCase(getString(R.string.light_theme_key)) )
        {
            Intent resetIntent = new Intent(this, MainFlashcardSetListActivity.class);
            resetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(resetIntent);
            finish();
        }
        else if( key.equalsIgnoreCase(getString(R.string.show_next_exam_countdown_key)) )
        {
            boolean showHeader = sharedPreferences.getBoolean(key, false);
            if( subjectContentFragment != null )
            {
                subjectContentFragment.setCountdownClockVisibility(showHeader);
            }
        }
    }

    @Override
    public void onExamCreated()
    {
        SharedPreferences pref = getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);
        boolean showCountdown = pref.getBoolean(getString(R.string.show_next_exam_countdown_key), true);
        if( showCountdown && subjectContentFragment != null )
        {
            subjectContentFragment.setCountdownClockVisibility(true);
        }
    }

    @Override
    public void onFlashcardSetSelected(Id flashcardSetId, NotePaperLayout setCard, NotePaperTextView titleView, ImageView quizletImage, NotePaperTextView cardCount)
    {
        if( flashcardSetId != null && flashcardSetId.getUid() != SubjectContentFragment.NO_FLASHCARD_SETS_ID &&
                flashcardSetId.getUid() != SubjectContentFragment.NO_SUBJECTS_ID )
        {
            Intent flashcardIntent = new Intent(this, FlashcardActivity.class);
            flashcardIntent.putExtra(getString(R.string.study_id), flashcardSetId.toString());
            flashcardIntent.putExtra(getString(R.string.flashcard_calling_activity), FlashcardActivity.SUBJECT_LIST);
            ArrayList<Pair<View, String>> sharedElements = new ArrayList<>();
            sharedElements.add(new Pair<View, String>(titleView, getString(R.string.transition_set_title)));
            sharedElements.add(new Pair<View, String>(toolBar, getString(R.string.transition_toolbar)));
            FlashcardSet set = FlashcardSetService.getInstance().getFlashcardSet(flashcardSetId);
            if( set.isQuizletSet() )
            {
                sharedElements.add(new Pair<View, String>(quizletImage, getString(R.string.transition_quizlet_image)));
            }
            //TODO: This is commented out because the transition doesn't work when it's included on Marshmallow
            //sharedElements.add(new Pair<View, String>(setCard, getString(R.string.transition_set_card)));
            sharedElements.add(new Pair<View, String>(cardCount, getString(R.string.transition_card_count)));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    // the context of the activity
                    this,
                    // For each shared element, add to this method a new Pair item,
                    // which contains the reference of the view we are transitioning *from*,
                    // and the value of the transitionName attribute
                    sharedElements.toArray(new Pair[sharedElements.size()])
            );

            ActivityCompat.startActivity(MainFlashcardSetListActivity.this, flashcardIntent, options.toBundle());
        }
    }

    /**
     * Requests the Storage permissions for saving and loading the subjects and flashcard sets permission.
     */
    private void requestStoragePermission() {
        Log.i(getString(R.string.log_tag), "Requesting storage permission");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.i(getString(R.string.log_tag),
                    "Displaying storage permission rationale to provide additional context.");
            Snackbar.make(mainLayout, R.string.storage_reason_phrase,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainFlashcardSetListActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_STORAGE);
                        }
                    })
                    .show();
        } else {
            //Storage permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE);
        }
    }

    //Handles request to get the storage permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_STORAGE)
        {
            //Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                //Storage permission has been granted, preview can be displayed
                Snackbar.make(mainLayout, R.string.storage_permission_granted,
                        Snackbar.LENGTH_SHORT).show();
            }
            else
            {
                Log.e(getString(R.string.log_tag), "Storage permission was NOT granted.");
                Snackbar.make(mainLayout, R.string.storage_permission_rejected,
                        Snackbar.LENGTH_SHORT).show();
            }
        }
        else
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onFlashcardSetCreated(FlashcardSet flashcardSet)
    {
        SubjectService.getInstance().getCurrentSubject().addFlashcardSet(flashcardSet.getId());
        if( subjectContentFragment != null )
        {
            subjectContentFragment.newChildAdded(flashcardSet);
        }
    }
}

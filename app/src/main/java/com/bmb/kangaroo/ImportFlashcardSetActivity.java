package com.bmb.kangaroo;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.bmb.kangaroo.dialogs.ImportSetAddSubjectDialog;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.Subject;
import com.bmb.kangaroo.model.builder.FlashcardSetJSONBuilder;
import com.bmb.kangaroo.model.services.ExamService;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.SubjectSpinnerAdapter;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Activity for importing flashcard sets that have been exported from the app. This is needed for sharing flashcard sets.
 */
public class ImportFlashcardSetActivity extends Activity implements ImportSetAddSubjectDialog.SubjectCreatedListener
{
    private Spinner subjectSpinner;
    private SubjectSpinnerAdapter subjectAdapter;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        LinearLayout importFlashcardSetLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.import_flashcard_set, null);
        subjectSpinner = (Spinner) importFlashcardSetLayout.findViewById(R.id.subject_selection);
        subjectAdapter = new SubjectSpinnerAdapter(this, SubjectService.getInstance().getSubjects());
        subjectSpinner.setAdapter(subjectAdapter);

        final Uri data = getIntent().getData();
        if( data != null )
        {
            getIntent().setData(null);
        }

        //TextView importingFile = (TextView) importFlashcardSetLayout.findViewById(R.id.imported_set_title);
        //importingFile.setText(data.toString());

        Button addNewSubject = (Button) importFlashcardSetLayout.findViewById(R.id.add_new_subject_button);
        addNewSubject.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogFragment dialog = new ImportSetAddSubjectDialog();
                dialog.show(getFragmentManager(), getString(R.string.add_subject_dialog_tag));
            }
        });

        Button importSet = (Button) importFlashcardSetLayout.findViewById(R.id.import_set);
        importSet.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Subject selectedSubject = (Subject) subjectSpinner.getSelectedItem();
                FlashcardSet importedSet = loadFlashcardSet(data);

                Id importedId = FlashcardSetService.getInstance().addFlashcardSet(importedSet);

                selectedSubject.addFlashcardSet(importedId);

                FlashcardDAO flashcardDAO = new FlashcardDAO();
                flashcardDAO.open();

                Id nextExamId = null;
                if( selectedSubject.getNextExam() != null )
                {
                    nextExamId = selectedSubject.getNextExam().getId();
                }
                for( long flashcardId : importedSet.getFlashcardIds() )
                {
                    flashcardDAO.updateFlashcard(selectedSubject.getId(), nextExamId, importedId, flashcardId);
                }

                flashcardDAO.close();

                SubjectService.getInstance().saveSubjectsToFile();
                ExamService.getInstance().saveExamsToFile();
                FlashcardSetService.getInstance().saveFlashcardSetsToFile();

                NavUtils.navigateUpFromSameTask(ImportFlashcardSetActivity.this);
            }
        });

        setContentView(importFlashcardSetLayout);
    }

    private FlashcardSet loadFlashcardSet(Uri setUri)
    {
        FlashcardSet importedSet = null;
        final String scheme = setUri.getScheme();

        if( ContentResolver.SCHEME_CONTENT.equals(scheme) || ContentResolver.SCHEME_FILE.equals(scheme) )
        {
            try
            {
                ContentResolver cr = getContentResolver();
                InputStream is = cr.openInputStream(setUri);
                if( is != null )
                {
                    StringBuilder buf = new StringBuilder();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String str;
                    while( (str = reader.readLine()) != null )
                    {
                        buf.append(str).append("\n");
                    }
                    is.close();

                    JSONObject json = new JSONObject(buf.toString());

                    importedSet = FlashcardSetJSONBuilder.deserializeFlashcardSet(json, this);
                }
            }
            catch( Exception e )
            {
                Log.e(getString(R.string.log_tag), "Error opening flashcard set file: " + e.getMessage());
            }
        }

        return importedSet;
    }

    @Override
    public void onSubjectCreated(Subject newSubject)
    {
        subjectAdapter.setSubjects(SubjectService.getInstance().getSubjects());
        int pos = subjectAdapter.getSubjects().indexOf(newSubject);
        subjectAdapter.notifyDataSetChanged();

        if( pos != -1 )
        {
            subjectSpinner.setSelection(pos);
        }
    }
}
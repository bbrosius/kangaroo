package com.bmb.kangaroo.oauth2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.bmb.kangaroo.R;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.Key;

/**
 * Execute the OAuthRequestTokenTask to retrieve the request, and authorize the request.
 * After the request is authorized by the user, the callback URL will be intercepted here.
 */
@SuppressLint("SetJavaScriptEnabled")
public class OauthLoginFragment extends Fragment
{
    private SharedPreferences prefs;
    private String redirectUri;

    private OauthLoginFinished loginHandler = dummyOathHandler;

    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();
    private HttpRequestFactory requestFactory;

    public OauthLoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
            public void initialize(HttpRequest request) {
                request.setParser(new JsonObjectParser(JSON_FACTORY));
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.i(getString(R.string.log_tag), "Starting task to retrieve request token.");
        prefs = getActivity().getSharedPreferences(getString(R.string.preferences_key), Context.MODE_PRIVATE);

        redirectUri = getString(R.string.quizlet_redirect_uri);

        String authorizationUrl = "https://quizlet.com/authorize?response_type=code&client_id=" + getString(R.string.quizlet_client_id) + "&scope=read%20write_set&state=RANDOM_STRING";
        LinearLayout oauthLoginLayout = (LinearLayout) inflater.inflate(R.layout.oauth_login_fragment, null);
        WebView webview = (WebView) oauthLoginLayout.findViewById(R.id.oauth_web_view);
        webview.getSettings().setJavaScriptEnabled(true);

        Log.i(getString(R.string.log_tag), "Using authorizationUrl = " + authorizationUrl);

        webview.setWebViewClient(new WebViewClient()
        {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {

                if( url.startsWith(redirectUri) )
                {
                    // extract OAuth2 get the access code appended in url
                    if( url.contains("code=") )
                    {

                        //Retrieve the temporary access code to get the token
                        String code = extractCode(url);
                        if( code != null && !code.isEmpty() )
                        {
                            new RetrieveTokenTask().execute(code);
                        }
                        else
                        {
                            Toast.makeText(getActivity(), R.string.oauth_login_url_error, Toast.LENGTH_LONG).show();
                            loginHandler.oauthFinished(false);
                        }

                    }
                    else
                    {
                        Toast.makeText(getActivity(), R.string.oauth_login_url_error, Toast.LENGTH_LONG).show();
                        loginHandler.oauthFinished(false);
                    }

                    // don't go to redirectUri
                    return true;
                }

                // load the webpage from url: login and grant access
                return super.shouldOverrideUrlLoading(view, url); // return false;
            }
        });

        webview.loadUrl(authorizationUrl);

        return oauthLoginLayout;
    }

    private String extractCode(String url)
    {
        String code;
        // url has format "YOUR_URL/?code=GENERATED_CODE&state=YOUR_STATE&expires_in=60"
        String[] sArray = url.split("code=");
        code =  (sArray[1].split("&state="))[0];

        return code;
    }

    public interface OauthLoginFinished
    {
        void oauthFinished(boolean loginSuccessful);
    }

    private final static OauthLoginFinished dummyOathHandler = new OauthLoginFinished()
    {
        @Override
        public void oauthFinished(boolean loginSuccessful)
        {
        }
    };

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (!(activity instanceof OauthLoginFinished))
        {
            throw new IllegalStateException(
                    "Activity must implement OauthLogin fragment callbacks.");
        }

        loginHandler = (OauthLoginFinished) activity;
    }

    public static class AuthenticatedUserResponse
    {
        @Key("user_id")
        public String userId;

        @Key("access_token")
        public String token;

        @Key("expires_in")
        public long expiration;
    }

    public static class ErrorResponse
    {
        @Key("error")
        public String errorType;

        @Key("error_description")
        public String errorMessage;
    }

    private class RetrieveTokenTask extends AsyncTask<String, Void, String>
    {
        private final ProgressDialog retrieveTokenDialog;

        public RetrieveTokenTask()
        {
            retrieveTokenDialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            retrieveTokenDialog.setMessage(getString(R.string.oauth_in_progress));
            retrieveTokenDialog.show();
        }

        @Override
        protected String doInBackground(String... accessCode)
        {
            // create entity object
            GenericUrl accessCodeUrl = new GenericUrl(getString(R.string.quizlet_token_url));

            accessCodeUrl.put("code", accessCode[0]);
            accessCodeUrl.put("grant_type", "authorization_code");
            accessCodeUrl.put("redirect_uri", "flashcard-fun:/oath-callback");

            try
            {
                HttpRequest accessPostRequest = requestFactory.buildPostRequest(accessCodeUrl, null);
                HttpHeaders headers = new HttpHeaders();

                String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                        (getString(R.string.quizlet_client_id) + ":" + getString(R.string.quizlet_app_secret)).getBytes(),
                        Base64.NO_WRAP);
                headers.setAuthorization(base64EncodedCredentials);
                accessPostRequest.setHeaders(headers);

                HttpResponse response = accessPostRequest.execute();

                if( response.getStatusCode() == 200 )
                {
                    AuthenticatedUserResponse userResponse = response.parseAs(AuthenticatedUserResponse.class);

                    if( userResponse != null )
                    {
                        // store in default SharedPreferences
                        SharedPreferences.Editor e = prefs.edit();
                        e.putString(getString(R.string.quizlet_user_id_key), userResponse.userId);
                        e.putString(getString(R.string.quizlet_token_key), userResponse.token);
                        long expirationTime = System.currentTimeMillis() + userResponse.expiration;
                        e.putLong(getString(R.string.quizlet_token_expiration_key), expirationTime);
                        e.apply();

                        return userResponse.userId;
                    }
                }
                else
                {
                    final ErrorResponse errorResponse = response.parseAs(ErrorResponse.class);

                    if( errorResponse != null )
                    {
                        Log.e(getString(R.string.log_tag), "Error getting access token " + errorResponse.errorType + ": " + errorResponse.errorMessage);
                        getActivity().runOnUiThread(new Runnable()
                        {
                            public void run()
                            {
                                Toast.makeText(getActivity(), "Error getting access token " + errorResponse.errorType + ": " + errorResponse.errorMessage, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
            catch( final Exception e )
            {
                Log.e(getString(R.string.log_tag), "Error getting user name and authenticating token: " + e.getMessage());
                getActivity().runOnUiThread(new Runnable()
                {
                    public void run()
                    {
                        Toast.makeText(getActivity(), "Error getting user name and authenticating token: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
            return null;
        }

        /**
         * When we're done and we've retrieved either a valid token or an error from the server,
         * we'll return to our original activity
         */
        @Override
        protected void onPostExecute(String username)
        {
            if( retrieveTokenDialog.isShowing() )
            {
                retrieveTokenDialog.dismiss();
            }

            boolean userNameRetrieved = false;
            if( username != null && !username.isEmpty() )
            {
                userNameRetrieved = true;
            }
            loginHandler.oauthFinished(userNameRetrieved);
        }
    }
}

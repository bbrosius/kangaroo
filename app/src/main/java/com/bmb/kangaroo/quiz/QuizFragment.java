package com.bmb.kangaroo.quiz;

import android.support.v4.app.Fragment;

public abstract class QuizFragment extends Fragment
{
    abstract public int gradeQuiz();
    
    abstract public int getQuestionCount();
    
    abstract public void resetQuiz();
}

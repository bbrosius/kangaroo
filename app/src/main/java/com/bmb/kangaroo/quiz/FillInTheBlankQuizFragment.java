package com.bmb.kangaroo.quiz;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;

import java.util.ArrayList;
import java.util.Random;

public class FillInTheBlankQuizFragment extends QuizFragment
{
    private Cursor flashcards;
    private FlashcardDAO flashcardDao;
    private Id studyId;
    private boolean lockQuiz;
    private LinearLayout quizForm;
    private ArrayList<String> correctAnswers;

    public FillInTheBlankQuizFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if( getArguments() != null && getArguments().containsKey(getString(R.string.study_id)) )
        {
            studyId = IdService.resolveId(getArguments().getString(getString(R.string.study_id)));
        }

        flashcardDao = new FlashcardDAO();
        flashcardDao.open();

        flashcards = flashcardDao.findFlashcards(studyId, true);
        flashcards.moveToFirst();

        correctAnswers = new ArrayList<>();

        lockQuiz = false;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        flashcardDao.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        LinearLayout quizLayout = (LinearLayout) inflater.inflate(R.layout.quiz_container_layout, container, false);

        createView(inflater, quizLayout);
        return quizLayout;
    }

    private void createView(LayoutInflater inflater, LinearLayout quizLayout)
    {
        quizForm = (LinearLayout) quizLayout.findViewById(R.id.quiz_form);

        for(int i = 0; i < flashcards.getCount(); i++)
        {
            flashcards.moveToPosition(i);

            String definition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
            int blankIndex = determineBlank(definition);
            String definitionStart = definition.substring(0, blankIndex);
            String question = (i + 1) + ") " + flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)) + " - " + definitionStart;
            int answerLength = correctAnswers.get(i).length();
            String defintionEnd = definition.substring(blankIndex + answerLength);

            final LinearLayout quizQuestion = new LinearLayout(getActivity());
            quizQuestion.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            quizQuestion.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout.LayoutParams questionParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView questionStart = new TextView(getActivity());
            questionStart.setLayoutParams(questionParams);
            questionStart.setText(question);
            quizQuestion.addView(questionStart);

            EditText questionBlank = new EditText(getActivity());
            questionBlank.setEms(answerLength + 5);
            questionBlank.setLayoutParams(questionParams);
            quizQuestion.addView(questionBlank);

            TextView questionEnd = new TextView(getActivity());
            questionEnd.setLayoutParams(questionParams);
            questionEnd.setText(defintionEnd);
            quizQuestion.addView(questionEnd);


            quizForm.addView(quizQuestion);
        }
    }

    private int determineBlank(String definition)
    {
        String[] split = definition.split("\\s");
        ArrayList<String> possibleAnswers = new ArrayList<>();

        for(int i = 0; i < split.length; i++)
        {
            if(split[i].length() >= 5)
            {
                if( wordAllowed(split[i]) )
                {
                    possibleAnswers.add(split[i]);
                }
            }
        }

        if( possibleAnswers.isEmpty() )
        {
            for(int i = 0; i < split.length; i++)
            {
                if(split[i].length() >= 4)
                {
                    if( wordAllowed(split[i]) )
                    {
                        possibleAnswers.add(split[i]);
                    }
                }
            }
        }

        if( possibleAnswers.isEmpty() )
        {
            for(int i = 0; i < split.length; i++)
            {
                if(split[i].length() >= 3)
                {
                    if( wordAllowed(split[i]) )
                    {
                        possibleAnswers.add(split[i]);
                    }
                }
            }
        }

        if( possibleAnswers.isEmpty() )
        {
            for(int i = 0; i < split.length; i++)
            {
                if(split[i].length() >= 2)
                {
                    if( wordAllowed(split[i]) )
                    {
                        possibleAnswers.add(split[i]);
                    }
                }
            }
        }

        if( possibleAnswers.isEmpty() )
        {
            for(int i = 0; i < split.length; i++)
            {
                if(split[i].length() >= 1)
                {
                    if( wordAllowed(split[i]) )
                    {
                        possibleAnswers.add(split[i]);
                    }
                }
            }
        }
        Random generator = new Random();
        int answerLocation = 0;
        if(possibleAnswers.size() > 0)
        {
            int blankIndex = 0;
            generator.nextInt(possibleAnswers.size());
            correctAnswers.add(possibleAnswers.get(blankIndex));
            answerLocation = definition.indexOf(possibleAnswers.get(blankIndex));
        }

        return answerLocation;
    }

    private boolean wordAllowed(String word)
    {
        boolean allowed = true;
        if(word.equalsIgnoreCase("WHERE"))
        {
            allowed = false;
        }
        else if(word.equalsIgnoreCase("WHICH"))
        {
            allowed = false;
        }
        else if(word.equalsIgnoreCase("MISTER"))
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("MISSES") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("WHEN") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("WHAT") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("THEN") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("THAN") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("THAT") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("AND") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("BUT") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("THE") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("HIS") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("HIM") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("HER") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("HERS") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("SHE") )
        {
            allowed = false;
        }
        else if( word.equalsIgnoreCase("yes") )
        {
            allowed = false;
        }
        else if (word.equalsIgnoreCase("whether") )
        {
            allowed = false;
        }
        else if (word.equalsIgnoreCase("however"))
        {
            allowed = false;
        }
        else if (word.equalsIgnoreCase("thus"))
        {
            allowed = false;
        }
        return allowed;
    }

    @Override
    public int gradeQuiz()
    {
        int correct = 0;
        for( int i = 0; i < flashcards.getCount(); i++)
        {
            flashcards.moveToPosition(i);
            String correctAnswer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));

            LinearLayout quizQuestion = (LinearLayout) quizForm.getChildAt(i);
            TextView answerView = (TextView) quizQuestion.findViewById(R.id.matching_answer);
            String answer = "";
            if( answerView.getText() != null )
            {
                answer = answerView.getText().toString();
            }

            if(correctAnswer.equalsIgnoreCase(answer))
            {
                answerView.setTextColor(Color.GREEN);
                correct++;
            }
            else
            {
                answerView.setTextColor(Color.RED);
                answerView.setText(correctAnswer);
            }
        }
        lockQuiz = true;

        return correct;
    }

    @Override
    public int getQuestionCount()
    {
        return flashcards.getCount();
    }

    @Override
    public void resetQuiz()
    {

    }

}

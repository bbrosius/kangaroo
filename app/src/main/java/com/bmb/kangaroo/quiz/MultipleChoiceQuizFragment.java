package com.bmb.kangaroo.quiz;


import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;

import java.util.ArrayList;
import java.util.Collections;

public class MultipleChoiceQuizFragment extends QuizFragment
{

    private Cursor flashcards;
    private FlashcardDAO flashcardDao;
    private Id studyId;
    private SparseArray<Integer> correctAnswers;
    private SparseArray<Integer> selectedAnswers;
    private SparseArray<ArrayList<Integer>> answerPositions;
    private boolean lockQuiz;
    private LinearLayout quizForm;
    private int questionCount = 20;

    public MultipleChoiceQuizFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if( getArguments() != null && getArguments().containsKey(getString(R.string.study_id)) )
        {
            studyId = IdService.resolveId(getArguments().getString(getString(R.string.study_id)));
        }

        if( getArguments() != null && getArguments().containsKey(getString(R.string.quiz_question_count_arg)) )
        {
            questionCount = getArguments().getInt(getString(R.string.quiz_question_count_arg));
        }

        flashcardDao = new FlashcardDAO();
        flashcardDao.open();

        if( studyId != null )
        {
            flashcards = flashcardDao.findFlashcards(studyId, true);
        }

        if( flashcards != null )
        {
            if( flashcards.getCount() < questionCount )
            {
                questionCount = flashcards.getCount();
            }

            flashcards.moveToFirst();
        }

        correctAnswers = new SparseArray<>();
        answerPositions = new SparseArray<>();
        selectedAnswers = new SparseArray<>();

        lockQuiz = false;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        flashcardDao.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        LinearLayout quizLayout = (LinearLayout) inflater.inflate(R.layout.quiz_container_layout, container, false);
        quizForm = (LinearLayout) quizLayout.findViewById(R.id.quiz_form);

        createView(inflater, quizForm);
        return quizLayout;
    }

    private void createView(LayoutInflater inflater, LinearLayout quizLayout)
    {

        for(int i = 0; i < questionCount; i++)
        {
            if( flashcards != null )
            {
                flashcards.moveToPosition(i);
            }
            RelativeLayout quizQuestion = (RelativeLayout) inflater.inflate(R.layout.multiple_choice_question, null);
            ArrayList<Integer> availableAnswers = new ArrayList<>();
            int correctPosition;
            if( answerPositions.get(i) == null )
            {
                availableAnswers.add(1);
                availableAnswers.add(2);
                availableAnswers.add(3);
                availableAnswers.add(4);
                Collections.shuffle(availableAnswers);

                correctPosition = availableAnswers.get(0);
                correctAnswers.put(i, correctPosition);
                answerPositions.put(i, availableAnswers);
            }
            else
            {
                availableAnswers = answerPositions.get(i);
                correctPosition = availableAnswers.get(0);
            }

            String correctAnswer = "";
            String question = "";
            if( flashcards != null )
            {
                correctAnswer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                question = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
            }

            TextView questionNumber = (TextView)quizQuestion.findViewById(R.id.multiple_choice_question_number);
            questionNumber.setText( (i + 1) + ")");

            if( flashcards != null )
            {
                if( flashcards.getInt(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_IS_PICTURE)) == 1 )
                {
                    TextView questionView = (TextView) quizQuestion.findViewById(R.id.multiple_choice_question);
                    questionView.setVisibility(View.GONE);
                    ImageView questionImageView = (ImageView) quizQuestion.findViewById(R.id.multiple_choice_question_image);

                    String imageUri = correctAnswer;
                    correctAnswer = question;

                    GraphicUtils.loadBitmap(Uri.parse(imageUri), 250, 250, getActivity(), questionImageView);

                    questionImageView.setVisibility(View.VISIBLE);
                }
                else
                {
                    TextView questionView = (TextView) quizQuestion.findViewById(R.id.multiple_choice_question);
                    questionView.setText(question);
                    questionView.setVisibility(View.VISIBLE);
                    ImageView questionImageView = (ImageView) quizQuestion.findViewById(R.id.multiple_choice_question_image);
                    questionImageView.setVisibility(View.GONE);
                }
            }

            mapAnswerToButton(quizQuestion, correctPosition, correctAnswer, i);

            int answerCount = 3;
            if( flashcards != null )
            {
                if( flashcards.getCount() < 4 )
                {
                    answerCount = flashcards.getCount() - 1;
                }
                for( int j = 0; j < answerCount; j++ )
                {
                    if( flashcards.isLast() )
                    {
                        flashcards.moveToFirst();
                    }
                    else
                    {
                        flashcards.moveToNext();
                    }

                    String answer;
                    if( flashcards.getInt(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_IS_PICTURE)) == 1 )
                    {
                        answer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                    }
                    else
                    {
                        answer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                    }
                    int position = availableAnswers.get(j + 1);
                    mapAnswerToButton(quizQuestion, position, answer, i);
                }
            }
            quizLayout.addView(quizQuestion);
        }
    }

    private void mapAnswerToButton(final RelativeLayout question, Integer position, String answer, final int questionNumber)
    {
        switch(position)
        {
            case 1:
                final CardView answerOne = (CardView) question.findViewById(R.id.multiple_choice_answer_one);
                answerOne.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        handleAnswerPress(1, question, questionNumber);
                    }
                });
                answer = "a) " + answer;
                final TextView answerOneText = (TextView) question.findViewById(R.id.multiple_choice_answer_one_text);
                answerOneText.setText(answer);
                break;
            case 2:
                final CardView answerTwo = (CardView) question.findViewById(R.id.multiple_choice_answer_two);
                answer = "b) " + answer;
                final TextView answerTwoText = (TextView) question.findViewById(R.id.multiple_choice_answer_two_text);
                answerTwoText.setText(answer);
                answerTwo.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        handleAnswerPress(2, question, questionNumber);
                    }
                });
                break;
            case 3:
                final CardView answerThree = (CardView) question.findViewById(R.id.multiple_choice_answer_three);
                final TextView answerThreeText = (TextView) question.findViewById(R.id.multiple_choice_answer_three_text);
                answer = "c) " + answer;
                answerThreeText.setText(answer);
                answerThree.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        handleAnswerPress(3, question, questionNumber);
                    }
                });
                break;
            case 4:
                final CardView answerFour = (CardView) question.findViewById(R.id.multiple_choice_answer_four);
                final TextView answerFourText = (TextView) question.findViewById(R.id.multiple_choice_answer_four_text);
                answer = "d) " + answer;
                answerFourText.setText(answer);
                answerFour.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        handleAnswerPress(4, question, questionNumber);
                    }
                });
                break;
        }
    }

    private void colorGradedAnswers(final RelativeLayout question, int questionNumber, boolean correct)
    {
        CardView answerOne = (CardView) question.findViewById(R.id.multiple_choice_answer_one);
        CardView answerTwo = (CardView) question.findViewById(R.id.multiple_choice_answer_two);
        CardView answerThree = (CardView) question.findViewById(R.id.multiple_choice_answer_three);
        CardView answerFour = (CardView) question.findViewById(R.id.multiple_choice_answer_four);

        switch(correctAnswers.get(questionNumber))
        {
            case 1:
                answerOne.setBackgroundResource(R.color.correct);
                answerTwo.setBackgroundColor(Color.WHITE);
                answerThree.setBackgroundColor(Color.WHITE);
                answerFour.setBackgroundColor(Color.WHITE);
                break;
            case 2:
                answerOne.setBackgroundColor(Color.WHITE);
                answerTwo.setBackgroundResource(R.color.correct);
                answerThree.setBackgroundColor(Color.WHITE);
                answerFour.setBackgroundColor(Color.WHITE);
                break;
            case 3:
                answerOne.setBackgroundColor(Color.WHITE);
                answerTwo.setBackgroundColor(Color.WHITE);
                answerThree.setBackgroundResource(R.color.correct);
                answerFour.setBackgroundColor(Color.WHITE);
                break;
            case 4:
                answerOne.setBackgroundColor(Color.WHITE);
                answerTwo.setBackgroundColor(Color.WHITE);
                answerThree.setBackgroundColor(Color.WHITE);
                answerFour.setBackgroundResource(R.color.correct);
                break;
        }

        if( !correct )
        {
            if(selectedAnswers.get(questionNumber) == null)
            {
                switch(correctAnswers.get(questionNumber))
                {
                    case 1:
                        answerOne.setBackgroundResource(R.color.incorrect);
                        break;
                    case 2:
                        answerTwo.setBackgroundResource(R.color.incorrect);
                        break;
                    case 3:
                        answerThree.setBackgroundResource(R.color.incorrect);
                        break;
                    case 4:
                        answerFour.setBackgroundResource(R.color.incorrect);
                        break;
                }
            }
            else
            {
                switch(selectedAnswers.get(questionNumber))
                {
                    case 1:
                        answerOne.setBackgroundResource(R.color.incorrect);
                        break;
                    case 2:
                        answerTwo.setBackgroundResource(R.color.incorrect);
                        break;
                    case 3:
                        answerThree.setBackgroundResource(R.color.incorrect);
                        break;
                    case 4:
                        answerFour.setBackgroundResource(R.color.incorrect);
                        break;
                }
            }
        }
    }

    private void handleAnswerPress(int position, final RelativeLayout question, int questionNumber)
    {
        if( !lockQuiz )
        {
            CardView answerOne = (CardView) question.findViewById(R.id.multiple_choice_answer_one);
            CardView answerTwo = (CardView) question.findViewById(R.id.multiple_choice_answer_two);
            CardView answerThree = (CardView) question.findViewById(R.id.multiple_choice_answer_three);
            CardView answerFour = (CardView) question.findViewById(R.id.multiple_choice_answer_four);
            selectedAnswers.put(questionNumber, position);
            switch(position)
            {
                case 1:
                    answerOne.setBackgroundResource(R.color.selected_answer);
                    answerTwo.setBackgroundColor(Color.WHITE);
                    answerThree.setBackgroundColor(Color.WHITE);
                    answerFour.setBackgroundColor(Color.WHITE);
                    break;
                case 2:
                    answerOne.setBackgroundColor(Color.WHITE);
                    answerTwo.setBackgroundResource(R.color.selected_answer);
                    answerThree.setBackgroundColor(Color.WHITE);
                    answerFour.setBackgroundColor(Color.WHITE);
                    break;
                case 3:
                    answerOne.setBackgroundColor(Color.WHITE);
                    answerTwo.setBackgroundColor(Color.WHITE);
                    answerThree.setBackgroundResource(R.color.selected_answer);
                    answerFour.setBackgroundColor(Color.WHITE);
                    break;
                case 4:
                    answerOne.setBackgroundColor(Color.WHITE);
                    answerTwo.setBackgroundColor(Color.WHITE);
                    answerThree.setBackgroundColor(Color.WHITE);
                    answerFour.setBackgroundResource(R.color.selected_answer);
                    break;
            }
        }
    }

    @Override
    public int gradeQuiz()
    {
        int correct = 0;
        for( int i = 0; i < questionCount; i++)
        {
            RelativeLayout quizQuestion = (RelativeLayout) quizForm.getChildAt(i);
            if( selectedAnswers.get(i) == null )
            {
                colorGradedAnswers(quizQuestion, i, false);
            }
            else if(!selectedAnswers.get(i).equals(correctAnswers.get(i)))
            {
                colorGradedAnswers(quizQuestion, i, false);
            }
            else
            {
                colorGradedAnswers(quizQuestion, i, true);
                correct++;
            }
        }
        lockQuiz = true;

        return correct;
    }

    @Override
    public int getQuestionCount()
    {
        return questionCount;
    }

    @Override
    public void resetQuiz()
    {
        lockQuiz = false;
        quizForm.removeAllViews();
        correctAnswers.clear();
        answerPositions.clear();
        selectedAnswers.clear();
        createView(getLayoutInflater(null), quizForm);
    }
}

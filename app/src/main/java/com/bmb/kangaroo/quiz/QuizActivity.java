package com.bmb.kangaroo.quiz;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.bmb.kangaroo.BaseActivity;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;

public class QuizActivity extends BaseActivity implements ChooseQuizFragment.QuizModeChosen
{
    private Id studyId;

    public final static int MAX_QUESTION_COUNT = 20;
    private QuizFragment quizFragment;
    private ChooseQuizFragment chooseQuizFragment;

    private Menu menu;

    private boolean quizTypeSelected = false;
    private DrawerLayout quizLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.quiz_layout);

        Toolbar toolBar = (Toolbar) findViewById(R.id.quiz_toolbar);
        setSupportActionBar(toolBar);

        quizLayout = (DrawerLayout) findViewById(R.id.quiz_layout);

        //Set the nav menu icon on the tool bar
        toolBar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                quizLayout.openDrawer(GravityCompat.START);
            }
        });

        if(getIntent().hasExtra(getString(R.string.study_id)))
        {
            studyId = IdService.resolveId(getIntent().getStringExtra(getString(R.string.study_id)));
        }
        else
        {
            Snackbar.make(quizLayout, R.string.quiz_no_subjects,
                    Snackbar.LENGTH_LONG).show();
        }

        quizFragment = (QuizFragment) getSupportFragmentManager().findFragmentById(R.id.quiz_container);
        if( quizFragment == null )
        {
            chooseQuizFragment = new ChooseQuizFragment();

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.quiz_container, chooseQuizFragment).commit();
        }
    }

    @Override
    public DrawerLayout getDrawerLayout()
    {
        return quizLayout;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.quiz_menu, menu);

        MenuItem reset = menu.findItem(R.id.reset_quiz);
        reset.setVisible(false);

        MenuItem gradeQuiz = menu.findItem(R.id.grade_quiz);
        gradeQuiz.setVisible(quizTypeSelected);

        MenuItem changeQuizType = menu.findItem(R.id.change_quiz_type);
        changeQuizType.setVisible(quizTypeSelected);

        this.menu = menu;

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.grade_quiz:
                int correct = quizFragment.gradeQuiz();
                int totalQuestion = quizFragment.getQuestionCount();
                String score = String.format(getString(R.string.quiz_score), correct, totalQuestion);
                if( getSupportActionBar() != null )
                {
                    getSupportActionBar().setSubtitle(score);
                }
                item.setVisible(false);
                menu.findItem(R.id.reset_quiz).setVisible(true);
                break;
            case R.id.reset_quiz:
                item.setVisible(false);
                if( getSupportActionBar() != null )
                {
                    getSupportActionBar().setSubtitle("");
                }
                menu.findItem(R.id.grade_quiz).setVisible(true);
                quizFragment.resetQuiz();
                break;
            case R.id.change_quiz_type:
                quizTypeSelected = false;
                chooseQuizFragment = new ChooseQuizFragment();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.quiz_container, chooseQuizFragment).commit();
                invalidateOptionsMenu();
                break;
        }
        return true;
    }

    @Override
    public void quizModeChosen(ChooseQuizFragment.QuizMode mode, int maxQuestionCount)
    {
        quizTypeSelected = true;
        switch(mode)
        {
            case MATCHING:
                quizFragment = new MatchingQuizFragment();
                break;
            case FILL_IN_THE_BLANK:
                quizFragment = new FillInTheBlankQuizFragment();
                break;
            default:
            case MULTIPLE_CHOICE:
                quizFragment = new MultipleChoiceQuizFragment();
                break;
        }

        Bundle arguments = new Bundle();
        if( studyId != null )
        {
            arguments.putString(getString(R.string.study_id), String.valueOf(studyId));
        }
        arguments.putInt(getString(R.string.quiz_question_count_arg), maxQuestionCount);
        quizFragment.setArguments(arguments);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.quiz_container, quizFragment).commit();

        invalidateOptionsMenu();
    }
}

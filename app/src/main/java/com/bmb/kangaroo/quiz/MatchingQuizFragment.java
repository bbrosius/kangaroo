package com.bmb.kangaroo.quiz;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.bmb.graphics.utils.GraphicUtils;
import com.bmb.kangaroo.R;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MatchingQuizFragment extends QuizFragment
{
    private final static String ANSWER_BANK = "answerBank";
    private Cursor flashcards;
    private FlashcardDAO flashcardDao;
    private Id studyId;
    private boolean lockQuiz;
    private LinearLayout quizForm;
    private GridView answerBank;
    private View.OnDragListener dragListener;
    private View draggedView;
    private AnswerGridAdapter answerAdapter;
    private int questionCount = 20;

    private final ArrayList<Integer> answerUsed = new ArrayList<>();

    public MatchingQuizFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if( getArguments() != null && getArguments().containsKey(getString(R.string.study_id)) )
        {
            studyId = IdService.resolveId(getArguments().getString(getString(R.string.study_id)));
        }

        if( getArguments() != null && getArguments().containsKey(getString(R.string.quiz_question_count_arg)) )
        {
            questionCount = getArguments().getInt(getString(R.string.quiz_question_count_arg));
        }

        flashcardDao = new FlashcardDAO();
        flashcardDao.open();

        if( studyId != null )
        {
            flashcards = flashcardDao.findFlashcards(studyId, true);
        }

        if( flashcards != null )
        {
            if( flashcards.getCount() < questionCount )
            {
                questionCount = flashcards.getCount();
            }

            flashcards.moveToFirst();
        }
        dragListener = new answerDragEventListener();
        lockQuiz = false;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        flashcardDao.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        LinearLayout quizLayout = (LinearLayout) inflater.inflate(R.layout.quiz_container_layout, container, false);

        createView(inflater, quizLayout);
        return quizLayout;
    }

    private void createView(LayoutInflater inflater, LinearLayout quizLayout)
    {
        quizForm = (LinearLayout) quizLayout.findViewById(R.id.quiz_form);
        LinearLayout quizHeader = (LinearLayout) quizLayout.findViewById(R.id.quiz_header);
        quizHeader.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 400));

        answerBank = new GridView(getActivity());
        answerBank.setLayoutParams(new GridView.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, GridLayout.LayoutParams.MATCH_PARENT));
        answerBank.setNumColumns(GridView.AUTO_FIT);
        answerBank.setColumnWidth(GridView.AUTO_FIT);
        answerBank.setVerticalSpacing(40);
        answerBank.setHorizontalSpacing(80);
        answerBank.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        answerBank.setGravity(Gravity.CENTER);
        answerBank.setOnDragListener(dragListener);
        answerBank.setTag(ANSWER_BANK);
        quizHeader.addView(answerBank);
        List<String> answerList = new ArrayList<>();

        for(int i = 0; i < questionCount; i++)
        {
            String question = "";
            if( flashcards != null )
            {
                flashcards.moveToPosition(i);
                String correctAnswer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                answerList.add(correctAnswer);

                question = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                question = (i + 1) + ") " + question;
            }
            final LinearLayout quizQuestion = (LinearLayout) inflater.inflate(R.layout.matching_question, null);

            quizQuestion.setOnDragListener(dragListener);
            TextView matchingQuestion = (TextView) quizQuestion.findViewById(R.id.matching_question);
            matchingQuestion.setText(question);

            CardView matchingAnswer = (CardView) quizQuestion.findViewById(R.id.matching_answer);
            matchingAnswer.setVisibility(View.INVISIBLE);

            quizForm.addView(quizQuestion);
        }

        long seed = System.nanoTime();
        Collections.shuffle(answerList, new Random(seed));

        answerAdapter = new AnswerGridAdapter(getActivity(), answerList);
        answerBank.setAdapter(answerAdapter);
    }

    @Override
    public int gradeQuiz()
    {
        int correct = 0;
        for( int i = 0; i < questionCount; i++)
        {
            String correctAnswer = "";
            if( flashcards != null )
            {
                flashcards.moveToPosition(i);
                correctAnswer = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
            }

            LinearLayout quizQuestion = (LinearLayout) quizForm.getChildAt(i);
            CardView quizAnswer = (CardView) quizQuestion.findViewById(R.id.matching_answer);
            TextView answerView = (TextView) quizAnswer.findViewById(R.id.answer_text);
            ImageView answerImageView = (ImageView) quizAnswer.findViewById(R.id.answer_image);
            String answer = "";
            if( answerView.getText() != null )
            {
                answer = answerView.getText().toString();
            }

            if(correctAnswer.equalsIgnoreCase(answer))
            {
                quizAnswer.setBackgroundColor(Color.GREEN);
                answerImageView.setBackgroundColor(Color.GREEN);
                correct++;
            }
            else
            {
                if( isImage(answer) && !isImage(correctAnswer) )
                {
                    quizAnswer.setBackgroundColor(Color.RED);
                    answerImageView.setVisibility(View.GONE);
                    answerView.setText(correctAnswer);
                    answerView.setVisibility(View.VISIBLE);
                }
                else if( (!isImage(answer) && isImage(correctAnswer))
                        || (isImage(answer) && isImage(correctAnswer)) )
                {
                    quizAnswer.setBackgroundColor(Color.RED);
                    answerView.setVisibility(View.GONE);
                    GraphicUtils.loadBitmap(Uri.parse(correctAnswer), 150, 150, getActivity(), answerImageView);
                    answerImageView.setBackgroundColor(Color.RED);
                    answerImageView.setVisibility(View.VISIBLE);
                }
                else
                {
                    quizAnswer.setBackgroundColor(Color.RED);
                    answerImageView.setVisibility(View.GONE);
                    answerView.setText(correctAnswer);
                    answerView.setVisibility(View.VISIBLE);
                }
            }
        }
        lockQuiz = true;

        return correct;
    }

    @Override
    public int getQuestionCount()
    {
        return questionCount;
    }

    @Override
    public void resetQuiz()
    {
        lockQuiz = false;
        for( int i = 0; i < quizForm.getChildCount(); i++ )
        {
            LinearLayout question = (LinearLayout) quizForm.getChildAt(i);
            if( question != null )
            {
                CardView answer = (CardView) question.findViewById(R.id.matching_answer);
                answer.setVisibility(View.INVISIBLE);
                TextView answerText = (TextView) answer.findViewById(R.id.answer_text);
                if( answerText != null )
                {
                    answerText.setText("");
                    answerText.setTextColor(Color.BLACK);
                    answerText.setVisibility(View.VISIBLE);
                }

                ImageView answerImage = (ImageView) answer.findViewById(R.id.answer_image);
                answerImage.setImageDrawable(null);
                answerImage.setVisibility(View.GONE);
                answerImage.setBackgroundColor(getResources().getColor(R.color.transparent_color));
            }
        }

        answerUsed.clear();
        for( int i = 0; i < answerBank.getChildCount(); i++ )
        {
            answerBank.getChildAt(i).setVisibility(View.VISIBLE);
        }
    }

    private class answerDragEventListener implements View.OnDragListener
    {

        // This is the method that the system calls when it dispatches a drag event to the
        // listener.
        public boolean onDrag(View v, DragEvent event)
        {

            // Defines a variable to store the action type for the incoming event
            final int action = event.getAction();
            if( v.getTag() != null && v.getTag().equals("answerBank") )
            {
                if( action == DragEvent.ACTION_DROP )
                {
                    draggedView.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                // Handles each of the expected events
                switch(action)
                {
                    case DragEvent.ACTION_DRAG_STARTED:
                        return(true);

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a green tint to the View. Return true; the return value is ignored.
                        v.setBackgroundColor(getResources().getColor(R.color.blue_highlight));
                            // Invalidate the view to force a redraw in the new tint
                            v.invalidate();


                        return(true);

                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event
                        return(true);

                    case DragEvent.ACTION_DRAG_EXITED:
                        // Invalidate the view to force a redraw in the new tint
                        v.setBackgroundColor(getResources().getColor(R.color.transparent_color));
                        v.invalidate();
                        return(true);

                    case DragEvent.ACTION_DROP:

                        // Gets the item containing the dragged data
                        ClipData.Item item = event.getClipData().getItemAt(0);

                        CardView answerView = (CardView) v.findViewById(R.id.matching_answer);
                        answerView.setVisibility(View.VISIBLE);
                        TextView answerTextView = (TextView) answerView.findViewById(R.id.answer_text);
                        answerTextView.setVisibility(View.VISIBLE);
                        ImageView answerImage = (ImageView) answerView.findViewById(R.id.answer_image);
                        answerImage.setVisibility(View.GONE);
                        String oldAnswer = answerTextView.getText().toString();
                        answerUsed.add(answerAdapter.getPosition(item.getText().toString()));
                        answerTextView.setText(item.getText());

                        if( isImage(item.getText().toString()) )
                        {
                            answerTextView.setVisibility(View.GONE);
                            GraphicUtils.loadBitmap(Uri.parse(item.getText().toString()), 150, 150, getActivity(), answerImage);
                            answerImage.setVisibility(View.VISIBLE);
                        }

                        v.setBackgroundColor(getResources().getColor(R.color.transparent_color));

                        // Invalidates the view to force a redraw
                        v.invalidate();

                        if( !oldAnswer.isEmpty() )
                        {
                            makeOldAnswerVisible(oldAnswer);
                        }
                        // Returns true. DragEvent.getResult() will return true.
                        return(true);

                    case DragEvent.ACTION_DRAG_ENDED:
                        // Invalidates the view to force a redraw
                        v.invalidate();

                        if( !event.getResult() )
                        {
                            View dragView = (View) event.getLocalState();
                            dragView.setVisibility(View.VISIBLE);
                        }
                        // returns true; the value is ignored.
                        return(true);

                    // An unknown action type was received.
                    default:
                        Log.e(getString(R.string.log_tag), "Unknown action type received by OnDragListener.");
                        break;
                }
            }
            return true;
        }
    }

    private void makeOldAnswerVisible(String answer)
    {
        int position = answerAdapter.getPosition(answer);
        if( position != -1 )
        {
            answerUsed.remove((Integer) position);
            View answerView = answerBank.getChildAt(position);
            if( answerView != null )
            {
                answerView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class AnswerGridAdapter extends ArrayAdapter<String>
    {
        final Context context;
        List<String> answers = null;

        public AnswerGridAdapter(Context context, List<String> answers)
        {
            super(context, R.layout.matching_item, answers);
            this.context = context;
            this.answers = answers;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            AnswerHolder holder;

            if(row == null)
            {
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                row = inflater.inflate(R.layout.matching_item, parent, false);

                holder = new AnswerHolder();
                holder.answerText = (TextView)row.findViewById(R.id.answer_text);
                holder.answerText.setTag(position);
                holder.answerImage = (ImageView) row.findViewById(R.id.answer_image);
                holder.answerText.setTag(position);

                row.setTag(holder);
            }
            else
            {
                holder = (AnswerHolder)row.getTag();
                ((AnswerHolder) row.getTag()).answerText.setTag(position);
            }

            String answer = answers.get(position);
            if( isImage(answer) )
            {
                holder.answerText.setVisibility(View.GONE);
                holder.answerText.setText(answer);
                holder.answerImage.setVisibility(View.VISIBLE);
                GraphicUtils.loadBitmap(Uri.parse(answer), 200, 200, getActivity(), holder.answerImage);
            }
            else
            {
                holder.answerText.setText(answer);
                holder.answerImage.setVisibility(View.GONE);
                holder.answerText.setVisibility(View.VISIBLE);
            }

            final int answerPosition = (Integer) holder.answerText.getTag();
            if( answerUsed.contains(answerPosition) )
            {
                row.setVisibility(View.INVISIBLE);
            }
            else
            {
                row.setVisibility(View.VISIBLE);
            }

            row.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    if (event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        if( !lockQuiz )
                        {
                            ClipData data = ClipData.newPlainText(String.valueOf(position), answers.get(position));
                            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                            v.startDrag(data, shadowBuilder, v, 0);
                            v.setVisibility(View.INVISIBLE);
                            draggedView = v;
                            // Starts the drag
                            v.startDrag(data,  // the data to be dragged
                                    shadowBuilder,  // the drag shadow builder
                                    null,      // no need to use local data
                                    0          // flags (not currently used, set to 0)
                            );
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            });

            return row;
        }

        private class AnswerHolder
        {
            TextView answerText;
            ImageView answerImage;
        }
    }

    private boolean isImage(String flashcardTerm)
    {
        return flashcardTerm.startsWith("content:") || flashcardTerm.startsWith("file:");
    }
}

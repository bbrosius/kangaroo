package com.bmb.kangaroo.quiz;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.bmb.kangaroo.R;

public class ChooseQuizFragment extends Fragment
{
    public enum QuizMode {
        MULTIPLE_CHOICE,
        MATCHING,
        FILL_IN_THE_BLANK,
    }

    private QuizModeChosen modeChosen = dummyResultHandler;

    public ChooseQuizFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
        final LinearLayout chooseQuizLayout = (LinearLayout) inflater.inflate(R.layout.choose_quiz_fragment, null);

        final EditText questionCount = (EditText) chooseQuizLayout.findViewById(R.id.question_count_entry);

        Button multipleChoiceButton = (Button) chooseQuizLayout.findViewById(R.id.choose_multiple_choice_quiz);
        multipleChoiceButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int maxQuestionCount = QuizActivity.MAX_QUESTION_COUNT;
                String countString = questionCount.getText().toString();
                if( !countString.isEmpty() )
                {
                    maxQuestionCount = Integer.parseInt(countString);
                }
                modeChosen.quizModeChosen(QuizMode.MULTIPLE_CHOICE, maxQuestionCount);
            }
        });

        Button matchingButton = (Button) chooseQuizLayout.findViewById(R.id.choose_matching_quiz);
        matchingButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int maxQuestionCount = QuizActivity.MAX_QUESTION_COUNT;
                String countString = questionCount.getText().toString();
                if( !countString.isEmpty() )
                {
                    maxQuestionCount = Integer.parseInt(countString);
                }
                modeChosen.quizModeChosen(QuizMode.MATCHING, maxQuestionCount);
            }
        });

        Button fillInTheBlank = (Button) chooseQuizLayout.findViewById(R.id.choose_fill_in_the_blank_quiz);
        fillInTheBlank.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int maxQuestionCount = QuizActivity.MAX_QUESTION_COUNT;
                String countString = questionCount.getText().toString();
                if( !countString.isEmpty() )
                {
                    maxQuestionCount = Integer.parseInt(countString);
                }
                modeChosen.quizModeChosen(QuizMode.FILL_IN_THE_BLANK, maxQuestionCount);
            }
        });

        //Fill in the blank won't be included in this release.
        fillInTheBlank.setVisibility(View.GONE);

        return chooseQuizLayout;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (!(activity instanceof QuizModeChosen))
        {
            throw new IllegalStateException(
                    "Activity must implement quizlet fragment callbacks.");
        }

        modeChosen = (QuizModeChosen) activity;
    }

    private final static QuizModeChosen dummyResultHandler = new QuizModeChosen()
    {
        @Override
        public void quizModeChosen(QuizMode mode, int questionCount)
        {

        }
    };

    public interface QuizModeChosen {
        void quizModeChosen(QuizMode mode, int questionCount);
    }
}
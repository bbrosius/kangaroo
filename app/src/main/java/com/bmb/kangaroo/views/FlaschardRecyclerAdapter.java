package com.bmb.kangaroo.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bmb.kangaroo.FlashcardStackFragment;
import com.bmb.kangaroo.R;
import com.bmb.utils.Utils;

import java.util.List;

public class FlaschardRecyclerAdapter extends
        RecyclerView.Adapter<FlaschardRecyclerAdapter.ViewHolder> {

    private Context context;
    //private Cursor flashcards;
    private List<FlashcardStackFragment.Flashcard> flashcards;
    public FlaschardRecyclerAdapter(Context context, List<FlashcardStackFragment.Flashcard> flashcards)
    {
        this.context = context;
        this.flashcards = flashcards;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        TextView flashcardFrontTitle;
        TextView flashcardBackTitle;
        TextView flashcardBackDefinition;
        ViewFlipper flashcardView;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            super(itemView);
            this.flashcardView = (ViewFlipper) itemView.findViewById(R.id.flashcard_flipper);
            this.flashcardFrontTitle = (TextView) itemView.findViewById(R.id.flashcard_front_term);
            this.flashcardBackTitle = (TextView) itemView.findViewById(R.id.flashcard_back_term);
            this.flashcardBackDefinition = (TextView) itemView.findViewById(R.id.flashcard_back_defintion);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            flashcardView.setInAnimation(Utils.getContext(), R.anim.grow_from_middle);
            flashcardView.setOutAnimation(Utils.getContext(), R.anim.shrink_to_middle);
            if (flashcardView.getDisplayedChild()==0)
            {

                flashcardView.showNext();
            }
            else
            {
                flashcardView.showPrevious();
            }
        }
    }

    // ... constructor and member variables

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public FlaschardRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate the custom layout
        View itemView = LayoutInflater.from(context).
                inflate(R.layout.flashcard_view_flipper, parent, false);
        // Return a new holder instance
        return new ViewHolder(itemView);
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(FlaschardRecyclerAdapter.ViewHolder holder, int position) {
        //String term = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
        //String definition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
        String term = flashcards.get(position).getTerm();
        String definition = flashcards.get(position).getDefintion();

        holder.flashcardFrontTitle.setText(term);
        holder.flashcardBackTitle.setText(term);
        holder.flashcardBackDefinition.setText(definition);
    }



    // Return the total count of items
    @Override
    public int getItemCount() {
        //return flashcards.getCount();
        return flashcards.size();
    }
}

package com.bmb.kangaroo.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.support.v7.widget.CardView;

public class NotePaperLayout extends CardView
{
    private Paint leftLinePaint;

    public NotePaperLayout(Context context)
    {
        super(context);
        leftLinePaint = new Paint();
        leftLinePaint.setColor(Color.RED);
        leftLinePaint.setStyle(Paint.Style.STROKE);
    }

    public NotePaperLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        leftLinePaint = new Paint();
        leftLinePaint.setColor(Color.RED);
        leftLinePaint.setStyle(Paint.Style.STROKE);
    }

    public NotePaperLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        leftLinePaint = new Paint();
        leftLinePaint.setColor(Color.RED);
        leftLinePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        canvas.drawLine(20, getHeight(), 20, 0, leftLinePaint);
        canvas.drawLine(25, getHeight(), 25, 0, leftLinePaint);

        super.onDraw(canvas);
    }
}

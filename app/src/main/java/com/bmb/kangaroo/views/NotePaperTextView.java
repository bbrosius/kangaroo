package com.bmb.kangaroo.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.widget.TextView;

public class NotePaperTextView extends TextView
{
    private Paint linePaint;
    private Rect bounds;
    public NotePaperTextView(Context context)
    {
        super(context);
        linePaint = new Paint();
        linePaint.setColor(Color.BLUE);
        linePaint.setStyle(Paint.Style.STROKE);
        bounds = new Rect();
    }

    public NotePaperTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        linePaint = new Paint();
        linePaint.setColor(Color.BLUE);
        linePaint.setStyle(Paint.Style.STROKE);
        bounds = new Rect();
    }

    public NotePaperTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        linePaint = new Paint();
        linePaint.setColor(Color.BLUE);
        linePaint.setStyle(Paint.Style.STROKE);
        bounds = new Rect();
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas)
    {
        int firstLineY = getLineBounds(0, bounds) + 3;
        int lineHeight = getLineHeight();
        int totalLines = Math.max(getLineCount(), getHeight() / lineHeight);

        for( int i = 0; i < totalLines; i++ )
        {
            int lineY = firstLineY + i * lineHeight;
            canvas.drawLine(0, lineY, bounds.right, lineY, linePaint);
        }

        setTextColor(Color.BLACK);
        super.onDraw(canvas);
    }
}
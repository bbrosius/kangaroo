package com.bmb.kangaroo.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

public class NoteCardLayout extends CardView
{
    private int color = Color.WHITE;
    private final static Paint linePaint;
    private final static Paint topLinePaint;

    static
    {
        linePaint = new Paint();
        linePaint.setColor(Color.BLUE);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setAlpha(100);

        topLinePaint = new Paint();
        topLinePaint.setColor(Color.RED);
        topLinePaint.setStyle(Paint.Style.STROKE);
    }

    public NoteCardLayout(Context context)
    {
        super(context);
    }

    public NoteCardLayout(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public NoteCardLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public void setCardColor(int color)
    {
        this.color = color;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        //Draw the background color
        canvas.drawColor(color);

        int firstLineY = 60;
        int lineHeight = 28;
        int totalLines = getHeight() / lineHeight;

        canvas.drawLine(0, firstLineY, getWidth(), firstLineY, topLinePaint);

        for( int i = 1; i < totalLines; i++ )
        {
            int lineY = firstLineY + i * lineHeight;
            canvas.drawLine(0, lineY, getWidth(), lineY, linePaint);
        }

        super.onDraw(canvas);
    }
}

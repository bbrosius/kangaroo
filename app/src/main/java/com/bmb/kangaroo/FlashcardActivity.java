package com.bmb.kangaroo;


import android.Manifest;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.bmb.kangaroo.dialogs.AddFlashcardDialog;
import com.bmb.kangaroo.model.Exam;
import com.bmb.kangaroo.model.FlashcardSet;
import com.bmb.kangaroo.model.builder.FlashcardSetJSONBuilder;
import com.bmb.kangaroo.model.services.FlashcardSetService;
import com.bmb.kangaroo.model.services.SubjectService;
import com.bmb.kangaroo.quizlet.QuizletActivity;
import com.bmb.kangaroo.sql.FlashcardDAO;
import com.bmb.kangaroo.sql.FlashcardOpenHelper;
import com.bmb.kangaroo.utils.ExamIdType;
import com.bmb.kangaroo.utils.FlashcardSetIdType;
import com.bmb.kangaroo.utils.Id;
import com.bmb.kangaroo.utils.IdService;
import com.bmb.kangaroo.utils.SubjectIdType;
import com.bmb.kangaroo.views.NotePaperLayout;
import com.bmb.kangaroo.views.NotePaperTextView;
import com.bmb.utils.Utils;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FlashcardActivity extends BaseActivity implements RecognitionListener, FlashcardFragment.FlashcardActionCallback, PopupMenu.OnMenuItemClickListener,
        ViewPager.OnPageChangeListener, TextToSpeech.OnInitListener, ActivityCompat.OnRequestPermissionsResultCallback
{
	private FlashcardPagerAdapter flashcardPageAdapter;
	private ViewPager flashcardPager;

    public static final String SUBJECT_LIST = "subject_list";

    private Cursor flashcards;

	private FlashcardDAO flashcardDao;
	
    private boolean showOnlyIncorrect = false;
    private boolean shuffle = false;
	
	private boolean studyingSubject = true;
    private boolean studyingExam = false;
    private boolean studyingFlashcardSet = false;

	private Id studyId;
    private String callingActivity = SUBJECT_LIST;

    private SpeechRecognizer recognizer;
    private boolean isListening = false;
    private TextToSpeech tts;
    private boolean speechEnabled = false;

    private boolean showDefinition = false;

    private DrawerLayout mainLayout;

    private boolean setIsFromQuizlet = false;

    private NotePaperTextView flashcardSetTitle;
    private NotePaperTextView flashcardCount;
    private ImageView quizletLogo;

    private static final int REQUEST_MICROPHONE = 1;

    @Override
	public void onCreate(Bundle bundle)
	{
        super.onCreate(bundle);

        setContentView(R.layout.flashcard_activity_layout);

        mainLayout = (DrawerLayout) findViewById(R.id.flashcard_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.flashcard_toolbar);
		setSupportActionBar(toolbar);

        NotePaperLayout flashcardSetCard = (NotePaperLayout) mainLayout.findViewById(R.id.flashcard_set_title_card);
        flashcardSetCard.setVisibility(View.VISIBLE);
        flashcardSetTitle = (NotePaperTextView) mainLayout.findViewById(R.id.flashcard_set_title);
        flashcardCount = (NotePaperTextView) mainLayout.findViewById(R.id.flashcard_count);
        quizletLogo = (ImageView) mainLayout.findViewById(R.id.set_from_quizlet);
        flashcardDao = new FlashcardDAO();
		flashcardDao.open();

        if( getIntent().hasExtra(getString(R.string.flashcard_calling_activity)) )
        {
            callingActivity = getIntent().getStringExtra(getString(R.string.flashcard_calling_activity));
        }

        String studyIdExtra = "";
        if( getIntent().hasExtra(getString(R.string.study_id)) )
        {
            studyIdExtra = getIntent().getStringExtra(getString(R.string.study_id));
        }
		studyId = IdService.resolveId(studyIdExtra);
		if( studyId.getType() instanceof SubjectIdType)
		{
			studyingSubject = true;
            studyingExam = false;
            studyingFlashcardSet = false;
		}
		else if( studyId.getType() instanceof ExamIdType)
		{
			studyingSubject = false;
            studyingExam = true;
            studyingFlashcardSet = false;
		}
        else if( studyId.getType() instanceof FlashcardSetIdType)
        {
            studyingExam = false;
            studyingSubject = false;
            studyingFlashcardSet = true;
            setIsFromQuizlet = FlashcardSetService.getInstance().getFlashcardSet(studyId).isQuizletSet();
            if( setIsFromQuizlet )
            {
                quizletLogo.setVisibility(View.VISIBLE);
            }
            String title = FlashcardSetService.getInstance().getFlashcardSet(studyId).getTitle();
            flashcardSetTitle.setText(title);
            flashcardCount.setText(FlashcardSetService.getInstance().getFlashcardSet(studyId).getFlashcardCount() + " " + getString(R.string.flashcards));
        }

		flashcards = flashcardDao.findFlashcards(studyId, shuffle);

		Bundle arguments = new Bundle();
		arguments.putString(getString(R.string.study_id), getIntent()
				.getStringExtra(getString(R.string.study_id)));

        FragmentManager fragmentManager = getSupportFragmentManager();
		
		flashcardPager = (ViewPager) findViewById(R.id.flashcard_pager);
		flashcardPageAdapter = new FlashcardPagerAdapter(fragmentManager);
		flashcardPager.setAdapter(flashcardPageAdapter);
        flashcardPager.addOnPageChangeListener(this);

        if(flashcards.getCount() > 0)
        {
            flashcards.moveToFirst();
        }

        //Setup the actionbar
        //Set the nav menu icon on the tool bar
        if( toolbar != null )
        {
            toolbar.setNavigationOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    mainLayout.openDrawer(GravityCompat.START);
                }
            });
        }

        if( getSupportActionBar() != null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        recognizer = SpeechRecognizer.createSpeechRecognizer(this);
        recognizer.setRecognitionListener(this);

        tts = new TextToSpeech(this, this);

        //Setup the floating action menu
        final FloatingActionsMenu flashcardFloatingMenu = (FloatingActionsMenu) findViewById(R.id.flashcard_floating_menu);

        FloatingActionButton addNewFlashcardButton = (FloatingActionButton) findViewById(R.id.add_new_flashcard_button);
        addNewFlashcardButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Bundle arguments = new Bundle();

                if( flashcards.getCount() == 0 )
                {
                    Id subjectId = SubjectService.getInstance().getCurrentSubjectId();
                    arguments.putString(getString(R.string.study_subject_id), subjectId.toString());

                    Exam nextExam = SubjectService.getInstance().getCurrentSubject().getNextExam();
                    if( nextExam != null )
                    {
                        arguments.putString(getString(R.string.study_exam_id), nextExam.getId().toString());
                    }

                    //If it's empty we can assume the study id was the flashcard set id.
                    if( studyId != null )
                    {
                        arguments.putString(getString(R.string.flashcard_set_id), studyId.toString());
                    }
                }
                else
                {
                    flashcards.moveToPosition(flashcardPager.getCurrentItem());
                    Id subjectId = IdService.resolveId(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_SUBJECT_ID)));
                    if( subjectId != null )
                    {
                        arguments.putString(getString(R.string.study_subject_id), subjectId.toString());
                    }

                    Id flashcardSetId = IdService.resolveId(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID)));
                    if( flashcardSetId != null )
                    {
                        arguments.putString(getString(R.string.flashcard_set_id), flashcardSetId.toString());
                    }

                    Id examId = IdService.resolveId(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_EXAM_ID)));
                    if( examId != null )
                    {
                        arguments.putString(getString(R.string.study_exam_id), examId.toString());
                    }

                    Id noteId = IdService.resolveId(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_NOTE_ID)));
                    if( noteId != null )
                    {
                        arguments.putString(getString(R.string.study_note_id), noteId.toString());
                    }
                }

                android.app.DialogFragment dialog = new AddFlashcardDialog();
                dialog.setArguments(arguments);
                dialog.show(getFragmentManager(), getString(R.string.add_flashcard_dialog_tag));
                flashcardFloatingMenu.collapse();
            }
        });

        FloatingActionButton exportToQuizletButton = (FloatingActionButton) findViewById(R.id.export_to_quizlet_button);
        exportToQuizletButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Id flashcardSetId = IdService.resolveId(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID)));
                Intent quizletIntent = new Intent(FlashcardActivity.this, QuizletActivity.class);
                quizletIntent.putExtra(getString(R.string.quizlet_export_arg), flashcardSetId.toString());

                startActivity(quizletIntent);
                flashcardFloatingMenu.collapse();
            }
        });

        FloatingActionButton shareFlashcardsButton = (FloatingActionButton) findViewById(R.id.share_flashcards_button);
        shareFlashcardsButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String formattedFlashcards;
                if( flashcards.getCount() > 0 )
                {
                    flashcards.moveToPosition(flashcardPager.getCurrentItem());
                    String setId = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID));

                    FlashcardSet set = FlashcardSetService.getInstance().getFlashcardSet(IdService.resolveId(setId));
                    formattedFlashcards = FlashcardSetJSONBuilder.serializeFlashcardSet(set, true, FlashcardActivity.this).toString();

                    File flashcardSetFile = null;
                    try
                    {
                        File path = new File(getCacheDir(), "flashcards");
                        if( path.mkdirs() )
                        {
                            flashcardSetFile = new File(path, "flashcards.json");
                            flashcardSetFile.deleteOnExit();
                            FileWriter writer = new FileWriter(flashcardSetFile);
                            writer.write(formattedFlashcards);
                            writer.close();
                        }
                        else
                        {
                            Snackbar.make(findViewById(android.R.id.content), R.string.error_saving_flashcard_file, Snackbar.LENGTH_SHORT)
                                    .show();
                        }
                    }
                    catch( IOException e )
                    {
                        Log.e(getString(R.string.log_tag), "Error saving tmp flashcard file for sharing " + e.getMessage());
                    }

                    Intent shareNotesIntent = new Intent(Intent.ACTION_SEND);
                    shareNotesIntent.putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(getApplicationContext(), "com.bmb.kangaroo.FlashcardActivity", flashcardSetFile));
                    shareNotesIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    shareNotesIntent.setType("application/json");
                    startActivity(shareNotesIntent);
                }
                flashcardFloatingMenu.collapse();
            }
        });
	}

    @Override
    public DrawerLayout getDrawerLayout()
    {
        return mainLayout;
    }

    @Override
	public void onDestroy()
	{
		super.onDestroy();
		flashcardDao.close();

        if (tts != null)
        {
            tts.stop();
            tts.shutdown();
        }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.study_menu, menu);

	    MenuItem studySubject = menu.findItem(R.id.study_entire_subject_menu_item);
        MenuItem studyFlashcardSet = menu.findItem(R.id.study_flashcard_set_menu_item);
        MenuItem studyExam = menu.findItem(R.id.study_next_exam_menu_item);

        if( studySubject != null && studyExam != null && studyFlashcardSet != null )
        {
            if( studyingFlashcardSet )
            {
                studySubject.setVisible(true);
                studyExam.setVisible(true);
                studyFlashcardSet.setVisible(false);
            }
            else if( studyingSubject )
            {
                studySubject.setVisible(false);
                studyExam.setVisible(true);
                studyFlashcardSet.setVisible(true);
            }
            else if( studyingExam )
            {
                studySubject.setVisible(true);
                studyExam.setVisible(false);
                studyFlashcardSet.setVisible(true);
            }
        }

        if ( flashcards != null && flashcards.getCount() > 0 )
        {
            String flashcardSubjectId = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_SUBJECT_ID));

            if( SubjectService.getInstance().getSubject(IdService.resolveId(flashcardSubjectId)).getNextExam() == null )
            {
                if( studyExam != null )
                {
                    studyExam.setVisible(false);
                }
            }
        }

        MenuItem toggleDefinition = menu.findItem(R.id.toggle_term_menu_item);
        if( toggleDefinition != null )
        {
            if( showDefinition )
            {
                toggleDefinition.setTitle(R.string.show_term);
            }
            else
            {
                toggleDefinition.setTitle(R.string.show_definition);
            }
        }

		return true;		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId()) 
	    {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                if(callingActivity.equalsIgnoreCase(SUBJECT_LIST))
                {
                    NavUtils.navigateUpTo(this, new Intent(this,
                            MainFlashcardSetListActivity.class));
                }
                return true;

            case R.id.study_entire_subject_menu_item:
                //Get the subject id for the current viewed flashcard
                flashcards.moveToPosition(flashcardPager.getCurrentItem());
                String flashcardSubjectId = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_SUBJECT_ID));

                //Update the study id to be the subject id for the currently viewed flashcard
                studyId = IdService.resolveId(flashcardSubjectId);
                flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                flashcardPageAdapter.notifyDataSetChanged();
                flashcardPager.postInvalidate();

                //Set the correct flags and redraw the menu
                studyingExam = false;
                studyingSubject = true;
                studyingFlashcardSet = false;
                invalidateOptionsMenu();

                return true;

            case R.id.study_flashcard_set_menu_item:
                //Get the flashcard set id for the current viewed flashcard
                flashcards.moveToPosition(flashcardPager.getCurrentItem());
                String newId = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID));

                //Update the study id to be the subject id for the currently viewed flashcard
                studyId = IdService.resolveId(newId);
                flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                flashcardPageAdapter.notifyDataSetChanged();
                flashcardPager.postInvalidate();

                //Set the correct flags and redraw the menu
                studyingExam = false;
                studyingSubject = false;
                studyingFlashcardSet = true;
                invalidateOptionsMenu();

                return true;

            case R.id.study_next_exam_menu_item:
                //Get the exam id for the current viewed flashcard
                flashcards.moveToPosition(flashcardPager.getCurrentItem());
                String flashcardExamId = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_EXAM_ID));

                //Update the study id to be the subject id for the currently viewed flashcard
                studyId = IdService.resolveId(flashcardExamId);
                flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                flashcardPageAdapter.notifyDataSetChanged();
                flashcardPager.postInvalidate();

                //Set the correct flags and redraw the menu
                studyingExam = true;
                studyingSubject = false;
                studyingFlashcardSet = false;
                invalidateOptionsMenu();

                return true;

            case R.id.clear_all_incorrect_menu_item:
                flashcards.moveToFirst();
                for(int i = 0; i < flashcards.getCount(); i++)
                {
                    flashcards.moveToPosition(i);
                    flashcardDao.updateFlashcardCorrectness(-1, flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID)));
                }

                flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                flashcardPageAdapter.notifyDataSetChanged();
                flashcardPager.postInvalidate();
                return true;

            case R.id.show_incorrect_menu_item:
                showOnlyIncorrect = !showOnlyIncorrect;
                if( showOnlyIncorrect )
                {
                    flashcards = flashcardDao.findIncorrectFlashcards(studyId, shuffle);
                    flashcardPageAdapter.notifyDataSetChanged();
                    flashcardPager.postInvalidate();
                    item.setTitle(R.string.show_all_flashcards);
                }
                else
                {
                    flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                    flashcardPageAdapter.notifyDataSetChanged();
                    flashcardPager.postInvalidate();
                    item.setTitle(R.string.show_only_incorrect);
                }
                return true;
            case R.id.edit_flashcard_menu_item:
                Bundle arguments = new Bundle();
                flashcards.moveToPosition(flashcardPager.getCurrentItem());
                arguments.putString(getString(R.string.flashcard_term_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
                arguments.putString(getString(R.string.flashcard_definition_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION)));
                arguments.putLong(getString(R.string.flashcard_id_argument), flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID)));

                DialogFragment dialog = new AddFlashcardDialog();
                dialog.setArguments(arguments);
                dialog.show(getFragmentManager(), getString(R.string.add_flashcard_dialog_tag));

                return true;
            case R.id.delete_flashcard_menu_item:
                Log.e(getString(R.string.log_tag), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
                if( !flashcardDao.deleteFlashcard(flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID))) )
                {
                    Log.e(getString(R.string.log_tag), getString(R.string.flashcard_delete_error));
                }
                flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                flashcardPageAdapter.notifyDataSetChanged();
                flashcardPager.postInvalidate();
                return true;
            case R.id.shuffle_flashcards_menu_item:
                shuffle = !shuffle;
                if( showOnlyIncorrect )
                {
                    flashcards = flashcardDao.findIncorrectFlashcards(studyId, shuffle);
                    flashcardPageAdapter.notifyDataSetChanged();
                    flashcardPager.postInvalidate();
                }
                else
                {
                    flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                    flashcardPageAdapter.notifyDataSetChanged();
                    flashcardPager.postInvalidate();
                }

                if( shuffle )
                {
                    item.setTitle(R.string.show_in_order);
                }
                else
                {
                    item.setTitle(R.string.shuffle_flashcards);
                }

                return true;
            case R.id.toggle_term_menu_item:
                showDefinition = !showDefinition;
                if( showDefinition )
                {
                    item.setTitle(R.string.show_term);
                }
                else
                {
                    item.setTitle(R.string.show_definition);
                }

                flashcardPageAdapter.notifyDataSetChanged();
                flashcardPager.postInvalidate();
                return true;
	    }
		return true;
	}
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		if (flashcardPager.getCurrentItem() == 0) 
		{
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
            mainLayout.findViewById(R.id.delete_set).setVisibility(View.VISIBLE);
        }
		else 
        {
            // Otherwise, select the previous step.
            flashcardPager.setCurrentItem(flashcardPager.getCurrentItem() - 1);
        }
	}

    public void flashcardChanged()
    {
        flashcards = flashcardDao.findFlashcards(studyId, shuffle);
        flashcardCount.setText(FlashcardSetService.getInstance().getFlashcardSet(studyId).getFlashcardCount() + " " + getString(R.string.flashcards));

        flashcardPageAdapter.notifyDataSetChanged();
        flashcardPager.postInvalidate();
    }

    @Override
    public void onPageScrolled(int i, float v, int i2)
    {

    }

    @Override
    public void onPageSelected(int i)
    {
        flashcards.moveToPosition(i);
        Id flashcardSetId = IdService.resolveId(flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_FLASHCARD_SET_ID)));
        FlashcardSet set = FlashcardSetService.getInstance().getFlashcardSet(flashcardSetId);
        String title = set.getTitle();
        flashcardSetTitle.setText(title);
        flashcardCount.setText(FlashcardSetService.getInstance().getFlashcardSet(flashcardSetId).getFlashcardCount() + " " + getString(R.string.flashcards));

        if( set.isQuizletSet() )
        {
            quizletLogo.setVisibility(View.VISIBLE);
        }
        else
        {
            quizletLogo.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onPageScrollStateChanged(int i)
    {

    }

    @Override
    public void onInit(int status)
    {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result != TextToSpeech.LANG_MISSING_DATA
                    && result != TextToSpeech.LANG_NOT_SUPPORTED)
            {
                speechEnabled = true;
            }

        } else {
            speechEnabled = false;
        }
    }

    private class FlashcardPagerAdapter extends FragmentStatePagerAdapter
	{

		public FlashcardPagerAdapter(FragmentManager fm) 
		{
			super(fm);
		}

		@Override
		public int getCount() 
		{
			int count = -1;
			if( flashcards != null )
			{
				count = flashcards.getCount();
			}
			return count;
		}

        @Override
        public float getPageWidth(int position)
        {
            return .9f;
        }


        @Override
		public Fragment getItem(int position)
		{			
			if( flashcards != null )
			{
				if( !flashcards.moveToPosition(position) )
				{
                    flashcards.moveToFirst();
                }

                Bundle arguments = new Bundle();
                String term = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM));
                String definition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));
                Boolean isImage = flashcards.getInt(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_IS_PICTURE)) == 1;
                if( showDefinition )
                {
                    arguments.putString(getString(R.string.flashcard_term_argument),
                            definition);
                    arguments.putString(getString(R.string.flashcard_definition_argument),
                            term);
                }
                else
                {
                    arguments.putString(getString(R.string.flashcard_term_argument),
                            term);
                    arguments.putString(getString(R.string.flashcard_definition_argument),
                            definition);
                }

                arguments.putBoolean(getString(R.string.flashcard_has_image_argument), isImage);
                arguments.putBoolean(getString(R.string.flashcard_front_argument), true);
                arguments.putInt(getString(R.string.flashcard_correct_argument),
                        flashcards.getInt(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_CORRECT)));
                arguments.putBoolean(getString(R.string.from_quizlet_argument), setIsFromQuizlet);

                FlashcardFragment fragment = new FlashcardFragment();
                fragment.setArguments(arguments);
                return fragment;
            }
			else
			{
				return new FlashcardFragment();
			}
				
		}

		@Override
		public int getItemPosition(Object object) 
		{
			return POSITION_NONE;
		}		
	}

    @Override
    public void flashcardCorrectnessChanged(int correctness)
    {
        flashcards.moveToPosition(flashcardPager.getCurrentItem());
        flashcardDao.updateFlashcardCorrectness(correctness, flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID)));
    }

    public void showFlashcardMenu(View v)
    {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.flashcard_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    public void voiceAnswer(View v)
    {
        if ( ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED )
        {
            requestMicrophonePermission();
        }
        else
        {
            listenForAnswer();
        }
    }

    private void listenForAnswer()
    {
        if( !isListening )
        {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "com.bmb.kangaroo");
            intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
            recognizer.startListening(intent);
            isListening = true;
        }
        else
        {
            recognizer.stopListening();
            isListening = false;
        }
    }

    /**
     * Requests the microphone permission for listening to answer.
     */
    private void requestMicrophonePermission() {
        Log.i(getString(R.string.log_tag), "Requesting storage permission");

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 )
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_MICROPHONE);
        }
    }

    //Handles request to get the storage permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_MICROPHONE)
        {
            //Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                listenForAnswer();
            }
        }
        else
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        switch( item.getItemId() )
        {
            case R.id.edit_flashcard_menu_item:
                Bundle arguments = new Bundle();
                flashcards.moveToPosition(flashcardPager.getCurrentItem());
                arguments.putString(getString(R.string.flashcard_term_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
                arguments.putString(getString(R.string.flashcard_definition_argument), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION)));
                arguments.putLong(getString(R.string.flashcard_id_argument), flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID)));

                android.app.DialogFragment dialog = new AddFlashcardDialog();
                dialog.setArguments(arguments);
                dialog.show(getFragmentManager(), getString(R.string.add_flashcard_dialog_tag));
                return true;
            case R.id.delete_flashcard_menu_item:
                flashcards.moveToPosition(flashcardPager.getCurrentItem());
                Log.e(getString(R.string.log_tag), flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_TERM)));
                if( !flashcardDao.deleteFlashcard(flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID))) )
                {
                    Log.e(getString(R.string.log_tag), getString(R.string.flashcard_delete_error));
                }
                flashcards = flashcardDao.findFlashcards(studyId, shuffle);
                flashcardPageAdapter.notifyDataSetChanged();
                flashcardPager.postInvalidate();
                return true;
        }
        return true;
    }

    @Override
    public void onReadyForSpeech(Bundle params)
    {

    }

    @Override
    public void onBeginningOfSpeech()
    {

    }

    @Override
    public void onRmsChanged(float rmsdB)
    {

    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {

    }

    @Override
    public void onEndOfSpeech()
    {
        isListening = false;
    }

    @Override
    public void onError(int error)
    {
        isListening = false;
    }

    @Override
    public void onResults(Bundle results)
    {
        isListening = false;
        String str = "";
        ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        if( data != null && !data.isEmpty() )
        {
            str = data.get(0).trim();
        }
        flashcards.moveToPosition(flashcardPager.getCurrentItem());
        String definition = flashcards.getString(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_DEFINITION));

        int correctness = FlashcardFragment.INCORRECT;

        if(str.matches(".*\\d.*") && !definition.matches(".*\\d.*"))
        {
            final Pattern pattern = Pattern.compile("\\d+"); // the regex
            final Matcher matcher = pattern.matcher(str); // your string

            while (matcher.find())
            {
                String numberString = Utils.convertNumberToString(Long.parseLong(matcher.group()));
                str = str.replace(matcher.group(), numberString);
            }
        }


        if(definition.equalsIgnoreCase(str))
        {
            correctness = FlashcardFragment.CORRECT;
            if( speechEnabled )
            {
                tts.speak(getString(R.string.correct), TextToSpeech.QUEUE_FLUSH, null);
            }
        }
        else
        {
            if( speechEnabled )
            {
                tts.speak(getString(R.string.incorrect) + " " + definition, TextToSpeech.QUEUE_FLUSH, null);
            }
        }

        FlashcardFragment currentFragment = (FlashcardFragment)flashcardPageAdapter.instantiateItem(flashcardPager, flashcardPager.getCurrentItem());
        currentFragment.updateFlashcardBackground(correctness);
        currentFragment.flipFlashcard();
        flashcardDao.updateFlashcardCorrectness(correctness, flashcards.getLong(flashcards.getColumnIndex(FlashcardOpenHelper.COLUMN_ID)));
    }

    @Override
    public void onPartialResults(Bundle partialResults)
    {

    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {

    }
}
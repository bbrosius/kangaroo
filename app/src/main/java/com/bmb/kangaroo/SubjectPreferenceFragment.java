package com.bmb.kangaroo;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import com.bmb.graphics.utils.ColorPickerPreference;

public class SubjectPreferenceFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {
	
	/**
	 * The fragment's callback that is used to tell the activity to launch the account selector.
	 */
	private SharedPreferences pref;
    private String prefName;
	
	public SubjectPreferenceFragment()
	{
	}

    @Override
	public void onCreate(Bundle icicle)
	{
		super.onCreate(icicle);

        prefName = getString(R.string.preferences_key);
        if( getArguments() != null )
        {
            if( getArguments().containsKey(getString(R.string.study_subject_id)) )
            {
                prefName = getArguments().getString(getString(R.string.study_subject_id));
            }
        }

		getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(
                this);

        pref = getActivity().getSharedPreferences(prefName, SubjectPreferenceActivity.MODE_PRIVATE);

        addPreferencesFromResource(R.xml.subject_preferences);

        ColorPickerPreference subjectColor = (ColorPickerPreference) findPreference(getString(R.string.subject_color_key));
        if(subjectColor != null)
        {
            subjectColor.setColor(pref.getInt(getString(R.string.subject_color_key), getResources().getColor(R.color.primary_color)));
        }
    }	

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) 
	{
        if( getActivity() != null )
        {
            pref = getActivity().getSharedPreferences(prefName, SubjectPreferenceActivity.MODE_PRIVATE);
            Editor editor = pref.edit();

            if( key.equalsIgnoreCase(getString(R.string.notification_enabled_key)) )
            {
                editor.putBoolean(key, sharedPreferences.getBoolean(key, true));
            }
            else if( key.equalsIgnoreCase(getString(R.string.show_next_exam_countdown_key)) )
            {
                editor.putBoolean(key, sharedPreferences.getBoolean(key, true));
            }
            else if( key.equalsIgnoreCase(getString(R.string.subject_color_key)) )
            {
                editor.putInt(key, sharedPreferences.getInt(key, getResources().getColor(R.color.primary_color)));
            }
            else if( key.equalsIgnoreCase(getString(R.string.color_flashcards_key)) )
            {
                editor.putBoolean(key, sharedPreferences.getBoolean(key, false));
            }
            else if( key.equalsIgnoreCase(getString(R.string.light_theme_key)) )
            {
                editor.putBoolean(key, sharedPreferences.getBoolean(key, false));
            }

            editor.apply();
        }
	}
}
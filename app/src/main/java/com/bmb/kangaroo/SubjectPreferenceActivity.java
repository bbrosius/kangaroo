package com.bmb.kangaroo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class SubjectPreferenceActivity extends BaseActivity implements SharedPreferences.OnSharedPreferenceChangeListener
{
    private DrawerLayout mainLayout;

    @Override
	public void onCreate(Bundle bundle)
	{
        super.onCreate(bundle);

        SharedPreferences pref = getSharedPreferences(getString(R.string.preferences_key), SubjectPreferenceActivity.MODE_PRIVATE);

        setContentView(R.layout.default_activity_layout);

        Toolbar toolBar = (Toolbar) findViewById(R.id.default_toolbar);
        setSupportActionBar(toolBar);

        mainLayout = (DrawerLayout) findViewById(R.id.default_layout);

        //Set the nav menu icon on the tool bar
        toolBar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mainLayout.openDrawer(GravityCompat.START);
            }
        });

        SubjectPreferenceFragment fragment = new SubjectPreferenceFragment();

        getFragmentManager().beginTransaction()
        .replace(R.id.fragment_content, fragment)
        .commit();

        pref.registerOnSharedPreferenceChangeListener(this);
	}

    @Override
    public DrawerLayout getDrawerLayout()
    {
        return mainLayout;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if( key.equalsIgnoreCase(getString(R.string.subject_color_key)) )
        {
            int actionbarColor = sharedPreferences.getInt(key, getResources().getColor(R.color.primary_color));

            if (getSupportActionBar() != null)
            {
                getSupportActionBar().setBackgroundDrawable(new ColorDrawable(actionbarColor));
            }
        }
        else if( key.equalsIgnoreCase(getString(R.string.light_theme_key)) )
        {
            Intent resetIntent = new Intent(this, SubjectPreferenceActivity.class);
            resetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(resetIntent);
            finish();
        }
    }
}
package com.bmb.kangaroo;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bmb.kangaroo.views.FlaschardRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FlashcardStackFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FlashcardStackFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FlashcardStackFragment extends Fragment
{
    //The id of the object to study the flashcards for
    private static final String STUDY_ID_ARG = "study_id";
    private static final String ARG_PARAM2 = "param2";

    private String studyId;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FlashcardStackFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FlashcardStackFragment newInstance(String param1, String param2)
    {
        FlashcardStackFragment fragment = new FlashcardStackFragment();
        Bundle args = new Bundle();
        args.putString(STUDY_ID_ARG, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FlashcardStackFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if( getArguments() != null )
        {
            studyId = getArguments().getString(STUDY_ID_ARG);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        LinearLayout flashcardLayout = (LinearLayout) inflater.inflate(R.layout.flashcard_stack_view, container, false);

        RecyclerView flashcardStack = (RecyclerView) flashcardLayout.findViewById(R.id.flashcard_recycler_view);

        Flashcard one = new Flashcard("foo", "bar");
        Flashcard two = new Flashcard("test", "two");

        List<Flashcard> flashcardList = new ArrayList<>();
        flashcardList.add(one);
        flashcardList.add(two);
        flashcardStack.addItemDecoration(new OverlapDecoration());
        flashcardStack.setAdapter(new FlaschardRecyclerAdapter(getActivity(), flashcardList));
        flashcardStack.setLayoutManager(new LinearLayoutManager(getActivity()));

        return flashcardLayout;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri)
    {
        if( mListener != null )
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //Private dummy class to test view
    public class Flashcard
    {
        String term;
        String defintion;

        public Flashcard(String term, String definition)
        {
            this.term = term;
            this.defintion = definition;
        }

        public String getTerm()
        {
            return term;
        }

        public String getDefintion()
        {
            return defintion;
        }
    }

    public class OverlapDecoration extends RecyclerView.ItemDecoration {

        private final static int vertOverlap = -500;

        @Override
        public void getItemOffsets (Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

            outRect.set(0, vertOverlap, 0, 0);

        }
    }
}
